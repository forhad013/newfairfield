package com.fairfield.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.databinding.FragmentCouponDetailBinding;
import com.fairfield.model.Coupondatum;

public class CouponDetailFragment extends Fragment {

    public static CouponDetailFragment initFragment(Coupondatum coupon) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_COUPON, coupon);
        CouponDetailFragment fragment = new CouponDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private static final String ARG_COUPON = "coupon";

    private static final String TAG = "CouponDetailFragment";

    private FragmentCouponDetailBinding binding;

    private Coupondatum coupon;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            coupon = (Coupondatum) getArguments().getSerializable(ARG_COUPON);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_coupon_detail, container, false);
        View view = binding.getRoot();
        binding.setCoupon(coupon);

        return view;
    }
}
