package com.fairfield.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.databinding.FragmentBannerDetailBinding;
import com.fairfield.databinding.FragmentCouponDetailBinding;
import com.fairfield.model.Bannerdatum;
import com.fairfield.model.Coupondatum;

public class BannerDetailFragment extends Fragment {

    public static BannerDetailFragment initFragment(Bannerdatum banner) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_BANNER, banner);
        BannerDetailFragment fragment = new BannerDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private static final String ARG_BANNER = "banner";

    private static final String TAG = "BannerDetailFragment";

    private FragmentBannerDetailBinding binding;

    private Bannerdatum banner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            banner = (Bannerdatum) getArguments().getSerializable(ARG_BANNER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_banner_detail, container, false);
        View view = binding.getRoot();
        binding.setBanner(banner);

        return view;
    }
}
