package com.fairfield.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.fairfield.R;
import com.fairfield.activity.DashBoardActivity;
import com.fairfield.activity.ForgotPasswordActivity;
import com.fairfield.controller.App;
import com.fairfield.databinding.FragmentSigninBinding;
import com.fairfield.model.UserModel;
import com.fairfield.utility.CheckNetworkConnectivity;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Argalon on 6/14/2017.
 */

/*
public class SiginFragment {
}
*/
public class SiginFragment extends Fragment {
  private  FragmentSigninBinding binding;
  private  Context mContext;
  private  UserModel userModel;
    private Gson gson;
    private  HashMap<String , String> hashMap;
    private static final String TAG = "SiginFragment";
    private PreferenceHelper preferenceHelper;
    App app;
    private CheckNetworkConnectivity mCheckNetworkConnectivity;


    public SiginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signin, container, false);
        mContext = getActivity();
        userModel = new UserModel();
        gson = new Gson();
        preferenceHelper = new PreferenceHelper(mContext);
        app = App.getInstance();
        View view = binding.getRoot();
        mCheckNetworkConnectivity = new CheckNetworkConnectivity(
               mContext);

        binding.inputEmail.addTextChangedListener(new MyTextWatcher(binding.inputEmail));
        binding.inputPassword.addTextChangedListener(new MyTextWatcher(binding.inputPassword));

        binding.textviewForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        binding.btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
        return view;
    }

    private void submitForm() {

        if (!validateEmail()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }

        String email = binding.inputEmail.getText().toString().trim();
        String pass = binding.inputPassword.getText().toString().trim();
        hashMap = new HashMap<>();
        hashMap.put(Constant.EMAIL ,email );
        hashMap.put("password" , pass);
        hashMap.put("from_where" ,"0");

        if (ConnectivityReceiver.isConnected()) {
            emailSignin();
        }else {
            Utility.showSnackBar(mContext, binding.inputEmail, getResources().getString(R.string.no_internet));
        }
    }



    private void emailSignin() {

        Log.e(TAG, "hashMap"+hashMap);

        Utility.showProgressHUD(mContext);
        Constant.retrofitService.emaillogin(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utility.hideProgressHud();
                if (response.isSuccessful()) {
                    try {
                        String responseString = response.body().string();
                        Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + responseString + "]");

                        JSONObject jsonObject = new JSONObject(responseString);
                        if (jsonObject.getBoolean("status")) {

                            userModel = gson.fromJson(jsonObject.getString("data"), UserModel.class);
                            preferenceHelper.putUserId(userModel.getUserId());
                            preferenceHelper.putEmail(userModel.getEmail());
                            preferenceHelper.putLoginBy(Constant.MANUAL);
                            preferenceHelper.putStatus(userModel.getStatus());
                            app.userModel = userModel;
                            Log.e(TAG, " userModel = [" + userModel + "]");
                            Log.e(TAG, " userModel = [" +  app.userModel + "]");
                            Intent intent = new Intent(getActivity() ,DashBoardActivity.class );
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();

                        } else {
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(mContext, "Please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.hideProgressHud();
            }
        });
    }

    private boolean validateEmail() {
        String email = binding.inputEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            //  binding.inputLayoutEmail.setError(getString(R.string.err_msg_email));
            binding.inputEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.worng_icon, 0);
            requestFocus(binding.inputEmail);
            return false;
        } else {
            // binding.inputLayoutEmail.setErrorEnabled(false);
            binding.inputEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_icon, 0);
        }
        return true;
    }

    private boolean validatePassword() {


        if (binding.inputPassword.getText().toString().trim().isEmpty() || binding.inputPassword.getText().toString().trim().length() <= 5) {
            //  binding.inputLayoutPassword.setError(getString(R.string.err_msg_password));
              binding.inputPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.worng_icon, 0);
            requestFocus(binding.inputPassword);
            return false;
        } else {
            //  binding.inputLayoutPassword.setErrorEnabled(false);
            binding.inputPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_icon, 0);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_email:
                    validateEmail();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }
    }





}