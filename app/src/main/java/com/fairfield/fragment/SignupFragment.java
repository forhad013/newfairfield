package com.fairfield.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;

import com.fairfield.R;
import com.fairfield.activity.TermAndConditionActivity;
import com.fairfield.activity.VarificationActivity;
import com.fairfield.controller.App;
import com.fairfield.databinding.FragmentSignupBinding;
import com.fairfield.model.UserModel;
import com.fairfield.utility.CheckNetworkConnectivity;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Argalon on 6/14/2017.
 */


public class SignupFragment extends Fragment {

    FragmentSignupBinding binding;
    private UserModel userModel;
    private Gson gson;
    Context mContext;
    private static final String TAG = "SignupFragment";
    private PreferenceHelper preferenceHelper;
    private App app;
    private CheckNetworkConnectivity mCheckNetworkConnectivity;
    String checkBoxText;

    public SignupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup, container, false);

        userModel = new UserModel();
        gson = new Gson();
        mContext = getActivity();
        preferenceHelper = new PreferenceHelper(mContext);
        app = App.getInstance();
        mCheckNetworkConnectivity = new CheckNetworkConnectivity(mContext);

        //https://www.localneighborhoodapp.com/user/terms_condition
        checkBoxText = "I agree to all the Terms and Conditions";
        binding.textviewCheckbox.setPaintFlags(binding.textviewCheckbox.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        binding.textviewCheckbox.setText(checkBoxText);

        binding.textviewCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, TermAndConditionActivity.class);
                startActivity(intent);

            }
        });

        //binding.checkbox.setMovementMethod(LinkMovementMethod.getInstance());


        binding.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){

                    userModel.setTerms_condition("1");
                }else {
                    userModel.setTerms_condition("0");
                }
            }
        });


        binding.btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {
                    submitForm();
                } else {
                    Utility.showSnackBar(mContext, binding.inputEmail, getResources().getString(R.string.no_internet));
                }
            }
        });

        View view = binding.getRoot();
        return view;
    }


    private void submitForm() {

        userModel.setDeviceToken("");
        userModel.setDeviceId(Utility.generateDeviceId(mContext));
        userModel.setDeviceType(Constant.DEVICE_TYPE_ANDROID);
        userModel.setFromWhere("0");

        if (!validateName()) {
            return;
        }

        if (!validateEmail()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        if (!validateConfirmPassword()) {
            return;
        }
        if (!comparePassword()) {
            return;
        }

        if (!binding.checkbox.isChecked()) {

            Utility.showAlert(mContext, getString(R.string.err_checkterm));
            return;
        }

     /*   if (!validateContact()){
            return;
        }*/

        userModel.setFirstName(binding.inputName.getText().toString().trim());
        userModel.setEmail(binding.inputEmail.getText().toString().trim());
        userModel.setPassword(binding.inputPassword.getText().toString().trim());
        userModel.setCpassword(binding.inputConfirmpassword.getText().toString().trim());
        //userModel.setContact(binding.inputPhonenumber.getText().toString().trim());


        emailLogin();

        // Toast.makeText(getActivity(), "Thank You!", Toast.LENGTH_SHORT).show();
    }


    private void emailLogin() {

        Log.e(TAG, "userModel" + userModel);

        Utility.showProgressHUD(mContext);
        Constant.retrofitService.user_signup(Utility.getModeltoMap(userModel)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utility.hideProgressHud();
                if (response.isSuccessful()) {
                    try {
                        String responseString = response.body().string();

                        Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + responseString + "]");

                        JSONObject jsonObject = new JSONObject(responseString);
                        if (jsonObject.getBoolean("status")) {

                            userModel = gson.fromJson(jsonObject.getString("data"), UserModel.class);
                            app.userModel = userModel;
                            preferenceHelper.putUserId(userModel.getUserId());
                            preferenceHelper.putEmail(userModel.getEmail());
                            preferenceHelper.putLoginBy(Constant.MANUAL);
                            preferenceHelper.putStatus(userModel.getStatus());

                            Log.e(TAG, " userModel = [" + userModel + "]");


                            Intent intent = new Intent(mContext, VarificationActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();

/*
                            Intent intent = new Intent(mContext, DashBoardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();*/


                        } else {
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(mContext, "Please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.hideProgressHud();
            }
        });
    }

    private boolean validateName() {
        if (binding.inputName.getText().toString().trim().isEmpty()) {
            binding.inputLayoutName.setError(getString(R.string.err_msg_name));
            requestFocus(binding.inputName);
            return false;
        } else {
            binding.inputLayoutName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = binding.inputEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            binding.inputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(binding.inputEmail);
            return false;
        } else {
            binding.inputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePassword() {
        if (binding.inputPassword.getText().toString().trim().isEmpty()) {

            Utility.showAlert(mContext, getString(R.string.err_msg_password));
            //  binding.inputLayoutPassword.setError(getString(R.string.err_msg_password));
            //  requestFocus(binding.inputPassword);
            return false;

        } else if (binding.inputPassword.getText().toString().trim().length() <= 5) {
            Utility.showAlert(mContext, "Please must enter 6 digit password!");

            return false;
        }

        // binding.inputLayoutPassword.setErrorEnabled(false);
        return true;
    }


    private boolean comparePassword() {

        if (!binding.inputPassword.getText().toString().trim().equals(binding.inputConfirmpassword.getText().toString().trim())) {
            Utility.showAlert(mContext, getString(R.string.notmatchpassword));

            return false;

        } else {

        }

        return true;
    }

    private boolean validateConfirmPassword() {
        if (binding.inputConfirmpassword.getText().toString().trim().isEmpty()) {

            Utility.showAlert(mContext, getString(R.string.err_msg_password));
            return false;

        } else {

        }

        return true;
    }


/*
    private boolean validateContact() {
        if (binding.inputPhonenumber.getText().toString().trim().isEmpty()) {
            binding.inputPhonenumber.setError(getString(R.string.err_msg_contact));
            requestFocus(binding.inputPhonenumber);
            return false;

        } else {
            binding.inputLayoutPhonenumber.setErrorEnabled(false);
        }

        return true;
    }
*/

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
                case R.id.input_email:
                    validateEmail();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }
    }

}