package com.fairfield.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fairfield.R;
import com.fairfield.activity.ProviderDetails;
import com.fairfield.adapter.ProvidersAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityNewBusinessOwnerBinding;
import com.fairfield.model.ProvidersModel;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NewBusinessFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ActivityNewBusinessOwnerBinding binding;

    private Context mContext;
    private PreferenceHelper preferenceHelper;
    private HashMap<String, String> hashMap;
    private static final String TAG = "NewBusinessFragment";
    private ArrayList<ProvidersModel> newprovidersModelArrayList;
    private Gson gson;
    private ProvidersAdapter adapter;
    private App app;
    String Subcategoryid;

    public NewBusinessFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_new_business_owner, container, false);
        mContext = getActivity();
        gson = new Gson();
        app = App.getInstance();
        preferenceHelper = new PreferenceHelper(mContext);

        Log.e(TAG, "Subcategoryid: " + Subcategoryid);
        newprovidersModelArrayList = new ArrayList<>();
        if (!newprovidersModelArrayList.isEmpty()) {
            newprovidersModelArrayList.clear();
        }
        View view = binding.getRoot();

        TextView toolbar = getActivity().findViewById(R.id.title_news_feed);
        toolbar.setText("New Businesses");

        binding.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));
        binding.recyclerView.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerView.setNestedScrollingEnabled(false);
        binding.recyclerView.setHasFixedSize(false);

        adapter = new ProvidersAdapter(mContext, newprovidersModelArrayList);
        binding.recyclerView.setAdapter(adapter);

        if (ConnectivityReceiver.isConnected()) {
            try {
                getProvidersData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            binding.textviewInternet.setVisibility(View.VISIBLE);
        }

        ((ProvidersAdapter) binding.recyclerView.getAdapter()).setOnItemClickListener(new ProvidersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                app.providersModel = adapter.getProvidersModel().get(position);
                Intent intent = new Intent(mContext, ProviderDetails.class);
                startActivity(intent);
            }
        });

        return view;
    }


    public void getProvidersData() {
        startAnim();
        binding.textviewInternet.setVisibility(View.GONE);
        binding.textviewNoNewBusinessOwners.setVisibility(View.GONE);
        HashMap<String, String> hashMap = new HashMap<>();

        Constant.retrofitService.newbusinessOwner().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);
                        stopAnim();
                        if (jsonObject.getBoolean("status")) {
                   //         Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<ProvidersModel>>() {
                            }.getType();

                            if (!newprovidersModelArrayList.isEmpty()) {
                                newprovidersModelArrayList.clear();
                            }
                            newprovidersModelArrayList = gson.fromJson(jsonObject.getString("data"), type);

                      //      Log.e("ArrayList", "" + newprovidersModelArrayList.size());
                      //      Log.e(TAG, "onResponse() called with: call = [" + call + "], localEventsArrayList = [" + newprovidersModelArrayList.size() + "]");
                            adapter.setProvidersModelList(newprovidersModelArrayList);
                            adapter.notifyDataSetChanged();
                        } else {
                            stopAnim();
                            binding.textviewNoNewBusinessOwners.setVisibility(View.VISIBLE);
                        }
                    } else {
                        stopAnim();
                        binding.textviewNoNewBusinessOwners.setVisibility(View.VISIBLE);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

           //     Log.e(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]", t);

            }
        });
    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }
}