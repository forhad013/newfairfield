package com.fairfield.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fairfield.R;
import com.fairfield.adapter.FeedbackCommentAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.FragmentNewReviewBinding;
import com.fairfield.model.FeedbackModel;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewReviewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FragmentNewReviewBinding binding;

    private Context mContext;
    private PreferenceHelper preferenceHelper;
    private HashMap<String, String> hashMap;
    private static final String TAG = "ShowAllEventComment";
    private ArrayList<FeedbackModel> arrayList;
    private Gson gson;
    private FeedbackCommentAdapter adapter;
    private App app;


    public NewReviewFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_review, container, false);
        mContext = getActivity();
        gson = new Gson();
        app = App.getInstance();

        arrayList = new ArrayList<>();
        View view = binding.getRoot();

        TextView toolbar = (TextView) getActivity().findViewById(R.id.title_news_feed);
        toolbar.setText("New Reviews");


        if (!arrayList.isEmpty()) {
            arrayList.clear();
        }


        binding.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));
        binding.recyclerView.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerView.setNestedScrollingEnabled(false);
        binding.recyclerView.setHasFixedSize(false);

        adapter = new FeedbackCommentAdapter(mContext, arrayList);
        binding.recyclerView.setAdapter(adapter);


        if (ConnectivityReceiver.isConnected()) {


            try {
                showfeedbackComment();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            binding.textviewInternet.setVisibility(View.VISIBLE);
        }


        return view;

    }

    private void showfeedbackComment() {

        binding.textviewInternet.setVisibility(View.GONE);
        binding.textviewNoNewReviews.setVisibility(View.GONE);
        startAnim();

        Constant.retrofitService.new_reviews().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        stopAnim();


                        final JSONObject jsonObject = new JSONObject(responsestring);
                        if (jsonObject.getBoolean("status")) {
                            Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");

                            if (!arrayList.isEmpty()) {
                                arrayList.clear();
                            }
                            final Type type = new TypeToken<List<FeedbackModel>>() {
                            }.getType();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    try {
                                        arrayList = gson.fromJson(jsonObject.getString("data"), type);
                                        Log.e("ArrayList", "" + arrayList.size());
                                        adapter.setFeedbackModelList(arrayList);
                                        adapter.notifyDataSetChanged();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else {

                            stopAnim();
                            binding.textviewNoNewReviews.setVisibility(View.VISIBLE);
                        }

                    } else {

                        stopAnim();
                        binding.textviewNoNewReviews.setVisibility(View.VISIBLE);
                        //Utility.showAlert(mContext, "Please try after some time.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    // TODO: Rename method, update argument and hook method into UI event

}
