package com.fairfield.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fairfield.R;
import com.fairfield.activity.ProviderDetails;
import com.fairfield.adapter.FeedbackRequestAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.FragmentNewFeedbackRequestBinding;
import com.fairfield.model.FeedbackRequestModel;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewFeedbackRequest extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FragmentNewFeedbackRequestBinding binding;
    private Context mContext;
    Gson gson;
    App app;
    private PreferenceHelper preferenceHelper;
    private ArrayList<FeedbackRequestModel> feedbackRequestArrayList;
    private static final String TAG = "NewFeedbackRequest";
    private FeedbackRequestAdapter feedbackRequestAdapter;

    public NewFeedbackRequest() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_feedback_request, container, false);
        mContext = getActivity();
        gson = new Gson();
        app = App.getInstance();
        preferenceHelper = new PreferenceHelper(mContext);
        feedbackRequestArrayList = new ArrayList<>();

        View view = binding.getRoot();
        TextView toolbar = (TextView) getActivity().findViewById(R.id.title_news_feed);
        toolbar.setText("Requested Reviews");


        binding.recyclerFeedback.setLayoutManager(new GridLayoutManager(mContext, 1));
        binding.recyclerFeedback.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerFeedback.setNestedScrollingEnabled(false);
        binding.recyclerFeedback.setHasFixedSize(false);

        feedbackRequestAdapter = new FeedbackRequestAdapter(mContext, feedbackRequestArrayList);
        binding.recyclerFeedback.setAdapter(feedbackRequestAdapter);


        if (ConnectivityReceiver.isConnected()) {

            try {
                showfeedbackrequest();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            binding.textviewInternet.setVisibility(View.VISIBLE);
        }


        ((FeedbackRequestAdapter) binding.recyclerFeedback.getAdapter()).setOnItemClickListener(new FeedbackRequestAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.e(TAG, "onItemClick() called with: view = [" + ((FeedbackRequestAdapter) binding.recyclerFeedback.getAdapter()) + "], position = [" + position + "]");


                String ownerid = feedbackRequestAdapter.getFeedbackRequestModel().get(position).getOwnerId();
              /*  Intent intent = new Intent(mContext, FeedbackActivity.class);
                intent.putExtra("FROM" ,"feedbackrequest");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
*/


                //      Log.e(TAG, "onItemClick() Des>>>>>>>>>>>>>>>= [" + app.providersModel + "]");

                //     Log.e(TAG, "onItemClick: click ");

              /*  Intent intent = new Intent(mContext, CategoryDetail.class);
                startActivity(intent);*/
                Intent intent = new Intent(mContext, ProviderDetails.class);
                intent.putExtra("From", "Ad");
                intent.putExtra("OWNERID", ownerid);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        return view;
    }


    private void showfeedbackrequest() {


        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        binding.textviewNo.setVisibility(View.GONE);
        binding.textviewInternet.setVisibility(View.GONE);
        startAnim();


        Constant.retrofitService.getAllFeedbackRequest(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        stopAnim();


                        final JSONObject jsonObject = new JSONObject(responsestring);
                        if (jsonObject.getBoolean("status")) {
                            Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");

                            if (!feedbackRequestArrayList.isEmpty()) {
                                feedbackRequestArrayList.clear();
                            }
                            final Type type = new TypeToken<List<FeedbackRequestModel>>() {
                            }.getType();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    try {
                                        feedbackRequestArrayList = gson.fromJson(jsonObject.getString("data"), type);
                                        Log.e("feedbackReques", "" + feedbackRequestArrayList.size());
                                        feedbackRequestAdapter.setFeedbackRequestModelList(feedbackRequestArrayList);
                                        feedbackRequestAdapter.notifyDataSetChanged();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else {

                            stopAnim();
                            binding.textviewNo.setVisibility(View.VISIBLE);
                        }

                    } else {
                        stopAnim();
                        binding.textviewNo.setVisibility(View.VISIBLE);
                        //Utility.showAlert(mContext, "Please try after some time.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

}
