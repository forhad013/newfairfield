package com.fairfield.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fairfield.model.RewardModel;
import com.fairfield.utility.Constant;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fairfield.R;
import com.fairfield.utility.RecyclerItemClickListener;

import okhttp3.ResponseBody;
import retrofit2.Callback;

public class RewardTableCellAdapter extends RecyclerView.Adapter<RewardTableCellAdapter.MyViewHolder> {

    private static RecyclerViewClickListener itemListener;

    private List<RewardModel> mRewardItems;
    private Context mContext;

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    // Pass in the contact array into the constructor
    public RewardTableCellAdapter(Context context, List<RewardModel> rewardItems) {
        mRewardItems = rewardItems;
        mContext = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title, datetext;
        public TextView comments;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);

            title = view.findViewById(R.id.title);
            comments = view.findViewById(R.id.comment);
            datetext = view.findViewById(R.id.datetext);
            thumbnail = view.findViewById(R.id.image);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }

    public RewardTableCellAdapter(Context context, List<RewardModel> rewardItems, RecyclerViewClickListener itemListener) {
        this.mContext = context;

        mRewardItems = rewardItems;

        this.itemListener = itemListener;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reward_tablecell, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder viewHolder, int position) {
        RewardModel rewardItem = mRewardItems.get(position);

        TextView textView = viewHolder.title;
        textView.setText(rewardItem.getmTitle());

        textView = viewHolder.comments;
        textView.setText(rewardItem.getmComment());
        if (rewardItem.getmComment().isEmpty()) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
        }

        textView = viewHolder.datetext;
        textView.setText(rewardItem.getmDate());

        final ImageView imgView = viewHolder.thumbnail;
        imgView.setVisibility(View.VISIBLE);
        if (!rewardItem.getmImage().isEmpty()) {
            String url_str = Constant.COMMENT_URL + rewardItem.getmImage();
            Glide.with(mContext).load(url_str).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    viewHolder.comments.setVisibility(View.VISIBLE);
                    return false;
                }
            }).into(imgView);
        } else {
//            imgView.getLayoutParams().height = 0;
            imgView.setVisibility(View.GONE);
            viewHolder.comments.setVisibility(View.VISIBLE);
            imgView.requestLayout();
        }
    }

    @Override
    public int getItemCount() {
        return mRewardItems.size();
    }



    public interface RecyclerViewClickListener
    {
        public void recyclerViewListClicked(View v, int position);
    }

    public void removeItem(int position) {
        mRewardItems.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(RewardModel item, int position) {
        mRewardItems.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bmp;
        }
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
//                Bitmap rotatedBitmap = null;
//
//                switch (orientation) {
//
//                    case ExifInterface.ORIENTATION_ROTATE_90:
//                        rotatedBitmap = rotateImage(result, 90);
//                        break;
//
//                    case ExifInterface.ORIENTATION_ROTATE_180:
//                        rotatedBitmap = rotateImage(result, 180);
//                        break;
//
//                    case ExifInterface.ORIENTATION_ROTATE_270:
//                        rotatedBitmap = rotateImage(result, 270);
//                        break;
//
//                    case ExifInterface.ORIENTATION_NORMAL:
//                    default:
//                        rotatedBitmap = result;
//                }
                bmImage.setVisibility(View.VISIBLE);
                bmImage.setImageBitmap(result);
            } else {
                bmImage.setVisibility(View.GONE);
            }
        }
    }
}
