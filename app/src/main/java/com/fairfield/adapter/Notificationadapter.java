package com.fairfield.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.databinding.RowNotificationBinding;
import com.fairfield.model.NotificationModel;

import java.util.List;


/**
 * Created by Argalon on 7/25/2017.
 */
public class Notificationadapter extends RecyclerView.Adapter<Notificationadapter.ViewHolder> {


    private final Context mContext;
    private List<NotificationModel> mNotificationModelList;
    private OnItemClickListener onItemClickListener;


    public Notificationadapter(Context mContext, List<NotificationModel> mNotificationModelList) {
        this.mNotificationModelList = mNotificationModelList;
        this.mContext = mContext;
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowNotificationBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_notification, parent, false);


        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NotificationModel mNotificationModel = this.mNotificationModelList.get(position);

        if (mNotificationModelList.get(holder.getAdapterPosition()).getFlag().equals("geofencing")) {


            holder.binding.textviewData.setText(Html.fromHtml(" You are near  <font color=\"#088fc6\">" + mNotificationModelList.get(holder.getAdapterPosition()).getOwnerData().getBusinessname()+ "</font>" ));
        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getFlag().equals("new_business")){


            holder.binding.textviewData.setText(Html.fromHtml(" <font color=\"#088fc6\">" + mNotificationModelList.get(holder.getAdapterPosition()).getOwnerData().getBusinessname() + "</font>" +" just registered!"));
        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getFlag().equals("feedback_request")){

            holder.binding.textviewData.setText(Html.fromHtml("A review is requested for  <font color=\"#088fc6\">" + mNotificationModelList.get(holder.getAdapterPosition()).getOwnerData().getBusinessname() + "</font>" +" please share your Experience! "));


        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getFlag().equals("new_reviews")){

            holder.binding.textviewData.setText(Html.fromHtml(" <font color=\"#088fc6\">" + mNotificationModelList.get(holder.getAdapterPosition()).getOwnerData().getBusinessname() + "</font>" +" received a new review!"));
        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getFlag().equals("email_recevied")){

            holder.binding.textviewData.setText(Html.fromHtml("\" Received email from \" <font color=\"#088fc6\">" + mNotificationModelList.get(holder.getAdapterPosition()).getOwnerData().getBusinessname() + "</font>" +"business owner. Check Inbox - Email! "));
        }

        holder.bind(mNotificationModel);
    }


    @Override
    public int getItemCount() {
        return mNotificationModelList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<NotificationModel> getNotificationModel() {
        return mNotificationModelList;
    }


    public void setNotificationModelList(List<NotificationModel> mNotificationModelList) {
        this.mNotificationModelList = mNotificationModelList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowNotificationBinding binding;


        public ViewHolder(final View view, final RowNotificationBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {


                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final NotificationModel mNotificationModel) {
            this.binding.setNotification(mNotificationModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}