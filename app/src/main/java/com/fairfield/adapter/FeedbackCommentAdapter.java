package com.fairfield.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.activity.FeedbackActivity;
import com.fairfield.activity.ProviderDetails;
import com.fairfield.activity.SliderActivity;
import com.fairfield.controller.App;
import com.fairfield.databinding.RowProviderFeedbackBinding;
import com.fairfield.model.FeedbackModel;
import com.fairfield.utility.CheckNetworkConnectivity;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.login.widget.ProfilePictureView.TAG;


/**
 * Created by Argalon on 7/13/2017.
 */
public class FeedbackCommentAdapter extends RecyclerView.Adapter<FeedbackCommentAdapter.ViewHolder> {


    private final Context mContext;
    private List<FeedbackModel> mFeedbackModelList;
    private OnItemClickListener onItemClickListener;
    CheckNetworkConnectivity mCheckNetworkConnectivity;
    App app;
    PreferenceHelper preferenceHelper;

    public FeedbackCommentAdapter(Context mContext, List<FeedbackModel> mFeedbackModelList) {
        this.mFeedbackModelList = mFeedbackModelList;
        this.mContext = mContext;
        this.mCheckNetworkConnectivity = new CheckNetworkConnectivity(mContext);
        preferenceHelper = new PreferenceHelper(mContext);
        app = App.getInstance();
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowProviderFeedbackBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_provider_feedback, parent, false);
        binding.mainLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.textFeedback.readMoreClickable();
            }
        });
        binding.textFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.textFeedback.readMoreClickable();
            }
        });
        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FeedbackModel mFeedbackModel = this.mFeedbackModelList.get(position);


        if (preferenceHelper.getUserId().equals(mFeedbackModel.getUserId())) {

            holder.binding.llEditDele.setVisibility(View.VISIBLE);
        }else{
            holder.binding.llEditDele.setVisibility(View.INVISIBLE);
        }


        holder.binding.textviewBusinessname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String providerid = mFeedbackModelList.get(holder.getAdapterPosition()).getOwnerId();
                Intent intent = new Intent(mContext, ProviderDetails.class);
                intent.putExtra("From", "Ad");
                intent.putExtra("OWNERID", providerid);
                mContext.startActivity(intent);


            }
        });


        holder.binding.fabPickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    Log.e(TAG, "onClick: " + holder.getAdapterPosition() + " " + mFeedbackModelList.get(holder.getAdapterPosition()).getImages());
                    if (!mFeedbackModelList.get(holder.getAdapterPosition()).getImages().isEmpty()) {

                        if (mFeedbackModelList.get(holder.getAdapterPosition()).getImages().size() > 0) {
                            app.eventCommentModelArrayList = mFeedbackModelList.get(holder.getAdapterPosition()).getImages();

                            Log.e(TAG, "eventCommentModelArrayList: " + app.eventCommentModelArrayList.size());
                            Intent intent = new Intent(mContext, SliderActivity.class);
                            intent.putExtra("FROM", "feedback");
                            mContext.startActivity(intent);
                        } else {
                            Utility.showSnackBar(mContext, holder.binding.fabPickGallery, "No images");
                        }
                    } else {
                        Utility.showSnackBar(mContext, holder.binding.fabPickGallery, "No images");
                    }
                } else {
                    Utility.showSnackBar(mContext, holder.binding.fabPickGallery, mContext.getResources().getString(R.string.no_internet));
                    //  Utility.showSnackBarlengthlong(mContext, "");
                }

            }
        });

        holder.binding.imageviewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("review_id", mFeedbackModelList.get(holder.getAdapterPosition()).getId());

                Constant.retrofitService.deletereview(hashMap).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {

                            try {
                                String res = response.body().string();
                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getBoolean("status")) {

                                    removeItem(holder.getAdapterPosition());

                                } else {
                                    Utility.showSnackBar(mContext, holder.binding.llEditDele, "please try after some time");

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });


        holder.binding.imageviewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                app.feedbackModel = mFeedbackModelList.get(holder.getAdapterPosition());

                Log.e(TAG, "feedbackModel>>>>>>>>>>>>>>>>>>>>>>: " + app.feedbackModel);

                app.ownerid = mFeedbackModelList.get(holder.getAdapterPosition()).getOwnerId();
                Intent intent = new Intent(mContext, FeedbackActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("FROM", "edit");
                mContext.startActivity(intent);
            }
        });


        holder.bind(mFeedbackModel);
    }

    public void removeItem(int position) {

        mFeedbackModelList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFeedbackModelList.size());

    }

    @Override
    public int getItemCount() {
        return mFeedbackModelList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<FeedbackModel> getFeedbackModel() {
        return mFeedbackModelList;
    }


    public void setFeedbackModelList(List<FeedbackModel> mFeedbackModelList) {
        this.mFeedbackModelList = mFeedbackModelList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowProviderFeedbackBinding binding;


        public ViewHolder(final View view, final RowProviderFeedbackBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {


                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final FeedbackModel mFeedbackModel) {
            this.binding.setFeedback(mFeedbackModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}