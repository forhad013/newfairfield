package com.fairfield.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.databinding.RowFeedbackRequestBinding;
import com.fairfield.model.FeedbackRequestModel;

import java.util.List;


/**
 * Created by Argalon on 7/26/2017.
 */
public class FeedbackRequestAdapter extends RecyclerView.Adapter<FeedbackRequestAdapter.ViewHolder> {


    private final Context mContext;
    private List<FeedbackRequestModel> mFeedbackRequestModelList;
    private OnItemClickListener onItemClickListener;


    public FeedbackRequestAdapter(Context mContext, List<FeedbackRequestModel> mFeedbackRequestModelList) {
        this.mFeedbackRequestModelList = mFeedbackRequestModelList;
        this.mContext = mContext;
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowFeedbackRequestBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_feedback_request, parent, false);
        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FeedbackRequestModel mFeedbackRequestModel = this.mFeedbackRequestModelList.get(position);


        holder.binding.textviewData.setText(Html.fromHtml("A review is requested for <font color=\"#088fc6\">" + mFeedbackRequestModelList.get(holder.getAdapterPosition()).getBusinessname() + "</font>" +" please share your Experience!"));
        holder.bind(mFeedbackRequestModel);
    }


    @Override
    public int getItemCount() {
        return mFeedbackRequestModelList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<FeedbackRequestModel> getFeedbackRequestModel() {
        return mFeedbackRequestModelList;
    }


    public void setFeedbackRequestModelList(List<FeedbackRequestModel> mFeedbackRequestModelList) {
        this.mFeedbackRequestModelList = mFeedbackRequestModelList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowFeedbackRequestBinding binding;


        public ViewHolder(final View view, final RowFeedbackRequestBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {


                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final FeedbackRequestModel mFeedbackRequestModel) {
            this.binding.setFeedbackrequest(mFeedbackRequestModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}