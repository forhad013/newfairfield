package com.fairfield.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.fairfield.fragment.CouponDetailFragment;
import com.fairfield.model.Coupondatum;

import java.util.List;

public class CouponPagerAdapter extends FragmentStatePagerAdapter {
    public CouponPagerAdapter(FragmentManager fm, List<Coupondatum> coupons) {
        super(fm);
        this.coupons = coupons;
    }

    private List<Coupondatum> coupons;

    @Override
    public Fragment getItem(int position) {
        return CouponDetailFragment.initFragment(coupons.get(position));
    }

    @Override
    public int getCount() {
        return coupons.size();
    }
}
