package com.fairfield.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fairfield.R;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Argalon on 9/22/2017.
 */
public class DiynamicImageAdapter extends RecyclerView.Adapter<DiynamicImageAdapter.ViewHolder> {


    List<String> stringList;
    Context context;
    View view1;
    ViewHolder viewHolder1;
    TextView textView;
    Bitmap  screled;


    public DiynamicImageAdapter(Context context1, List<String> stringList1 ) {

        stringList = stringList1;
        context = context1;
    }

    public DiynamicImageAdapter(Context context1, Bitmap screled) {

        screled = screled;
        context = context1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.imageview);

        }
    }

    @Override
    public DiynamicImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view1 = LayoutInflater.from(context).inflate(R.layout.row_addimagediynamic, parent, false);
        viewHolder1 = new ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.imageView.setImageResource(R.drawable.deleticon);

        String baseurl_for_image = "https://www.localneighborhoodapp.com/assets/rating_review/";

        Picasso.with(context).load(baseurl_for_image + stringList.get(position)).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(holder.imageView);
    }

    @Override
    public int getItemCount() {

        return stringList.size();
    }
}