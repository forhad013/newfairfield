package com.fairfield.adapter;

/**
 * Created by Argalon on 7/5/2017.
 */


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.databinding.RowSavecouponsBinding;
import com.fairfield.model.Coupondatum;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Argalon on 6/20/2017.
 */
public class SaveCouponAdapter extends RecyclerView.Adapter<SaveCouponAdapter.ViewHolder> {


    private final Context mContext;
    private List<Coupondatum> mSavecouponList;
    private OnItemClickListener onItemClickListener;

    private SparseBooleanArray selectedItems;
    private static final String TAG = "ProfileSaveAdapter";
    LayoutInflater mInflater;
    private boolean isActivated = false;
    private PreferenceHelper preferenceHelper;

    public SaveCouponAdapter(Context mContext, List<Coupondatum> mSavecouponList) {
        this.mSavecouponList = mSavecouponList;
        this.mContext = mContext;
        selectedItems = new SparseBooleanArray();
        mInflater = LayoutInflater.from(mContext);
        this.preferenceHelper = new PreferenceHelper(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        //     final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowSavecouponsBinding binding = DataBindingUtil.inflate(mInflater, R.layout.row_savecoupons, parent, false);
        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Coupondatum showCoupansModel = this.mSavecouponList.get(position);

        holder.binding.unsaveIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    String value = showCoupansModel.getId();

                    Log.e(TAG, "value: " + value);
                    Log.e(TAG, "position: " + holder.getAdapterPosition());


                    removeCouponbyUser(mSavecouponList.get(holder.getAdapterPosition()).getId());
                    removeItem(holder.getAdapterPosition());

                    Log.e(TAG, "onClick save icon " + value);

                } else {
                    //  Utility.showSnackBarlengthlong(mContext, "");
                }
                Log.e(TAG, "save onClick: " + position);

            }
        });


        holder.bind(showCoupansModel);
    }

    private void removeCouponbyUser(String coupon_id) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("coupon_id", coupon_id);
        Log.e(TAG, "removeCouponbyUser: " + hashMap);

        Constant.retrofitService.removecoupon(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");

            }
        });

    }

    public void removeItem(int position) {

        mSavecouponList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mSavecouponList.size());

    }


    @Override
    public int getItemCount() {
        return mSavecouponList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<Coupondatum> getProfileSave() {
        return mSavecouponList;
    }


    public void setProfileSaveList(List<Coupondatum> mSavecouponList) {
        this.mSavecouponList = mSavecouponList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowSavecouponsBinding binding;


        public ViewHolder(final View view, final RowSavecouponsBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {

                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final Coupondatum showCoupansModel) {
            this.binding.setSavecoupons(showCoupansModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }


    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {

            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
            if (!isActivated)
                isActivated = true;

        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        if (isActivated)
            isActivated = false;
        notifyDataSetChanged();
    }


    public int getSelectedItemCount() {
        return selectedItems.size();
    }


    public Coupondatum getItem(int position) {
        return mSavecouponList.get(position);
    }

}