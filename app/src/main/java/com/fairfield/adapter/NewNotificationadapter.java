package com.fairfield.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.databinding.RowNotificationBinding;
import com.fairfield.model.NewNotificationModel;
import com.fairfield.model.NotificationModel;

import java.util.List;


/**
 * Created by Argalon on 7/25/2017.
 */
public class NewNotificationadapter extends RecyclerView.Adapter<NewNotificationadapter.ViewHolder> {


    private final Context mContext;
    private List<NewNotificationModel> mNotificationModelList;
    private OnItemClickListener onItemClickListener;


    public NewNotificationadapter(Context mContext, List<NewNotificationModel> mNotificationModelList) {
        this.mNotificationModelList = mNotificationModelList;
        this.mContext = mContext;
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowNotificationBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_notification, parent, false);


        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NewNotificationModel mNotificationModel = this.mNotificationModelList.get(position);

        if(mNotificationModelList.get(holder.getAdapterPosition()).getType().equals("new_business")){


            holder.binding.textviewData.setText(Html.fromHtml(" <font color=\"#088fc6\">" + mNotificationModelList.get(holder.getAdapterPosition()).getBusinessName() + "</font>" +" just registered!"));
        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getType().equals("review_request")){

            holder.binding.textviewData.setText(Html.fromHtml("A review is requested for  <font color=\"#088fc6\">" + mNotificationModelList.get(holder.getAdapterPosition()).getBusinessName() + "</font>" +" please share your Experience! "));


        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getType().equals("new_review")){

            holder.binding.textviewData.setText(Html.fromHtml(" <font color=\"#088fc6\">" + mNotificationModelList.get(holder.getAdapterPosition()).getBusinessName() + "</font>" +" received a new review!"));

        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getType().equals("new_announcement")){

            holder.binding.textviewData.setText( mNotificationModelList.get(holder.getAdapterPosition()).getTitle());

        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getType().equals("new_coupon")){

            holder.binding.textviewData.setText(Html.fromHtml(  "New coupon from" + " <font color=\"#088fc6\">" + mNotificationModelList.get(holder.getAdapterPosition()).getBusinessName() + "</font>" ));


        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getType().equals("new_event")){

            holder.binding.textviewData.setText(mNotificationModelList.get(holder.getAdapterPosition()).getTitle());

        }else if(mNotificationModelList.get(holder.getAdapterPosition()).getType().equals("new_reward")) {

            holder.binding.textviewData.setText(mNotificationModelList.get(holder.getAdapterPosition()).getTitle());

        }
        holder.bind(mNotificationModel);
    }


    @Override
    public int getItemCount() {
        return mNotificationModelList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<NewNotificationModel> getNotificationModel() {
        return mNotificationModelList;
    }


    public void setNotificationModelList(List<NewNotificationModel> mNotificationModelList) {
        this.mNotificationModelList = mNotificationModelList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowNotificationBinding binding;


        public ViewHolder(final View view, final RowNotificationBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {


                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final NewNotificationModel mNotificationModel) {
           // this.binding.setNotification(mNotificationModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}