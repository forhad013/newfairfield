package com.fairfield.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.activity.SliderActivity;
import com.fairfield.controller.App;
import com.fairfield.databinding.RowEventCommentBinding;
import com.fairfield.model.EventCommentModel;
import com.fairfield.utility.CheckNetworkConnectivity;
import com.fairfield.utility.Utility;

import java.util.List;


/**
 * Created by Argalon on 7/7/2017.
 */
public class EventCommentAdapter extends RecyclerView.Adapter<EventCommentAdapter.ViewHolder> {


    private final Context mContext;
    private List<EventCommentModel> mEventCommentModelList;
    private OnItemClickListener onItemClickListener;
    CheckNetworkConnectivity mCheckNetworkConnectivity;
    App app;
    private static final String TAG = "EventCommentAdapter";

    public EventCommentAdapter(Context mContext, List<EventCommentModel> mEventCommentModelList) {
        this.mEventCommentModelList = mEventCommentModelList;
        this.mContext = mContext;
        this.mCheckNetworkConnectivity = new CheckNetworkConnectivity(mContext);
        app = App.getInstance();
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowEventCommentBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_event_comment, parent, false);


        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final EventCommentModel mEventCommentModel = this.mEventCommentModelList.get(position);


        holder.binding.fabPickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCheckNetworkConnectivity.isNetworkAvailable()) {


                    Log.e(TAG, "onClick: "+holder.getAdapterPosition() +" "+mEventCommentModelList.get(holder.getAdapterPosition()).getImages());

                    if (!mEventCommentModelList.get(holder.getAdapterPosition()).getImages().isEmpty()) {

                        if (mEventCommentModelList.get(holder.getAdapterPosition()).getImages().size() > 0) {
                            app.eventCommentModelArrayList = mEventCommentModelList.get(holder.getAdapterPosition()).getImages();


                            Log.e(TAG, "eventCommentModelArrayList: "+ app.eventCommentModelArrayList.size() );

                            Intent intent = new Intent(mContext, SliderActivity.class);
                            intent.putExtra("FROM","event");
                            mContext.startActivity(intent);
                        } else {

                            Utility.showSnackBar(mContext, holder.binding.fabPickGallery, "No images");

                        }

                    }else {

                        Utility.showSnackBar(mContext, holder.binding.fabPickGallery, "No images");

                    }


                } else {
                    //  Utility.showSnackBarlengthlong(mContext, "");
                }

            }
        });

        holder.bind(mEventCommentModel);
    }


    @Override
    public int getItemCount() {
        return mEventCommentModelList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<EventCommentModel> getEventCommentModel() {
        return mEventCommentModelList;
    }


    public void setEventCommentModelList(List<EventCommentModel> mEventCommentModelList) {
        this.mEventCommentModelList = mEventCommentModelList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowEventCommentBinding binding;


        public ViewHolder(final View view, final RowEventCommentBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {


                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final EventCommentModel mEventCommentModel) {
            this.binding.setCommnet(mEventCommentModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}