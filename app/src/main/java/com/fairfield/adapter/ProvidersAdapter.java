package com.fairfield.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.databinding.RowBusinessProvidersBinding;
import com.fairfield.model.ProvidersModel;

import java.util.List;


/**
 * Created by Argalon on 7/11/2017.
 */
public class ProvidersAdapter extends RecyclerView.Adapter<ProvidersAdapter.ViewHolder> {


    private final Context mContext;
    private List<ProvidersModel> mProvidersModelList;
    private OnItemClickListener onItemClickListener;


    public ProvidersAdapter(Context mContext, List<ProvidersModel> mProvidersModelList) {
        this.mProvidersModelList = mProvidersModelList;
        this.mContext = mContext;
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowBusinessProvidersBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_business_providers, parent, false);
        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ProvidersModel mProvidersModel = this.mProvidersModelList.get(position);
        holder.bind(mProvidersModel);
    }


    @Override
    public int getItemCount() {
        return mProvidersModelList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<ProvidersModel> getProvidersModel() {
        return mProvidersModelList;
    }


    public void setProvidersModelList(List<ProvidersModel> mProvidersModelList) {
        this.mProvidersModelList = mProvidersModelList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowBusinessProvidersBinding binding;


        public ViewHolder(final View view, final RowBusinessProvidersBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {


                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final ProvidersModel mProvidersModel) {
            this.binding.setProvider(mProvidersModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}