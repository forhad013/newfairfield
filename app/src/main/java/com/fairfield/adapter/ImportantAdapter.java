package com.fairfield.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.activity.FacebookGroupActivity;
import com.fairfield.activity.NewsWebview;
import com.fairfield.databinding.RowImportantBinding;
import com.fairfield.model.ImportantModel;

import java.util.List;


/**
 * Created by Argalon on 8/3/2017.
 */
public class ImportantAdapter extends RecyclerView.Adapter<ImportantAdapter.ViewHolder> {


    private final Context mContext;
    private List<ImportantModel> mImportantModelList;
    private OnItemClickListener onItemClickListener;


    public ImportantAdapter(Context mContext, List<ImportantModel> mImportantModelList) {
        this.mImportantModelList = mImportantModelList;
        this.mContext = mContext;
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowImportantBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_important, parent, false);


        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ImportantModel mImportantModel = this.mImportantModelList.get(position);



        holder.binding.textviewWebsite.setPaintFlags( holder.binding.textviewWebsite.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        holder.binding.textviewFacebook.setPaintFlags( holder.binding.textviewFacebook.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        holder.binding.textviewWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = mImportantModelList.get(holder.getAdapterPosition()).getWebsite();

                Intent intent = new Intent(mContext, NewsWebview.class);
                intent.putExtra("url", url);
                intent.putExtra("texttoolbar", mContext.getResources().getString(R.string.website));
                mContext.startActivity(intent);
            }
        });


        holder.binding.textviewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = mImportantModelList.get(holder.getAdapterPosition()).getFacebook();

                Intent intent = new Intent(mContext, FacebookGroupActivity.class);
                intent.putExtra("url", url);
                // intent.putExtra("texttoolbar", mContext.getResources().getString(R.string.website));
                mContext.startActivity(intent);
            }
        });

        holder.bind(mImportantModel);
    }


    @Override
    public int getItemCount() {
        return mImportantModelList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<ImportantModel> getImportantModel() {
        return mImportantModelList;
    }


    public void setImportantModelList(List<ImportantModel> mImportantModelList) {
        this.mImportantModelList = mImportantModelList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowImportantBinding binding;


        public ViewHolder(final View view, final RowImportantBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {


                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final ImportantModel mImportantModel) {
            this.binding.setImportant(mImportantModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}