package com.fairfield.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.fairfield.fragment.BannerDetailFragment;
import com.fairfield.fragment.CouponDetailFragment;
import com.fairfield.model.Bannerdatum;
import com.fairfield.model.Coupondatum;

import java.util.List;

public class BannerPagerAdapter extends FragmentStatePagerAdapter {
    public BannerPagerAdapter(FragmentManager fm, List<Bannerdatum> banners) {
        super(fm);
        this.banners = banners;
    }

    private List<Bannerdatum> banners;

    @Override
    public Fragment getItem(int position) {
        return BannerDetailFragment.initFragment(banners.get(position));
    }

    @Override
    public int getCount() {
        return banners.size();
    }
}
