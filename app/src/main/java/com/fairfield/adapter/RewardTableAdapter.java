package com.fairfield.adapter;

import com.bumptech.glide.Glide;
import com.fairfield.model.RewardModel;
import com.fairfield.utility.Constant;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fairfield.R;
import com.fairfield.utility.RecyclerItemClickListener;

import okhttp3.ResponseBody;
import retrofit2.Callback;

public class RewardTableAdapter extends RecyclerView.Adapter<RewardTableAdapter.MyViewHolder> {

    private static RecyclerViewClickListener itemListener;

    private List<RewardModel> mRewardItems;
    private Context mContext;

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    // Pass in the contact array into the constructor
    public RewardTableAdapter(Context context, List<RewardModel> rewardItems) {
        mRewardItems = rewardItems;
        mContext = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);

            title = view.findViewById(R.id.titleReward);
            thumbnail = view.findViewById(R.id.imageReward);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }

    public RewardTableAdapter(Context context, List<RewardModel> rewardItems, RecyclerViewClickListener itemListener) {
        this.mContext = context;

        mRewardItems = rewardItems;

        this.itemListener = itemListener;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reward_cell, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position) {
        RewardModel rewardItem = mRewardItems.get(position);

        TextView titletextView = viewHolder.title;
//        if (!rewardItem.isShowTitle()) {
//            titletextView.setText("");
//        } else {
//            titletextView.setText(rewardItem.getmTitle());
//        }
        titletextView.setText(rewardItem.getmTitle());

        ImageView imgView = viewHolder.thumbnail;
        String url_str = Constant.REWARDIMG_URL + rewardItem.getmImage();
        Glide.with(mContext).load(url_str).into(imgView);
        //new DownloadImageTask(imgView).execute(url_str);
    }

    @Override
    public int getItemCount() {
        return mRewardItems.size();
    }



    public interface RecyclerViewClickListener
    {
        public void recyclerViewListClicked(View v, int position);
    }

    public void removeItem(int position) {
        mRewardItems.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(RewardModel item, int position) {
        mRewardItems.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bmp;
        }
        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
