package com.fairfield.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.databinding.RowLocaleventsBinding;
import com.fairfield.model.LocalEventsModel;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.login.widget.ProfilePictureView.TAG;

/**
 * Created by Argalon on 7/4/2017.
 */

public class SaveEventAdapter extends RecyclerView.Adapter<SaveEventAdapter.ViewHolder> {


    private final Context mContext;
    private List<LocalEventsModel> mLocalEventsModelList;
    private OnItemClickListener onItemClickListener;
    private PreferenceHelper preferenceHelper;
    //CheckNetworkConnectivity mCheckNetworkConnectivity;


    public SaveEventAdapter(Context mContext, List<LocalEventsModel> mLocalEventsModelList) {
        this.mLocalEventsModelList = mLocalEventsModelList;
        this.mContext = mContext;
        this.preferenceHelper = new PreferenceHelper(mContext);
       // this.mCheckNetworkConnectivity = new CheckNetworkConnectivity(mContext);
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowLocaleventsBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_localevents, parent, false);
        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final LocalEventsModel mLocalEventsModel = this.mLocalEventsModelList.get(position);


        Log.e(TAG, "onBindViewHolder: "+mLocalEventsModelList.get(position).getEventsave() );

        if (mLocalEventsModelList.get(position).getEventsave().equals("1")){

            holder.binding.imageviewSaveicon.setImageResource(R.drawable.saveicon);
        }else {

            holder.binding.imageviewSaveicon.setImageResource(R.drawable.unsaveicon);
        }


        holder.binding.imageviewSaveicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {

                    String value = mLocalEventsModel.getEventsave();

                    Log.e(TAG, "value: "+value);
                    Log.e(TAG, "position: "+holder.getAdapterPosition() );


                    removeEventbyUser(mLocalEventsModelList.get(holder.getAdapterPosition()).getId(), "0");
                    removeItem(holder.getAdapterPosition());

                    Log.e(TAG, "onClick save icon "+value );

                } else {
                    //  Utility.showSnackBarlengthlong(mContext, "");
                }
                Log.e(TAG, "save onClick: "+position );

            }
        });

        holder.bind(mLocalEventsModel);
    }


    @Override
    public int getItemCount() {
        return mLocalEventsModelList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<LocalEventsModel> getLocalEventsModel() {
        return mLocalEventsModelList;
    }


    public void setLocalEventsModelList(List<LocalEventsModel> mLocalEventsModelList) {
        this.mLocalEventsModelList = mLocalEventsModelList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public void removeItem(int position) {

        mLocalEventsModelList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,mLocalEventsModelList.size());

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowLocaleventsBinding binding;


        public ViewHolder(final View view, final RowLocaleventsBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {

                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final LocalEventsModel mLocalEventsModel) {
            this.binding.setLocalevent(mLocalEventsModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }



    private void removeEventbyUser(String eventid , String value){

        HashMap<String , String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID ,preferenceHelper.getUserId());
        hashMap.put("event_id" , eventid);
        hashMap.put("event_like" ,value);
        Log.e(TAG, "saveEventbyUser: "+hashMap );

        Constant.retrofitService.saveEventbyUser(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");

            }
        });

    }

}
