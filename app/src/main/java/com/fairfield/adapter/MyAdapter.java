package com.fairfield.adapter;

/**
 * Created by Argalon on 7/7/2017.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fairfield.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends PagerAdapter {

    private List<String> images;
    private LayoutInflater inflater;
    private Context context;
    private static final String TAG = "MyAdapter";
    private String url;

    public MyAdapter(Context context, List<String> images, String url) {
        this.context = context;
        this.images = images;
        this.url = url;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.row_slider, view, false);
        ImageView myImage = (ImageView) myImageLayout
                .findViewById(R.id.image);

        Log.e(TAG, "instantiateItem: " + images);
        String imag = url + images.get(position);

        Log.e(TAG, "instantiateItem: " + imag);

        Picasso.with(view.getContext()).load(imag).into(myImage);

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}