package com.fairfield.adapter;

import android.databinding.BindingAdapter;
import android.util.Log;
import android.widget.ImageView;

import com.fairfield.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Rohit on 7/25/2016.
 */

public class DatabindingCustomAdapter {

    private static final String TAG = "DatabindingCustomAdapte";

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Log.e(TAG, "loadImage() ImageView = [" + view + "], imageUrl = [" + imageUrl + "]");
        if (imageUrl.isEmpty()) {
            imageUrl = "https://share.okdothis.com/assets/default-avatar-c246f74ff56d382424ac2c750e40b0a6.png?w=64&h=64";

            imageUrl.replaceAll(" ", "%20");
            Picasso.with(view.getContext()).load(imageUrl).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(view);

        } else {
            imageUrl.replaceAll(" ", "%20");
            Picasso.with(view.getContext()).load(imageUrl).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(view);
        }
//        Glide.with(view.getContext()).load(imageUrl).skipMemoryCache( false ).into(view);
    }


    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(CircleImageView view, String imageUrl) {
        imageUrl.replaceAll(" ", "%20");
        Log.e(TAG, "loadImage() CircleImageView  = [" + view + "], imageUrl = [" + imageUrl+ "]");
        Picasso.with(view.getContext())
                .load(imageUrl).placeholder(R.drawable.placeholder).error(R.drawable.placeholder)
                .into(view);
        // Glide.with(view.getContext()).load(imageUrl).skipMemoryCache( false ).placeholder(R.drawable.argalon).into(view);
    }
}
