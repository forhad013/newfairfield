package com.fairfield.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairfield.R;
import com.fairfield.databinding.RowLocaleventsBinding;
import com.fairfield.model.LocalEventsModel;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Argalon on 6/30/2017.
 */
public class LocalEventsAdapter extends RecyclerView.Adapter<LocalEventsAdapter.ViewHolder> {


    private final Context mContext;
    private List<LocalEventsModel> mLocalEventsModelList;
    private OnItemClickListener onItemClickListener;
    private PreferenceHelper  preferenceHelper;



    public LocalEventsAdapter(Context mContext, List<LocalEventsModel> mLocalEventsModelList) {
        this.mLocalEventsModelList = mLocalEventsModelList;
        this.mContext = mContext;
        this.preferenceHelper = new PreferenceHelper(mContext);

    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowLocaleventsBinding binding = DataBindingUtil.inflate(inflater, R.layout.row_localevents, parent, false);
        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final LocalEventsModel mLocalEventsModel = this.mLocalEventsModelList.get(position);


       // Log.e(TAG, "onBindViewHolder: "+mLocalEventsModelList.get(position).getEventsave() );

     if (mLocalEventsModelList.get(position).getEventsave().equals("1")){

            holder.binding.imageviewSaveicon.setImageResource(R.drawable.saveicon);
        }else {

            holder.binding.imageviewSaveicon.setImageResource(R.drawable.unsaveicon);
        }


        holder.binding.imageviewSaveicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {

                    String value = mLocalEventsModel.getEventsave();

               //     Log.e(TAG, "value: "+value);

                    if (value.equals("1")){
                        mLocalEventsModelList.get(position).setEventsave("0");
                        holder.binding.imageviewSaveicon.setImageResource(R.drawable.unsaveicon);
                        saveEventbyUser(mLocalEventsModelList.get(position).getId() , "0");
                    }else {
                        mLocalEventsModelList.get(position).setEventsave("1");
                        holder.binding.imageviewSaveicon.setImageResource(R.drawable.saveicon);
                        saveEventbyUser(mLocalEventsModelList.get(position).getId() , "1");
                    }

                   // Log.e(TAG, "onClick save icon "+value );

                } else {
                    Utility.showSnackBar(mContext,  holder.binding.imageviewSaveicon, mContext.getResources().getString(R.string.no_internet));
                    //  Utility.showSnackBarlengthlong(mContext, "");
                }
               // Log.e(TAG, "save onClick: "+position );
            }
        });

        holder.bind(mLocalEventsModel);
    }


    @Override
    public int getItemCount() {
        return mLocalEventsModelList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<LocalEventsModel> getLocalEventsModel() {
        return mLocalEventsModelList;
    }


    public void setLocalEventsModelList(List<LocalEventsModel> mLocalEventsModelList) {
        this.mLocalEventsModelList = mLocalEventsModelList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowLocaleventsBinding binding;


        public ViewHolder(final View view, final RowLocaleventsBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {

                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final LocalEventsModel mLocalEventsModel) {
            this.binding.setLocalevent(mLocalEventsModel);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }



    private void saveEventbyUser(String eventid , String value){

        HashMap<String , String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID ,preferenceHelper.getUserId());
        hashMap.put("event_id" , eventid);
        hashMap.put("event_like" ,value);

       // Log.e(TAG, "saveEventbyUser: "+hashMap );

        Constant.retrofitService.saveEventbyUser(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

              //  Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

             //   Log.d(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");

            }
        });

    }

}