package com.fairfield.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.fairfield.R;
import com.fairfield.databinding.RowProfilesaveBinding;
import com.fairfield.model.ProfileSave;

import java.util.List;


/**
 * Created by Argalon on 6/20/2017.
 */
public class ProfileSaveAdapter extends RecyclerView.Adapter<ProfileSaveAdapter.ViewHolder> {


    private final Context mContext;
    private List<ProfileSave> mProfileSaveList;
    private OnItemClickListener onItemClickListener;

    private SparseBooleanArray selectedItems;
    private static final String TAG = "ProfileSaveAdapter";
    LayoutInflater mInflater;
    private boolean isActivated = false;

    public ProfileSaveAdapter(Context mContext, List<ProfileSave> mProfileSaveList) {
        this.mProfileSaveList = mProfileSaveList;
        this.mContext = mContext;
        selectedItems = new SparseBooleanArray();
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        //     final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final RowProfilesaveBinding binding = DataBindingUtil.inflate(mInflater, R.layout.row_profilesave, parent, false);
        return new ViewHolder(binding.getRoot(), binding);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ProfileSave mProfileSave = this.mProfileSaveList.get(position);



        holder.itemView.setActivated(selectedItems.get(position, false));

        if (holder.itemView.isActivated()) {

            final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.scale_for_button_animtion_enter);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    holder.binding.selectIcon.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            holder.binding.selectIcon.startAnimation(animation);
        } else {

            final Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.scale_for_button_animtion_exit);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    holder.binding.selectIcon.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            holder.binding.selectIcon.startAnimation(animation);

        }

        holder.bind(mProfileSave);
    }


    @Override
    public int getItemCount() {
        return mProfileSaveList.size();
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    public List<ProfileSave> getProfileSave() {
        return mProfileSaveList;
    }


    public void setProfileSaveList(List<ProfileSave> mProfileSaveList) {
        this.mProfileSaveList = mProfileSaveList;
        notifyDataSetChanged();
    }

    private void dataSetChanged() {
        this.notifyDataSetChanged();

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowProfilesaveBinding binding;


        public ViewHolder(final View view, final RowProfilesaveBinding binding) {
            super(view);
            this.binding = binding;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            try {

                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @UiThread
        public void bind(final ProfileSave mProfileSave) {
            this.binding.setProfilesave(mProfileSave);
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }


    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {

            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
            if (!isActivated)
                isActivated = true;

        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        if (isActivated)
            isActivated = false;
        notifyDataSetChanged();
    }


    public int getSelectedItemCount() {
        return selectedItems.size();
    }


    public ProfileSave getItem(int position) {
        return mProfileSaveList.get(position);
    }

}