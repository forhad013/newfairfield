package com.fairfield.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.List;

/**
 * Created by Argalon on 7/5/2017.
 */

public class CommentModel {
    @SerializedName("event_id")
    @Expose
    String event_id;

    @SerializedName("user_id")
    @Expose
    String user_id ;

    @SerializedName("comment")
    @Expose
    String comment;

    @SerializedName("id")
    @Expose
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @SerializedName("images")

    @Expose
    List<File> images;



    @SerializedName("tag")
    @Expose
    String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        return "CommentModel{" +
                "event_id='" + event_id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", comment='" + comment + '\'' +
                ", images=" + images +
                '}';
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<File> getImages() {
        return images;
    }

    public void setImages(List<File> images) {
        this.images = images;
    }
}
