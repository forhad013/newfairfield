
package com.fairfield.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;


public class BusinessCategoryModel {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @SerializedName("image")

    @Expose
    private String image;


    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("subcategories")
    @Expose
    @Valid
    private List<Subcategory> subcategories = new ArrayList<Subcategory>();

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The subcategories
     */
    public List<Subcategory> getSubcategories() {
        return subcategories;
    }

    /**
     * 
     * @param subcategories
     *     The subcategories
     */
    public void setSubcategories(List<Subcategory> subcategories) {
        this.subcategories = subcategories;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
