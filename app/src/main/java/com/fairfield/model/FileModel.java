package com.fairfield.model;

import java.io.File;

/**
 * Created by Argalon on 9/25/2017.
 */

public class FileModel {

    int imageid;

    File file;

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
