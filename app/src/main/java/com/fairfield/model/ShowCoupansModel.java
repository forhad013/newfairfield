
package com.fairfield.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class ShowCoupansModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id ="";
    @SerializedName("owner_id")
    @Expose
    private String ownerId="";
    @SerializedName("image")
    @Expose
    private String image="";
    @SerializedName("description")
    @Expose
    private String description ="";
    @SerializedName("coupon_name")
    @Expose
    private String couponName ="";
    @SerializedName("coupon_code")
    @Expose
    private String couponCode ="";
    @SerializedName("created_at")
    @Expose
    private String createdAt ="";
    @SerializedName("update_at")
    @Expose
    private String updateAt ="";

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The ownerId
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     * 
     * @param ownerId
     *     The owner_id
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The couponName
     */
    public String getCouponName() {
        return couponName;
    }

    /**
     * 
     * @param couponName
     *     The coupon_name
     */
    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    /**
     * 
     * @return
     *     The couponCode
     */
    public String getCouponCode() {
        return couponCode;
    }

    /**
     * 
     * @param couponCode
     *     The coupon_code
     */
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updateAt
     */
    public String getUpdateAt() {
        return updateAt;
    }

    /**
     * 
     * @param updateAt
     *     The update_at
     */
    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
