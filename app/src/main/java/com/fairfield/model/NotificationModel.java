
package com.fairfield.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.Valid;


public class NotificationModel {

    @SerializedName("type")
    @Expose
    private String flag;
    @SerializedName("review")
    @Expose
    private String review;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public ProvidersModel getProvidersModel() {
        return providersModel;
    }

    public void setProvidersModel(ProvidersModel providersModel) {
        this.providersModel = providersModel;
    }

    @SerializedName("user_name")

    @Expose
    private String user_name;


    @SerializedName("review_by")
    @Expose
    private String reviewBy;
    @SerializedName("owner_data")
    @Expose
    @Valid
    private ProvidersModel providersModel;

    /**
     * 
     * @return
     *     The flag
     */
    public String getFlag() {
        return flag;
    }

    /**
     * 
     * @param flag
     *     The flag
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * 
     * @return
     *     The review
     */
    public String getReview() {
        return review;
    }

    /**
     * 
     * @param review
     *     The review
     */
    public void setReview(String review) {
        this.review = review;
    }

    /**
     * 
     * @return
     *     The reviewBy
     */
    public String getReviewBy() {
        return reviewBy;
    }

    /**
     * 
     * @param reviewBy
     *     The review_by
     */
    public void setReviewBy(String reviewBy) {
        this.reviewBy = reviewBy;
    }

    /**
     * 
     * @return
     *     The ownerData
     */
    public ProvidersModel getOwnerData() {
        return providersModel;
    }

    /**
     * 
     * @param ownerData
     *     The owner_data
     */
    public void setOwnerData(ProvidersModel ownerData) {
        this.providersModel = ownerData;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
