
package com.fairfield.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;


public class FeedbackRequestModel {

    @SerializedName("owner_id")
    @Expose
    private String ownerId;
    @SerializedName("businessname")
    @Expose
    private String businessname;
    @SerializedName("businessdescription")
    @Expose
    private String businessdescription;
    @SerializedName("phonenumber")
    @Expose
    private String phonenumber;
    @SerializedName("ownername")
    @Expose
    private String ownername;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("subcategory")
    @Expose
    private String subcategory;
    @SerializedName("weblink")
    @Expose
    private String weblink;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("kyewords")
    @Expose
    private String kyewords;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("emailcode")
    @Expose
    private String emailcode;
    @SerializedName("logintype")
    @Expose
    private String logintype;
    @SerializedName("referred_id")
    @Expose
    private String referredId;
    @SerializedName("referred_point")
    @Expose
    private String referredPoint;
    @SerializedName("paystatus")
    @Expose
    private String paystatus;
    @SerializedName("currentplan")
    @Expose
    private String currentplan;
    @SerializedName("paytill")
    @Expose
    private String paytill;
    @SerializedName("store_purchase")
    @Expose
    private String storePurchase;
    @SerializedName("profileupdate")
    @Expose
    private String profileupdate;
    @SerializedName("average_rating")
    @Expose
    private String averageRating;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("notifyon")
    @Expose
    private String notifyon;
    @SerializedName("ini_count_feedback")
    @Expose
    private String iniCountFeedback;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    /**
     * 
     * @return
     *     The ownerId
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     * 
     * @param ownerId
     *     The owner_id
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * 
     * @return
     *     The businessname
     */
    public String getBusinessname() {
        return businessname;
    }

    /**
     * 
     * @param businessname
     *     The businessname
     */
    public void setBusinessname(String businessname) {
        this.businessname = businessname;
    }

    /**
     * 
     * @return
     *     The businessdescription
     */
    public String getBusinessdescription() {
        return businessdescription;
    }

    /**
     * 
     * @param businessdescription
     *     The businessdescription
     */
    public void setBusinessdescription(String businessdescription) {
        this.businessdescription = businessdescription;
    }

    /**
     * 
     * @return
     *     The phonenumber
     */
    public String getPhonenumber() {
        return phonenumber;
    }

    /**
     * 
     * @param phonenumber
     *     The phonenumber
     */
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    /**
     * 
     * @return
     *     The ownername
     */
    public String getOwnername() {
        return ownername;
    }

    /**
     * 
     * @param ownername
     *     The ownername
     */
    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param password
     *     The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * @return
     *     The category
     */
    public String getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * 
     * @return
     *     The subcategory
     */
    public String getSubcategory() {
        return subcategory;
    }

    /**
     * 
     * @param subcategory
     *     The subcategory
     */
    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    /**
     * 
     * @return
     *     The weblink
     */
    public String getWeblink() {
        return weblink;
    }

    /**
     * 
     * @param weblink
     *     The weblink
     */
    public void setWeblink(String weblink) {
        this.weblink = weblink;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * 
     * @param address
     *     The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 
     * @return
     *     The kyewords
     */
    public String getKyewords() {
        return kyewords;
    }

    /**
     * 
     * @param kyewords
     *     The kyewords
     */
    public void setKyewords(String kyewords) {
        this.kyewords = kyewords;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The emailcode
     */
    public String getEmailcode() {
        return emailcode;
    }

    /**
     * 
     * @param emailcode
     *     The emailcode
     */
    public void setEmailcode(String emailcode) {
        this.emailcode = emailcode;
    }

    /**
     * 
     * @return
     *     The logintype
     */
    public String getLogintype() {
        return logintype;
    }

    /**
     * 
     * @param logintype
     *     The logintype
     */
    public void setLogintype(String logintype) {
        this.logintype = logintype;
    }

    /**
     * 
     * @return
     *     The referredId
     */
    public String getReferredId() {
        return referredId;
    }

    /**
     * 
     * @param referredId
     *     The referred_id
     */
    public void setReferredId(String referredId) {
        this.referredId = referredId;
    }

    /**
     * 
     * @return
     *     The referredPoint
     */
    public String getReferredPoint() {
        return referredPoint;
    }

    /**
     * 
     * @param referredPoint
     *     The referred_point
     */
    public void setReferredPoint(String referredPoint) {
        this.referredPoint = referredPoint;
    }

    /**
     * 
     * @return
     *     The paystatus
     */
    public String getPaystatus() {
        return paystatus;
    }

    /**
     * 
     * @param paystatus
     *     The paystatus
     */
    public void setPaystatus(String paystatus) {
        this.paystatus = paystatus;
    }

    /**
     * 
     * @return
     *     The currentplan
     */
    public String getCurrentplan() {
        return currentplan;
    }

    /**
     * 
     * @param currentplan
     *     The currentplan
     */
    public void setCurrentplan(String currentplan) {
        this.currentplan = currentplan;
    }

    /**
     * 
     * @return
     *     The paytill
     */
    public String getPaytill() {
        return paytill;
    }

    /**
     * 
     * @param paytill
     *     The paytill
     */
    public void setPaytill(String paytill) {
        this.paytill = paytill;
    }

    /**
     * 
     * @return
     *     The storePurchase
     */
    public String getStorePurchase() {
        return storePurchase;
    }

    /**
     * 
     * @param storePurchase
     *     The store_purchase
     */
    public void setStorePurchase(String storePurchase) {
        this.storePurchase = storePurchase;
    }

    /**
     * 
     * @return
     *     The profileupdate
     */
    public String getProfileupdate() {
        return profileupdate;
    }

    /**
     * 
     * @param profileupdate
     *     The profileupdate
     */
    public void setProfileupdate(String profileupdate) {
        this.profileupdate = profileupdate;
    }

    /**
     * 
     * @return
     *     The averageRating
     */
    public String getAverageRating() {
        return averageRating;
    }

    /**
     * 
     * @param averageRating
     *     The average_rating
     */
    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    /**
     * 
     * @return
     *     The createdTime
     */
    public String getCreatedTime() {
        return createdTime;
    }

    /**
     * 
     * @param createdTime
     *     The created_time
     */
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 
     * @return
     *     The notifyon
     */
    public String getNotifyon() {
        return notifyon;
    }

    /**
     * 
     * @param notifyon
     *     The notifyon
     */
    public void setNotifyon(String notifyon) {
        this.notifyon = notifyon;
    }

    /**
     * 
     * @return
     *     The iniCountFeedback
     */
    public String getIniCountFeedback() {
        return iniCountFeedback;
    }

    /**
     * 
     * @param iniCountFeedback
     *     The ini_count_feedback
     */
    public void setIniCountFeedback(String iniCountFeedback) {
        this.iniCountFeedback = iniCountFeedback;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
