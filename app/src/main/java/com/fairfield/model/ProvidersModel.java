
package com.fairfield.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.validation.Valid;

public class ProvidersModel {

    @SerializedName("owner_id")
    @Expose
    private String ownerId;
    @SerializedName("businessname")
    @Expose
    private String businessname;
    @SerializedName("businessdescription")
    @Expose
    private String businessdescription;
    @SerializedName("phonenumber")
    @Expose
    private String phonenumber;
    @SerializedName("ownername")
    @Expose
    private String ownername;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("weblink")
    @Expose
    private String weblink;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("kyewords")
    @Expose
    private String kyewords;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("average_rating")
    @Expose
    private Float averageRating;

    public int getAddress_status() {



        if(address_status==null || address_status.equalsIgnoreCase("")) {
            return 0;

        }
        return Integer.parseInt(address_status);
    }

    public void setAddress_status(String address_status) {
        this.address_status = address_status;
    }

    public String getOwner_name_status() {
        return owner_name_status;
    }

    public void setOwner_name_status(String owner_name_status) {
        this.owner_name_status = owner_name_status;
    }

    @SerializedName("address_status")
    @Expose
    private String address_status;


    @SerializedName("owner_name_status")
    @Expose
    private String owner_name_status;

    @SerializedName("open_time")
    @Expose
    private String open_time;


    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public String getClose_time() {
        return close_time;
    }

    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }

    @SerializedName("close_time")

    @Expose
    private String close_time;


    @SerializedName("coupondata")
    @Expose
    @Valid
    private List<Coupondatum> coupondata = new ArrayList<Coupondatum>();
    @SerializedName("bannerdata")
    @Expose
    @Valid
    private List<Bannerdatum> bannerdata = new ArrayList<Bannerdatum>();

    /**
     * @return The ownerId
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     * @param ownerId The owner_id
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * @return The businessname
     */
    public String getBusinessname() {
        return businessname;
    }

    /**
     * @param businessname The businessname
     */
    public void setBusinessname(String businessname) {
        this.businessname = businessname;
    }

    /**
     * @return The businessdescription
     */
    public String getBusinessdescription() {
        return businessdescription;
    }

    /**
     * @param businessdescription The businessdescription
     */
    public void setBusinessdescription(String businessdescription) {
        this.businessdescription = businessdescription;
    }

    /**
     * @return The phonenumber
     */
    public String getPhonenumber() {
        return phonenumber;
    }

    /**
     * @param phonenumber The phonenumber
     */
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    /**
     * @return The ownername
     */
    public String getOwnername() {
        return ownername;
    }

    /**
     * @param ownername The ownername
     */
    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The weblink
     */
    public String getWeblink() {
        return weblink;
    }

    /**
     * @param weblink The weblink
     */
    public void setWeblink(String weblink) {
        this.weblink = weblink;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The kyewords
     */
    public String getKyewords() {
        return kyewords;
    }

    /**
     * @param kyewords The kyewords
     */
    public void setKyewords(String kyewords) {
        this.kyewords = kyewords;
    }

    /**
     * @return The createdTime
     */
    public String getCreatedTime() {
        return createdTime;
    }

    /**
     * @param createdTime The created_time
     */
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }


    public float getAverageRating() {
        return averageRating != null ? averageRating.floatValue() : 0;
    }

    public void setAverageRating(float averageRating) {
        this.averageRating = averageRating;
    }

    /**
     * @return The coupondata
     */
    public List<Coupondatum> getCoupondata() {
        return coupondata;
    }

    /**
     * @param coupondata The coupondata
     */
    public void setCoupondata(List<Coupondatum> coupondata) {
        this.coupondata = coupondata;
    }

    /**
     * @return The bannerdata
     */
    public List<Bannerdatum> getBannerdata() {
        return bannerdata;
    }

    /**
     * @param bannerdata The bannerdata
     */
    public void setBannerdata(List<Bannerdatum> bannerdata) {
        this.bannerdata = bannerdata;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
