
package com.fairfield.model;

public class RewardDataModel {

    public int reward_id;
    public boolean can_comment;
    public boolean can_enter;
    public boolean show_title;
    public String mDescription;
    public String mTitle;
    public String mImage;
    public int mActive;

    public RewardDataModel(int id, boolean can_cmt, boolean can_ent, boolean show_ttl, String mDesc, String title, String imgUrl, int active) {

        reward_id = id;
        can_comment = can_cmt;
        can_enter = can_ent;
        show_title = show_ttl;
        mDescription = mDesc;
        mTitle = title;
        mImage = imgUrl;
        mActive = active;
    }

    public int getReward_id() {
        return reward_id;
    }

    public int getmActive() {
        return mActive;
    }

    public String getmDescription() {
        return mDescription;
    }

    public boolean isCan_comment() {
        return can_comment;
    }

    public String getmImage() {
        return mImage;
    }

    public String getmTitle() {
        return mTitle;
    }

    public boolean isCan_enter() {
        return can_enter;
    }

    public boolean isShow_title() {
        return show_title;
    }
}
