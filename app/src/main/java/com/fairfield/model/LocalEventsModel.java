
package com.fairfield.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;


public class LocalEventsModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("event_date")
    @Expose
    private String eventDate;
    @SerializedName("eventsave")
    @Expose
    private String eventsave;
    @SerializedName("interested")
    @Expose
    private String interested;
    @SerializedName("nothing")
    @Expose
    private String nothing;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * 
     * @param address
     *     The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 
     * @return
     *     The contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * 
     * @param contact
     *     The contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * 
     * @return
     *     The eventDate
     */
    public String getEventDate() {
        return eventDate;
    }

    /**
     * 
     * @param eventDate
     *     The event_date
     */
    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    /**
     * 
     * @return
     *     The eventsave
     */
    public String getEventsave() {
        return eventsave;
    }

    /**
     * 
     * @param eventsave
     *     The eventsave
     */
    public void setEventsave(String eventsave) {
        this.eventsave = eventsave;
    }

    /**
     * 
     * @return
     *     The interested
     */
    public String getInterested() {
        return interested;
    }

    /**
     * 
     * @param interested
     *     The interested
     */
    public void setInterested(String interested) {
        this.interested = interested;
    }

    /**
     * 
     * @return
     *     The nothing
     */
    public String getNothing() {
        return nothing;
    }

    /**
     * 
     * @param nothing
     *     The nothing
     */
    public void setNothing(String nothing) {
        this.nothing = nothing;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
