package com.fairfield.model;

public class RewardModel {

    public String mTitle;
    public String mImage;
    public int mId;
    public boolean showTitle;
    public String mDate;
    public String mComment;

    public String getmTitle() {
        return mTitle;
    }

    public String getmImage() {
        return mImage;
    }

    public int getmId() {
        return mId;
    }

    public String getmDate() {
        return mDate;
    }

    public boolean isShowTitle() {
        return showTitle;
    }

    public String getmComment() {
        return mComment;
    }

    public RewardModel(int id, String title, boolean shtitle, String img, String date, String comment) {

        mId = id;
        mTitle = title;
        showTitle = shtitle;
        mImage = img;
        mDate = date;
        mComment = comment;
    }
}
