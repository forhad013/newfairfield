package com.fairfield.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.fairfield.R;
import com.fairfield.databinding.ActivityGetStartBinding;
import com.fairfield.utility.Constant;

public class GetStartActivity extends AppCompatActivity implements View.OnClickListener{

    ActivityGetStartBinding binding;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        binding = DataBindingUtil.setContentView(this , R.layout.activity_get_start);
        binding.textviewSignin.setOnClickListener(this);
        binding.textviewGetstart.setOnClickListener(this);

      //  getPermission();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.textview_signin:
                Intent  intent = new Intent(mContext , SignupActivity.class);
                startActivity(intent);
                finish();

                // Do something
                break;
            case R.id.textview_getstart:
                int page = 1;
                Intent  intent_getstart = new Intent(mContext , SignupActivity.class);
                intent_getstart.putExtra(Constant.STARTFLOW , page);
                startActivity(intent_getstart);
                finish();
                // Do something
                break;

        }
    }

    @SuppressLint("NewApi")
    private void getPermission() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {

            String[] permissions_dummy = new String[1];
            int i = 0;

            String permission = "android.permission.ACCESS_FINE_LOCATION";
            int res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {
                permissions_dummy[i] = permission;
                i = i + 1;
            }
            String[] permissions = new String[i];

            for (int j = 0; j < i; j++) {
                permissions[j] = permissions_dummy[j];

            }
            int yourRequestId = 1;
            if (i != 0) {

                // Do something for lollipop and above versions
                requestPermissions(permissions, yourRequestId);
            }

        }

    }

}
