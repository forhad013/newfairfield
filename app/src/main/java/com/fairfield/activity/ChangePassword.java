package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityChangePasswordBinding;
import com.fairfield.model.UserModel;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity {

    ActivityChangePasswordBinding binding;
    private Context mContext;
    private Gson gson;
    private App app;
    private UserModel userModel;
    private HashMap<String, String> hashMap;
    private PreferenceHelper preferenceHelper;
    private static final String TAG = "ChangePassword";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        mContext = this;
        preferenceHelper = new PreferenceHelper(mContext);
        gson = new Gson();
        app = App.getInstance();
        userModel = new UserModel();


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(" ");
        }
        binding.myToolbar.getBackground().setAlpha(0);
        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_white));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        binding.btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });
    }

    private void submitForm() {

        if (!validatePassword()) {
            return;
        }
        if (!validateNewPassword()) {
            return;
        }
        if (!validateConfirmPassword()) {
            return;
        }
        if (!comparePassword()) {
            return;
        }
        if (ConnectivityReceiver.isConnected()) {

            changePassword();
        } else {
            Utility.showSnackBar(mContext, binding.btnSignup, getResources().getString(R.string.no_internet));
        }

    }

    private void changePassword() {
        String oldpassword = binding.inputOldPassword.getText().toString().trim();
        String newpassword = binding.inputNewPassword.getText().toString().trim();
        String confirmpassword = binding.inputConfirmpassword.getText().toString().trim();
        hashMap = new HashMap<String, String>();

        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("old_password", oldpassword);
        hashMap.put("confirm_password", newpassword);
        hashMap.put("new_password", confirmpassword);


        Utility.showProgressHUD(mContext);
        Constant.retrofitService.changepassword(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utility.hideProgressHud();
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();


                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("status")) {

                            userModel = gson.fromJson(jsonObject.getString("data"), UserModel.class);
                            preferenceHelper.putUserId(userModel.getUserId());
                            preferenceHelper.putEmail(userModel.getEmail());
                            preferenceHelper.putStatus(userModel.getStatus());
                            app.userModel = userModel;


                            Utility.showLongToast(jsonObject.getString("message"), mContext);

                            Intent intent = new Intent(mContext, DashBoardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        } else {
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    Utility.showAlert(mContext, "Please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.hideProgressHud();

            }
        });
    }

    private boolean validatePassword() {
        if (binding.inputNewPassword.getText().toString().trim().isEmpty()) {

            Utility.showAlert(mContext, getString(R.string.err_msg_password));
            //  binding.inputLayoutPassword.setError(getString(R.string.err_msg_password));
            //  requestFocus(binding.inputPassword);
            return false;

        } else if (binding.inputNewPassword.getText().toString().trim().length() <= 5) {
            Utility.showAlert(mContext, "Please must enter 6 digit password!");

            return false;
        }

        // binding.inputLayoutPassword.setErrorEnabled(false);
        return true;
    }

    private boolean validateNewPassword() {
        if (binding.inputNewPassword.getText().toString().trim().isEmpty()) {

            Utility.showAlert(mContext, getString(R.string.err_msg_password));
            //  binding.inputLayoutPassword.setError(getString(R.string.err_msg_password));
            //  requestFocus(binding.inputPassword);
            return false;

        } else if (binding.inputNewPassword.getText().toString().trim().length() <= 5) {
            Utility.showAlert(mContext, "Please must enter 6 digit password!");

            return false;
        }

        // binding.inputLayoutPassword.setErrorEnabled(false);
        return true;
    }

    private boolean comparePassword() {

        if (!binding.inputNewPassword.getText().toString().trim().equals(binding.inputConfirmpassword.getText().toString().trim())) {
            Utility.showAlert(mContext, getString(R.string.notmatchpassword));

            return false;

        } else {

        }
        return true;
    }

    private boolean validateConfirmPassword() {
        if (binding.inputConfirmpassword.getText().toString().trim().isEmpty()) {

            Utility.showAlert(mContext, getString(R.string.err_msg_password));
            return false;

        } else {
        }
        return true;
    }
}
