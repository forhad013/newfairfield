package com.fairfield.activity;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fairfield.R;
import com.fairfield.adapter.FeedbackCommentAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityProviderDetailsBinding;
import com.fairfield.databinding.ItemProviderBannerBinding;
import com.fairfield.databinding.ItemProviderCouponsBinding;
import com.fairfield.model.Bannerdatum;
import com.fairfield.model.CommentModel;
import com.fairfield.model.Coupondatum;
import com.fairfield.model.FeedbackModel;
import com.fairfield.model.ProvidersModel;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.MarshMallowPermission;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.fairfield.widgets.CropImage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import me.crosswall.lib.coverflow.CoverFlow;
import me.crosswall.lib.coverflow.core.PageItemClickListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.System.currentTimeMillis;

public class ProviderDetails extends AppCompatActivity {

    private ActivityProviderDetailsBinding binding;
    private Context mContext;
    App app;
    private static final String TAG = "ProviderDetails";
    private PreferenceHelper preferenceHelper;
    List<File> filesimage;
    CommentModel commentModel;
    private String FileImagePath = null;
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x3;
    static final int REQUEST_TAKE_PHOTO = 1;
    public static final int SELECT_MESSAGES_CAMERA = 0x007;
    private Gson gson;
    public int position = 0;
    private String FileSize = "0";
    private Uri fileUri;
    private File mFileTemp = new File(Environment.getExternalStorageDirectory() + "/photo.jpg");
    private MarshMallowPermission marshMallowPermission;
    private FeedbackCommentAdapter adapter;
    private ArrayList<FeedbackModel> feedbackModelArrayList;
    int imagecount = 0;

    int token = 0;
    int imageposton = 0;

    private PagerAdapter banneradatpter;
    PagerAdapter adapterCoupons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        gson = new Gson();
        app = App.getInstance();
        preferenceHelper = new PreferenceHelper(mContext);
        marshMallowPermission = new MarshMallowPermission(mContext);
        commentModel = new CommentModel();
        filesimage = new ArrayList<>();
        feedbackModelArrayList = new ArrayList<>();
        if (!feedbackModelArrayList.isEmpty()) {
            feedbackModelArrayList.clear();
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_provider_details);
        binding.setProvider(app.providersModel);

        setSupportActionBar(binding.myToolbar);

        try {
            token = getIntent().getIntExtra("token", 0);
        }catch (Exception e){

        }


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String result = extras.getString("From", "From"); // This line modified
            String OWNERID = extras.getString("OWNERID");
            System.out.println("yeah" + result);
            setProviderData(OWNERID);

        } else {
            showfeedbackComment();
            setPageView();
        }
        binding.textviewWebsite.setPaintFlags(binding.textviewWebsite.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

       /* String from = getIntent().getExtras().getString("From", "From");

        Log.e(TAG, "onCreate() called with: from = [" + from + "]");


        if (from.equals("Ad")) {

            Log.e(TAG, "onCreate() called with: savedInstanceState = [" + getIntent().getExtras().getString("From") + "]");

        } else {

            showfeedbackComment();
            setPageView();
        }
*/

        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(token==1){
                    Intent intent = new Intent(mContext, DashBoardActivity.class);
                    startActivity(intent);
                }else {
                    finish();
                }
            }
        });


        binding.recyclerViewFeedback.setLayoutManager(new GridLayoutManager(mContext, 1));
        binding.recyclerViewFeedback.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerViewFeedback.setNestedScrollingEnabled(false);
        binding.recyclerViewFeedback.setHasFixedSize(false);

        adapter = new FeedbackCommentAdapter(mContext, feedbackModelArrayList);
        binding.recyclerViewFeedback.setAdapter(adapter);


        // PagerContainer mContainer = (PagerContainer) findViewById(R.id.pager_container);


        final ViewPager pagerBanners = binding.pagerContainerBanners.getViewPager();
        banneradatpter = new MyBannerAdapter(mContext, app.providersModel.getBannerdata());
        pagerBanners.setAdapter(banneradatpter);
        banneradatpter.notifyDataSetChanged();
//        pagerBanners.setOffscreenPageLimit(banneradatpter.getCount());
//        pagerBanners.setClipChildren(false);


//        binding.pagerContainerBanners.setPageItemClickListener(new PageItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//
//                Toast.makeText(ProviderDetails.this, "position:" + position, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        boolean showRotate = getIntent().getBooleanExtra("showRotate", true);
//        if (showRotate) {
//            new CoverFlow.Builder()
//                    .with(pagerBanners)
//                    .scale(0.3f)
//                    .pagerMargin(0f)
//                    .spaceSize(0f)
//                    .rotationY(25f)
//                    .build();
//        }


        final ViewPager pagerCoupons = binding.pagerContainerCoupons.getViewPager();
        adapterCoupons = new MyCouponAdapter(mContext, app.providersModel.getCoupondata());
        pagerCoupons.setAdapter(adapterCoupons);
        adapterCoupons.notifyDataSetChanged();
//        pagerCoupons.setOffscreenPageLimit(adapterCoupons.getCount());
//        pagerCoupons.setClipChildren(false);

//        boolean showRotatecoupon = getIntent().getBooleanExtra("showRotate", true);
//        if (showRotatecoupon) {
//            new CoverFlow.Builder()
//                    .with(pagerCoupons)
//                    .scale(0.3f)
//                    .pagerMargin(0f)
//                    .spaceSize(0f)
//                    .rotationY(25f)
//                    .build();
//        }

        //-----------------------------------------------------image pick from gallery and camera---------------------------------------------------------

        binding.fabPickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (marshMallowPermission.isPermissionForReadExtertalStorage()) {

                    if (filesimage.size() <= 4) {

                        openGallery();
                    } else {
                        Utility.showSnackBar(mContext, binding.edittextTitle, "Please select minimum five image");
                        return;
                    }

                } else {
                    try {
                        marshMallowPermission.requestPermissionForReadExtertalStorage();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        binding.fabPickCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (marshMallowPermission.isPermissionForCamera_And_ReadExternal()) {


                    if (filesimage.size() <= 4) {

                        takePicture();
                    } else {
                        Utility.showSnackBar(mContext, binding.edittextTitle, "Please select minimum five image");
                        return;
                    }


                } else {
                    try {
                        marshMallowPermission.requestPermissionForCamera_And_ReadExternal();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        binding.btnShowmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {

                    Intent intent = new Intent(mContext, ShowMoreFeedbackActivity.class);
                    startActivity(intent);
                } else {
                    Utility.showSnackBar(mContext, binding.btnSubmitcomment, getResources().getString(R.string.no_internet));
                }
            }
        });


        binding.btnReviewRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectivityReceiver.isConnected()) {
                    sendReviewRequest();
                } else {
                    Utility.showSnackBar(mContext, binding.btnSubmitcomment, getResources().getString(R.string.no_internet));
                }


            }
        });

        binding.llWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = app.providersModel.getWeblink();

                Intent intent = new Intent(mContext, NewsWebview.class);
                intent.putExtra("url", url);
                intent.putExtra("texttoolbar", mContext.getResources().getString(R.string.website));
                mContext.startActivity(intent);
            }
        });


        binding.btnSubmitcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {


                    long currentTime = Calendar.getInstance().getTimeInMillis();

                    Log.e(TAG, "onResume:currentTime " + currentTime);

                    Log.e(TAG, "ClickTime  " + preferenceHelper.getClickTime());
                    Log.e(TAG, "onResume: DAY_IN_MILLIS " + DateUtils.DAY_IN_MILLIS);
                    upload();

                  /*  if (preferenceHelper.getClickTime() == 0) {

                        upload();
                    } else {

                        if (preferenceHelper.getClickTime() >= currentTime) {

                            Log.e(TAG, "onClick:  in if>>>>>");
                            //  upload();
                        } else {

                            Log.e(TAG, "onClick:  in else if >>>>>");
                        }
                    }
*/

                } else {
                    Utility.showSnackBar(mContext, binding.btnSubmitcomment, getResources().getString(R.string.no_internet));
                }

            }
        });
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture() {


        String fileName = currentTimeMillis() + ".jpg";
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, SELECT_MESSAGES_CAMERA);

       /* Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        startActivityForResult(cameraIntent, SELECT_MESSAGES_CAMERA);*/
    }


    public void adddyamicimage(Bitmap scaled) {

        final FrameLayout deleteframeLayout = new FrameLayout(mContext);
        LinearLayout.LayoutParams frahmelayoutParams = new LinearLayout.LayoutParams(300, 300);
        deleteframeLayout.setLayoutParams(frahmelayoutParams);


        ImageView imageview_select = new ImageView(mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
        imageview_select.setLayoutParams(layoutParams);
        imageview_select.setImageBitmap(scaled);
        imageview_select.setPadding(5, 5, 5, 5);
        imageview_select.setScaleType(ImageView.ScaleType.FIT_XY);


        ImageView delete = new ImageView(mContext);
        LinearLayout.LayoutParams imageview_crossParams = new LinearLayout.LayoutParams(50, 50);
        delete.setLayoutParams(imageview_crossParams);
        delete.setImageResource(R.drawable.deleticon);
        delete.setId(imageposton);
        delete.setTag(filesimage.get(imageposton).getName());

        int pos = imageposton;

        Log.e(TAG, "imageposton: " + pos);
        Log.e(TAG, "imageposton: " + delete.getId());
        imageposton++;

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e(TAG, "id in GALLERY>>>>>>>>>>>: " + view.getId());
                Log.e(TAG, "id in GALLERY>>>>>>>>>>>: " + view.getTag());

                int postition = 0;

                for (int i = 0; i < commentModel.getImages().size(); i++) {


                    if (view.getTag().equals(commentModel.getImages().get(i).getName())) {

                        commentModel.getImages().remove(i);
                        Log.e(TAG, "filesimage: " + filesimage.size());
                        Log.e(TAG, "commentModel: " + commentModel.getImages().toString());

                        deleteframeLayout.setVisibility(View.GONE);
                        imageposton--;
                    } else {

                        Log.e(TAG, "onClick: >>>>>>>in else image");
                    }
                }

              /* filesimage.remove(view.getId());
                synchronized(filesimage){
                    filesimage.notify();
                }*/


            }
        });


        deleteframeLayout.addView(imageview_select);
        deleteframeLayout.addView(delete);
        binding.llMain.addView(deleteframeLayout);

        binding.scrollviewProviderdetails.post(new Runnable() {
            @Override
            public void run() {
                binding.scrollviewProviderdetails.fullScroll(View.FOCUS_DOWN);
            }
        });

    }


    private boolean validateTitle() {
        if (binding.edittextTitle.getText().toString().trim().isEmpty()) {

            Utility.showSnackBar(mContext, binding.edittextTitle, getResources().getString(R.string.err_msg_title));

            //  Utility.showAlert(mContext, getString(R.string.err_msg_title));
            return false;
        }
        return true;
    }

    private boolean validateDesc() {
        if (binding.edittextDesc.getText().toString().trim().isEmpty()) {

            Utility.showSnackBar(mContext, binding.edittextDesc, getResources().getString(R.string.err_msg_des));
            //  Utility.showAlert(mContext, getString(R.string.err_msg_des));
            return false;
        }
        return true;
    }

    private boolean validaterating() {
        Log.e(TAG, "validaterating: " + binding.ratingbar.getRating());

        if (binding.ratingbar.getRating() == 0) {

            Utility.showSnackBar(mContext, binding.edittextDesc, getResources().getString(R.string.err_msg_rating));

            //     Utility.showAlert(mContext, getString(R.string.err_msg_rating));
            return false;
        }
        return true;
    }

    private void upload() {

        if (!validaterating()) {
            return;
        }

        if (!validateTitle()) {
            return;
        }
        if (!validateDesc()) {
            return;
        }


        try {
            Utility.showProgressHUD(mContext);
            // String comment = binding.editComment.getText().toString();

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);


            if (!filesimage.isEmpty()) {

                for (int i = 0; i < filesimage.size(); i++) {
                    builder.addFormDataPart("images[]", commentModel.getImages().get(i).getName(), RequestBody.create(MediaType.parse("image/*"), commentModel.getImages().get(i)));
                }
            }
            if (!filesimage.isEmpty()) {
                filesimage.clear();
                commentModel = new CommentModel();
            }


            builder.addFormDataPart(Constant.USER_ID, preferenceHelper.getUserId());
            builder.addFormDataPart("owner_id", app.providersModel.getOwnerId());
            builder.addFormDataPart("rating", "" + binding.ratingbar.getRating());
            builder.addFormDataPart("review", binding.edittextDesc.getText().toString());
            builder.addFormDataPart("title", binding.edittextTitle.getText().toString());
            final MultipartBody requestBody = builder.build();
            Constant.retrofitService.submitRating_Review(requestBody).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        try {
                            if (response.isSuccessful()) {
                                String res = response.body().string();
                                Log.e(TAG, "onResponse: " + res);


                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getBoolean("status")) {
                                    binding.llMain.removeAllViews();
                                    binding.edittextTitle.setText("");
                                    binding.edittextDesc.setText("");
                                    binding.ratingbar.setRating(0);
                                    imageposton = 0;

                                    Utility.hideProgressHud();

                                    Calendar clicktime = Calendar.getInstance();
                                    long time = clicktime.getTimeInMillis();

                                    Log.e(TAG, "Click time:>>>>>>>>>>>>>>>>>>>>>>>> " + time);

                                    preferenceHelper.putClickTime(time);


                                    Utility.showSnackBar(mContext, binding.btnSubmitcomment, getResources().getString(R.string.successfully_submit_review));
                                   setProviderData(app.providersModel.getOwnerId());

                                } else {


                                    binding.llMain.removeAllViews();
                                    binding.edittextTitle.setText("");
                                    binding.edittextDesc.setText("");
                                    binding.ratingbar.setRating(0);
                                    imageposton = 0;
                                    Utility.hideProgressHud();

                                    Utility.showSnackBar(mContext, binding.edittextTitle, jsonObject.getString("message"));
                                }


                            } else {
                                binding.llMain.removeAllViews();
                                //   binding.editComment.setText("");
                                Utility.hideProgressHud();
                                Utility.showAlert(mContext, "Please try after some time.");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");

        File file = null;
        if (resultCode != RESULT_OK) {

            return;
        }
        switch (requestCode) {

            case REQUEST_CODE_GALLERY:

                try {

                    try {
                        Uri selectedImageUri = data.getData();
                        String s1 = data.getDataString();
                        Log.e("GetPath", s1);
                        Log.e("OK", "" + selectedImageUri);

                        String selectedImagePath = getPath(selectedImageUri);
                        if (selectedImagePath == null && s1 != null) {
                            selectedImagePath = s1.replaceAll("file://", "");
                        }

                        long totalSpace = 0;

                        totalSpace = new File(selectedImagePath).length() / 1000;

                        Log.e(TAG, "onActivityResult: " + totalSpace);
                        if (totalSpace > 150) {
                            filesimage.add(new File(compressImage(selectedImagePath)));
                            commentModel.setImages(filesimage);
                        } else {
                            filesimage.add(new File(selectedImagePath));
                            commentModel.setImages(filesimage);
                        }

                        Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(selectedImagePath));
                        int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);


                        adddyamicimage(scaled);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {


                    Log.e(TAG, "Error while creating temp file", e);
                }

                break;

            case SELECT_MESSAGES_CAMERA:

                try {
                    if (getPath(fileUri) != null) {
                        FileImagePath = getPath(fileUri);
                        System.out.println("Image Path >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>: " + FileImagePath);
                        Log.e(TAG, "SELECT_MESSAGES_CAMERA: in if");
                        //   FileImagePath = getPath(getApplicationContext(), data.getData());
                        if (FileImagePath != null) {
                            file = new File(FileImagePath);
                        }
                        if (file != null) {
                            FileSize = String.valueOf(file.length());

                        }
                        long totalSpace = 0;

                        totalSpace = new File(FileImagePath).length() / 1000;

                        Log.e(TAG, "onActivityResult: " + totalSpace);
                        if (totalSpace > 150) {
                            filesimage.add(new File(compressImage(FileImagePath)));
                            commentModel.setImages(filesimage);
                        } else {
                            filesimage.add(file);
                            commentModel.setImages(filesimage);
                        }

                        Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(FileImagePath));
                        int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);


                        adddyamicimage(scaled);


                      /*  ImageView iv = new ImageView(mContext);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
                        iv.setLayoutParams(layoutParams);
                        iv.setImageBitmap(scaled);
                        iv.setPadding(5, 5, 5, 5);
                        iv.setScaleType(ImageView.ScaleType.FIT_XY);
                        binding.llMain.addView(iv);*/

                    } else {

                        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                        String cachePath = ""; // you still need a default value if not mounted
                        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !Environment.isExternalStorageRemovable()) {
                            if (getExternalCacheDir() != null) {
                                cachePath = getExternalCacheDir().getPath(); // most likely your null value
                            }
                        } else {
                            if (mContext.getCacheDir() != null) {
                                cachePath = mContext.getCacheDir().getPath();
                            }
                        }

                        String path = cachePath + "" + Calendar.getInstance().getTimeInMillis() + "";

                        File imageFile = savebitmap(bitmap, path);
                        if (imageFile.exists()) {
                            FileImagePath = imageFile.getPath();
                            file = new File(FileImagePath);
                            FileSize = String.valueOf(file.length());
                            long totalSpace = 0;

                            totalSpace = new File(FileImagePath).length() / 1000;

                            Log.e(TAG, "onActivityResult: " + totalSpace);
                            if (totalSpace > 150) {
                                filesimage.add(new File(compressImage(FileImagePath)));
                                commentModel.setImages(filesimage);
                            } else {
                                filesimage.add(file);
                                commentModel.setImages(filesimage);
                            }


                            Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(FileImagePath));
                            int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                            Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);


                            adddyamicimage(scaled);
                          /*  ImageView iv = new ImageView(mContext);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
                            iv.setLayoutParams(layoutParams);
                            iv.setImageBitmap(scaled);
                            iv.setPadding(5, 5, 5, 5);
                            iv.setScaleType(ImageView.ScaleType.FIT_XY);
                            binding.llMain.addView(iv);*/
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e);
                    return;
                }


                break;
            case REQUEST_CODE_CROP_IMAGE:

                try {
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {

                        return;
                    }

                    long totalSpace = 0;
                    totalSpace = new File(path).length() / 1000;
                    Log.e(TAG, "onActivityResult: " + totalSpace);

                    if (totalSpace > 150) {
                        filesimage.add(new File(compressImage(path)));
                        commentModel.setImages(filesimage);
                    } else {
                        filesimage.add(new File(path));
                        commentModel.setImages(filesimage);
                    }

                    Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(path));
                    int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);


                /*    ImageView iv = new ImageView(mContext);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
                    iv.setLayoutParams(layoutParams);
                    iv.setImageBitmap(bitmapImage);
                    iv.setPadding(5, 5, 5, 5);
                    iv.setScaleType(ImageView.ScaleType.FIT_XY);
                    binding.llMain.addView(iv);*/
                    adddyamicimage(scaled);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Return the path of the file.
    public String getPath(Uri uri) {

        try {
            String[] projection = {MediaStore.Images.Media.DATA};
            Log.e("OK 1", "" + projection);
            Cursor cursor = managedQuery(uri, projection, null, null, null);
            Log.e("OK 2", "" + cursor);
            if (cursor == null) {
                return null;
            }
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            Log.e("OK 3", "" + column_index);
            cursor.moveToFirst();
            Log.e("OK 4", "" + cursor.getString(column_index));
            return cursor.getString(column_index);
        } catch (Exception e) {
            Toast.makeText(mContext, "Image is too big in resolution please try again", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == marshMallowPermission.CAMERA_READ_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (filesimage.size() <= 4) {

                    takePicture();
                } else {
                    Utility.showSnackBar(mContext, binding.edittextTitle, "Please select minimum five image");
                    return;
                }


            } else {
                Toast.makeText(mContext, "Please accept permission", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == marshMallowPermission.READ_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                showDialog(new String[]{"Gallery"});

                if (filesimage.size() <= 4) {

                    openGallery();
                } else {
                    Utility.showSnackBar(mContext, binding.edittextTitle, "Please select minimum five image");
                    return;
                }

            } else {
                Toast.makeText(mContext, "Please accept permission", Toast.LENGTH_SHORT).show();
            }
        }

    }

    //-----------------------------------------------------------------------------------feedback---------------------------------------------------------------------------------------------------
    private void showfeedbackComment() {

        final HashMap<String, String> meMap = new HashMap<String, String>();
        meMap.put("owner_id", app.providersModel.getOwnerId());
        Constant.retrofitService.showFeedbackComments(meMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responsestring = response.body().string();

                        final JSONObject jsonObject = new JSONObject(responsestring);
                        if (jsonObject.getBoolean("status")) {
                            Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");


                            if (!feedbackModelArrayList.isEmpty()) {
                                feedbackModelArrayList.clear();
                            }
                            final Type type = new TypeToken<List<FeedbackModel>>() {
                            }.getType();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    try {
                                        feedbackModelArrayList = gson.fromJson(jsonObject.getString("data"), type);

                                        binding.textviewMrcr.setVisibility(View.VISIBLE);
                                        binding.viewBelowRecyleview.setVisibility(View.VISIBLE);

                                        if (feedbackModelArrayList.size() > 1) {
                                            binding.llShowmore.setVisibility(View.VISIBLE);

                                        }

                                        Log.e("ArrayList", "" + feedbackModelArrayList.size());
                                        adapter.setFeedbackModelList(feedbackModelArrayList.subList(0, 1));
                                        adapter.notifyDataSetChanged();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else {
                            binding.llShowmore.setVisibility(View.GONE);
                            // Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        binding.llShowmore.setVisibility(View.GONE);

                        //Utility.showAlert(mContext, "Please try after some time.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    //------------------------------------------------------------coupans------------------------------------------------------------------------------------------------------------------------------


    private void sendReviewRequest() {
        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put("owner_id", app.providersModel.getOwnerId());
        hashMap.put("user_id", preferenceHelper.getUserId());

        Utility.showProgressHUD(mContext);

        Constant.retrofitService.reviewRequest(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getBoolean("status")) {

                            Utility.hideProgressHud();
                            Utility.showSnackBar(mContext, binding.btnReviewRequest, "Review Request successfully submitted");

                        } else {
                            Utility.hideProgressHud();
                            Utility.showSnackBar(mContext, binding.btnReviewRequest, jsonObject.getString("message"));

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    Utility.hideProgressHud();
                    Utility.showSnackBar(mContext, binding.btnReviewRequest, "please try after some time");
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    //-----------------------------------------------------------------Review request--------------------------------------------------------------------------------------------------


    private class MyBannerAdapter extends PagerAdapter {


        private Context mContext;
        private List<Bannerdatum> bannerList;

        private ItemProviderBannerBinding binding;


        public MyBannerAdapter(Context mContext, List<Bannerdatum> bannerList) {

            this.mContext = mContext;
            this.bannerList = bannerList;
        }


        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);

            binding = DataBindingUtil.inflate(inflater, R.layout.item_provider_banner, container, false);
            binding.setBanner(this.bannerList.get(position));

            View view = binding.getRoot();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    app.banners = app.providersModel.getBannerdata();
                    app.bannerIndex = position;

                    Intent intent = new Intent(mContext, ShowFullBanner.class);
                    startActivity(intent);
                }
            });
//            TextView view = new TextView(mContext);
//            view.setText("Item "+position);
//            view.setGravity(Gravity.CENTER);
//            view.setBackgroundColor(Color.argb(255, position * 50, position * 10, position * 50));

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return bannerList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }
    }

    //------------------------------------------Banners-------------------------------------------------------------------------------------------------------------------------------------------------
    private class MyCouponAdapter extends PagerAdapter {


        private Context mContext;
        private List<Coupondatum> couponList;

        private ItemProviderCouponsBinding binding;


        public MyCouponAdapter(Context mContext, List<Coupondatum> couponList) {
            this.mContext = mContext;
            this.couponList = couponList;
        }


        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);

            binding = DataBindingUtil.inflate(inflater, R.layout.item_provider_coupons, container, false);
            binding.setCoupon(this.couponList.get(position));

            View view = binding.getRoot();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    app.coupons = app.providersModel.getCoupondata();
                    app.couponIndex = position;
                    app.showBusinessInfo = false;

                    Intent intent = new Intent(mContext, ShowFullCoupon.class);
                    startActivity(intent);
                }
            });
//            TextView view = new TextView(mContext);
//            view.setText("Item "+position);
//            view.setGravity(Gravity.CENTER);
//            view.setBackgroundColor(Color.argb(255, position * 50, position * 10, position * 50));

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return couponList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }
    }


    //-------------------------------------------------------------------------------------------file comress---------------------------------------------------------------------------------
    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    public static String compressImageShow(Context mContext, String imagePath) {
        final float maxHeight = 640.0f;
        final float maxWidth = 640.0f;

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        String path = MediaStore.Images.Media.insertImage(mContext.getContentResolver(), scaledBitmap, "Title", null);

//        return out.toString();
        return path;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     */
    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {


        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static void copyFile(File src, File dst) throws IOException {
        FileInputStream var2 = new FileInputStream(src);
        FileOutputStream var3 = new FileOutputStream(dst);
        byte[] var4 = new byte[1024];

        int var5;
        while ((var5 = var2.read(var4)) > 0) {
            var3.write(var4, 0, var5);
        }
        var2.close();
        var3.close();
    }

    private File savebitmap(Bitmap bitmap, String filename) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;

        File file = new File(filename + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");
            Log.e("file exist", "" + file + ",Bitmap= " + filename);
        }
        try {
            // make a new bitmap from your file
//            Bitmap bitmap = BitmapFactory.decodeFile(file.getName());

            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("file", "" + file);
        return file;
    }

    private void setPageView() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("owner_id", app.providersModel.getOwnerId());

        Constant.retrofitService.setpageViewer(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private void setProviderData(String id) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("owner_id", id);


        Log.e(TAG, "setProviderData() called with: id = [" + id + "]");

        Constant.retrofitService.getProviderDetails(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        String responseString = response.body().string();

                        Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + responseString + "]");

                        JSONObject jsonObject = new JSONObject(responseString);
                        if (jsonObject.getBoolean("status")) {

                            app.providersModel = gson.fromJson(jsonObject.getString("data"), ProvidersModel.class);
                            binding.setProvider(app.providersModel);


                            final ViewPager pagerBanners = binding.pagerContainerBanners.getViewPager();
                            banneradatpter = new MyBannerAdapter(mContext, app.providersModel.getBannerdata());
                            pagerBanners.setAdapter(banneradatpter);
                            banneradatpter.notifyDataSetChanged();
//                            pagerBanners.setOffscreenPageLimit(banneradatpter.getCount());
//                            pagerBanners.setClipChildren(false);


                            final ViewPager pagerCoupons = binding.pagerContainerCoupons.getViewPager();
                            adapterCoupons = new MyCouponAdapter(mContext, app.providersModel.getCoupondata());
                            pagerCoupons.setAdapter(adapterCoupons);
                            adapterCoupons.notifyDataSetChanged();
//                            pagerCoupons.setOffscreenPageLimit(adapterCoupons.getCount());
//                            pagerCoupons.setClipChildren(false);


                            showfeedbackComment();
                            setPageView();

                        } else {
                            //Utility.showAlert(mContext, jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // Utility.showAlert(mContext, "Please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();


    }
}
