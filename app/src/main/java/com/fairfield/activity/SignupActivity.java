package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivitySignupBinding;
import com.fairfield.fragment.SiginFragment;
import com.fairfield.fragment.SignupFragment;
import com.fairfield.model.SocialMediaProfile;
import com.fairfield.model.UserModel;
import com.fairfield.utility.CheckNetworkConnectivity;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    ActivitySignupBinding activitySignupBinding;
    Context mContext;
    Gson gson;
    private CallbackManager callbackManager;
    private static final String TAG = "SignupActivity";
    private int RC_SIGN_IN = 101;
    private AccessToken accessToken;
    private CheckNetworkConnectivity networkConnectivity;
    private String loginType = Constant.MANUAL;
    private String sFirstName, sLastName, sEmailId, sPassword, sUserName, sSocial_unique_id, pictureUrl;
    private String sPictureUrl;
    private SocialMediaProfile mediaProfile;
    App app;
    private UserModel userModel;
    private PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);

        activitySignupBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        intview();
        setupTabIcons();

        activitySignupBinding.btnFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ConnectivityReceiver.isConnected()) {
                    try {
                        clickFb();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    Utility.showSnackBar(mContext, activitySignupBinding.btnFb, "No internet connection.");
                }


            }
        });
    }

    private void intview() {
        mContext = this;
        gson = new Gson();
        networkConnectivity = new CheckNetworkConnectivity(mContext);
        app = App.getInstance();
        userModel = new UserModel();
        preferenceHelper = new PreferenceHelper(mContext);
        setupViewPager(activitySignupBinding.viewpager);
        activitySignupBinding.tablayout.setupWithViewPager(activitySignupBinding.viewpager);
        Utility.printKeyHash(this);
        callbackManager = CallbackManager.Factory.create();

      /*  String checkBoxText = "I agree to all the <a href='https://www.localneighborhoodapp.com/terms_condition' > Terms and Conditions</a>";

        activitySignupBinding.checkbox.setText(Html.fromHtml(checkBoxText));
        activitySignupBinding.checkbox.setMovementMethod(LinkMovementMethod.getInstance());*/


        int defaultValue = 0;
        int page = getIntent().getIntExtra(Constant.STARTFLOW, defaultValue);
        activitySignupBinding.viewpager.setCurrentItem(page);
    }

    private void setupTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("SIGN IN ");
        activitySignupBinding.tablayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("SIGN UP");
        activitySignupBinding.tablayout.getTabAt(1).setCustomView(tabTwo);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SiginFragment(), "ONE");
        adapter.addFragment(new SignupFragment(), "TWO");


        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void clickFb() {


        /*if (!activitySignupBinding.checkbox.isChecked()){

            Utility.showAlert(mContext, getString(R.string.err_checkterm));
            return;
        }*/


        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.e(TAG, "onSuccess() called with: " + "loginResult = [" + loginResult.getAccessToken() + "]");
                Log.e(TAG, "Token::" + loginResult.getAccessToken().getToken());
                accessToken = loginResult.getAccessToken();
                getFacebookUserInformation();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e("Error in login  188", "" + exception);

                if (exception instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });
    }

    void getFacebookUserInformation() {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                        try {

                            if (jsonObject != null && graphResponse != null) {

                                Utility.appLog("Json Object", jsonObject.toString());
                                Utility.appLog("Graph response", graphResponse.toString());
                                try {
                                    sUserName = jsonObject.getString("name");
                                    sEmailId = jsonObject.getString("email");
                                    sSocial_unique_id = jsonObject.getString("id");
                                    sPictureUrl = "https://graph.facebook.com/" + sSocial_unique_id + "/picture?type=large";
                                    mediaProfile = new SocialMediaProfile();

                                    if (sUserName != null) {
                                        String[] name = sUserName.split(" ");
                                        if (name[0] != null) {
                                            mediaProfile.setFirstName(name[0]);
                                        }
                                        if (name[1] != null) {
                                            mediaProfile.setLastName(name[1]);
                                        }
                                    }
                                    mediaProfile.setEmailId(sEmailId);
                                    mediaProfile.setSocialUniqueId(sSocial_unique_id);
                                    mediaProfile.setPictureUrl(sPictureUrl);
                                    mediaProfile.setLoginType(Constant.SOCIAL_FACEBOOK);

                                    Log.e(TAG, "Social_unique_id: " + sSocial_unique_id);

                                    app.userModel.setFromWhere("1");
                                    app.userModel.setSocial_id(sSocial_unique_id);
                                    app.userModel.setFirstName(sUserName);
                                   app.userModel.setEmail(sEmailId);

                                  //  app.userModel.setEmail("");
                                    app.userModel.setDeviceToken("");
                                    app.userModel.setDeviceId(Utility.generateDeviceId(mContext));
                                    app.userModel.setDeviceType(Constant.DEVICE_TYPE_ANDROID);
                                    app.userModel.setLoginType("user");
                                    Utility.appLog("facebookData", mediaProfile.toString());

                                    if (sSocial_unique_id != null) {
                                        loginType = Constant.SOCIAL_FACEBOOK;
                                        facebookLogin();

                                        //  UserLogin(Const.SOCIAL_FACEBOOK);
                                    } else {
                                        Utility.showShortToast("Invalidate Data", SignupActivity.this);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        /*    Log.e(TAG, "onCompleted() called with: " + "object = [" + object.toString() + "], response = [" + response + "]");

                            final String name = object.getString("name");
                            final String id = object.getString("id");
                            String email = object.getString("email");

                            JSONObject jsonObject = object.getJSONObject("picture");
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            final String url = jsonObject1.getString("url");
                            String fbImg = "https://graph.facebook.com/" + id + "/picture?width=9999&height=9999";
                            //   setInfo(name, id, email, fbImg, "fb");

                            Intent intent = new Intent(mContext, DashBoardActivity.class);
                            startActivity(intent);
                            finish();
*/
                    }
                });


        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,locale,hometown,email,gender,birthday,location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.e(TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data.getExtras().toString() + "]");
            if (requestCode == RC_SIGN_IN) {
                if (resultCode == RESULT_OK) {
                }
            } else {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void facebookLogin() {

        Log.e(TAG, "userModel" + app.userModel);

        Utility.showProgressHUD(mContext);
        Constant.retrofitService.facebooklogin(Utility.getModeltoMap(app.userModel)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utility.hideProgressHud();
                if (response.isSuccessful()) {
                    try {
                        String responseString = response.body().string();

                        Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + responseString + "]");

                        JSONObject jsonObject = new JSONObject(responseString);
                        if (jsonObject.getBoolean("status")) {

                            userModel = gson.fromJson(jsonObject.getString("data"), UserModel.class);
                            app.userModel = userModel;


                            preferenceHelper.putUserId(userModel.getUserId());
                            preferenceHelper.putEmail(userModel.getEmail());
                            preferenceHelper.putLoginBy(Constant.BYFACEBOOK);
                            preferenceHelper.putStatus(userModel.getStatus());
                            preferenceHelper.putSocialId(userModel.getSocial_id());

                            Log.e(TAG, " userModel = [" + userModel + "]");


                            if (jsonObject.getString("email_flag").equals("0")) {

                                Intent intent = new Intent(mContext, FacebookloginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();

                            } else {
                                Intent intent = new Intent(mContext, DashBoardActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(mContext, "Please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.hideProgressHud();
            }
        });
    }
}
