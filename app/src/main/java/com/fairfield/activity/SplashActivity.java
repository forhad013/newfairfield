package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.utility.CheckNetworkConnectivity;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "SplashActivity";
    private ImageView imageView;
    private App app;

    //    private BroadcastReceiver mRegistrationBroadcastReceiver;
//    private ProgressBar mRegistrationProgressBar;
    //    private TextView mInformationTextView;
//    private boolean isReceiverRegistered;
    protected boolean _active = true, isLogin, isBackPress;
    protected int _splashTime = 3000; // Splash screen time
    private Context mContext;
    private CheckNetworkConnectivity mCheckNetworkConnectivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window w = getWindow(); // in Activity's onCreate() for instance
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        setContentView(R.layout.activity_splash);
//        mRegistrationProgressBar = (ProgressBar) findViewById(R.id.registrationProgressBar);
        imageView = (ImageView) findViewById(R.id.imageView);
        mCheckNetworkConnectivity = new CheckNetworkConnectivity(
                getApplicationContext());



        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.get("data")!=null) {


            String datas = bundle.get("data").toString();

            Log.e("data",datas);


            try {
                JSONObject jsonObject = new JSONObject(datas);

                String type = jsonObject.getString("type");

                Intent intent = null;

                if (type.equals("new_reward")) {
                    intent = new Intent(this, RewardActivity.class);
                    intent.putExtra("token",1);
                } else if (type.equals("new_coupon")) {
                    intent = new Intent(this, PramotionActivity.class);
                    intent.putExtra("token",1);
                } else if (type.equals("new_event")) {
                    intent = new Intent(this, LocalEventActivity.class);
                    intent.putExtra("token",1);
                } else if (type.equals("new_review")) {

                    String owner_id = jsonObject.getString("owner_id");

                    intent = new Intent(this, ProviderDetails.class);
                    intent.putExtra("From", "Ad");
                    intent.putExtra("OWNERID", owner_id);
                    intent.putExtra("token",1);
                } else if (type.equals("new_business")) {

                    String owner_id = jsonObject.getString("owner_id");
                    intent = new Intent(this, ProviderDetails.class);
                    intent.putExtra("From", "Ad");
                    intent.putExtra("OWNERID", owner_id);
                    intent.putExtra("token",1);
                } else if (type.equals("review_request")) {

                    String owner_id = jsonObject.getString("owner_id");
                    String requesterId = jsonObject.getString("user_id");

                    intent = new Intent(this, ProviderDetails.class);
                    intent.putExtra("From", "Ad");
                    intent.putExtra("OWNERID", owner_id);
                    intent.putExtra("token",1);

                } else if (type.equals("new_announcement")) {

                    String owner_id = jsonObject.getString("owner_id");
                    String notificationId = jsonObject.getString("id");
                    String image = jsonObject.getString("image");
                    String titleText = jsonObject.getString("title");
                    String bodyText = jsonObject.getString("body");
                    intent = new Intent(this, PopUpActivity.class);
                    intent.putExtra("image", image);
                    intent.putExtra("titleText", titleText);
                    intent.putExtra("bodyText", bodyText);
                    intent.putExtra("notificationId", notificationId);



                }

                startActivity(intent);
                finish();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }else{
            mContext = this;


            Utility.printKeyHash(this);

            Thread splashTread = new Thread() {
                @Override
                public void run() {
                    try {
                        int waited = 0;
                        while (_active && (waited < _splashTime)) {
                            sleep(100);
                            if (_active) {
                                waited += 100;
                                // crateSightings();
                            }
                        }
                    } catch (InterruptedException e) {
                        // do nothing
                    } finally {

                    }
                    runOnUiThread(endSplashThread);
                }
            };
            splashTread.start();

        }




     /*   if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Log.e(TAG, "checkPlayServices() called with: " + "savedInstanceState = [" + savedInstanceState + "]");
            try {
//                Intent intent = new Intent(this, RegistrationIntentService.class);
//                startService(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/



    }

    private Runnable endSplashThread = new Runnable() {
        public void run() {


            if (ConnectivityReceiver.isConnected()) {


                startActivity();
            } else {

                Utility.showSnackBar(mContext, imageView, getResources().getString(R.string.no_internet));

                //  Utility.showSnackBarlengthlong(mContext, "");
            }
        }

    };


    private void startActivity() {

        if (!TextUtils.isEmpty(new PreferenceHelper(mContext).getUserId())) {


            //   Log.e(TAG, "new PreferenceHelper(mContext).getUserId())************" + new PreferenceHelper(mContext).getUserId());
            checkUser();
            // return;
        } else {
            new PreferenceHelper(mContext).putNotification(true);
            Intent intent = new Intent(mContext, GetStartActivity.class);
            startActivity(intent);
            finish();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isBackPress = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    public void checkUser() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", new PreferenceHelper(mContext).getUserId());
        //  Log.e(TAG, "checkUser() called" + hashMap);

        Utility.showProgressHUD(mContext);
        Constant.retrofitService.checkUser(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utility.hideProgressHud();
                if (response.isSuccessful()) {
                    try {
                        String responseString = response.body().string();
                        //           Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + responseString + "]");

                        JSONObject jsonObject = new JSONObject(responseString);
                        if (jsonObject.getBoolean("status")) {


                            if (Constant.MANUAL.equals(new PreferenceHelper(mContext).getLoginBy())) {


                                if (Constant.NONVEARIFY.equals(new PreferenceHelper(mContext).getStaus())) {
                                    startActivity(new Intent(mContext, VarificationActivity.class));
                                    finish();
                                } else {
                                    startActivity(new Intent(mContext, DashBoardActivity.class));
                                    finish();

                                }

                            } else {
                                startActivity(new Intent(mContext, DashBoardActivity.class));
                                finish();
                            }

                        } else {
                            app = new App();
                            Utility.clearAllSharedPreferences(mContext);
                            startActivity(new Intent(mContext, GetStartActivity.class));
                            finish();

                            finish();
                            //Utility.showAlert(mContext, jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(mContext, "Please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.hideProgressHud();
            }
        });
    }


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
  /*  private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }*/


}

