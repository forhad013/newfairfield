package com.fairfield.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fairfield.R;
import com.fairfield.adapter.NewNotificationadapter;

import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityNotificationBinding;
import com.fairfield.model.BannerModel;
import com.fairfield.model.NewNotificationModel;
import com.fairfield.model.NotificationModel;
import com.fairfield.model.ProvidersModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {

    ActivityNotificationBinding binding;
    private PreferenceHelper preferenceHelper;
    private Context mContext;
    private Gson gson;
    private ArrayList<NewNotificationModel> notificationArrayList;
    private NewNotificationadapter adapter;
    private static final String TAG = "NotificationActivity";
    App app;



    private Handler mHandler;
    private Runnable mUpdateResults;
    public int currentimageindex = 0;
    private Timer timer;
    private ArrayList<BannerModel> bannerArrayList;
    private int[] IMAGE_IDS = {R.drawable.banner, R.drawable.btn_fb, R.drawable.like,
            R.drawable.new_logo};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferenceHelper = new PreferenceHelper(this);
        mContext = this;
        gson = new Gson();


        bannerArrayList = new ArrayList<>();
        timer = new Timer();
        mHandler = new Handler();

        notificationArrayList = new ArrayList<>();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        app = App.getInstance();
        setSupportActionBar(binding.myToolbar);

        // fragmentManager = getFragmentManager();
        // fragmentTransaction = fragmentManagerger.SupportFragmentManager().beginTransaction();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        binding.recyclerViewNotification.setLayoutManager(new GridLayoutManager(mContext, 1));
        binding.recyclerViewNotification.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerViewNotification.setNestedScrollingEnabled(false);
        binding.recyclerViewNotification.setHasFixedSize(false);

        adapter = new NewNotificationadapter(mContext, notificationArrayList);
        binding.recyclerViewNotification.setAdapter(adapter);



        getNotification();

        getAllCoupons();
        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ((NewNotificationadapter) binding.recyclerViewNotification.getAdapter()).setOnItemClickListener(new NewNotificationadapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.e(TAG, "onItemClick() called with: view = [" + ((NewNotificationadapter) binding.recyclerViewNotification.getAdapter()) + "], position = [" + position + "]");


                Intent intent = null;
                String type = notificationArrayList.get(position).getType();

                if (type.equals("new_reward")) {
                    intent = new Intent(NotificationActivity.this, RewardActivity.class);
                    startActivity(intent);
                } else if (type.equals("new_coupon")) {
                    intent = new Intent(NotificationActivity.this, PramotionActivity.class);
                    startActivity(intent);
                } else if (type.equals("new_event")) {
                    intent = new Intent(NotificationActivity.this, LocalEventActivity.class);
                    startActivity(intent);
                } else if (type.equals("new_review")) {

                    String owner_id =  notificationArrayList.get(position).getOwnerId()+"";

                    intent = new Intent(NotificationActivity.this, ProviderDetails.class);
                    intent.putExtra("From", "Ad");
                    intent.putExtra("OWNERID", owner_id);
                    startActivity(intent);

                } else if (type.equals("new_business")) {

                    String owner_id =  notificationArrayList.get(position).getOwnerId()+"";
                    intent = new Intent(NotificationActivity.this, ProviderDetails.class);
                    intent.putExtra("From", "Ad");
                    intent.putExtra("OWNERID", owner_id);
                    startActivity(intent);
                } else if (type.equals("review_request")) {

                    String owner_id =  notificationArrayList.get(position).getOwnerId()+"";
                    intent = new Intent(NotificationActivity.this, ProviderDetails.class);
                    intent.putExtra("From", "Ad");
                    intent.putExtra("OWNERID", owner_id);
                    startActivity(intent);
                } else if (type.equals("new_announcement")) {

                    String owner_id =  notificationArrayList.get(position).getOwnerId()+"";
                    String image =    notificationArrayList.get(position).getImage()+"";

                    String title =  notificationArrayList.get(position).getTitle()+"";
                    String msg =    notificationArrayList.get(position).getBody()+"";


                    String orginalLink = Constant.IMAGE_URL+"notifications/"+image;

                    Log.e("orginal",orginalLink);
                    showDialoge(title,msg,orginalLink);


                }


             /*   if (((NewNotificationadapter) binding.recyclerViewNotification.getAdapter()).getNotificationModel().get(position).getFlag().equals("feedback_request")) {

//                    app.ownerid = adapter.getNotificationModel().get(position).getOwnerData().getOwnerId();
//                    Intent intent = new Intent(mContext, FeedbackActivity.class);
//                    intent.putExtra("FROM" ,"notification");
//                    startActivity(intent);

                    app.providersModel = adapter.getNotificationModel().get(position).getOwnerData();
                    Intent intent = new Intent(mContext, ProviderDetails.class);
                    startActivity(intent);

                } else if (((NewNotificationadapter) binding.recyclerViewNotification.getAdapter()).getNotificationModel().get(position).getFlag().equals("new_business")) {

                    app.providersModel = adapter.getNotificationModel().get(position).getOwnerData();
                    Intent intent = new Intent(mContext, ProviderDetails.class);
                    startActivity(intent);

                } else if (((Notificationadapter) binding.recyclerViewNotification.getAdapter()).getNotificationModel().get(position).getFlag().equals("geofencing")) {

                    app.providersModel = adapter.getNotificationModel().get(position).getOwnerData();
                    Intent intent = new Intent(mContext, ProviderDetails.class);
                    startActivity(intent);

                } else if (((NewNotificationadapter) binding.recyclerViewNotification.getAdapter()).getNotificationModel().get(position).getFlag().equals("new_reviews")) {

                    app.providersModel = adapter.getNotificationModel().get(position).getOwnerData();
                    Intent intent = new Intent(mContext, ProviderDetails.class);
                    startActivity(intent);

                } else if (((NewNotificationadapter) binding.recyclerViewNotification.getAdapter()).getNotificationModel().get(position).getFlag().equals("email_recevied")) {

                    Utility.showSnackBar(mContext, binding.recyclerViewNotification, "Check Inbox - Email");
                }
                //    app.localeventsModel = adapter.getLocalEventsModel().get(position);
                //  Log.e(TAG, "onItemClick() Des>>>>>>>>>>>>>>>= [" + app.localeventsModel + "]");

                //   Log.e(TAG, "onItemClick: click ");

//                 Intent intent = new Intent(mContext, CategoryDetail.class);
//                startActivity(intent);

                //    Intent intent = new Intent(mContext, EventDetailsActivity.class);
                // startActivity(intent);
                */
            }
        });




        try {
            binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        binding.slidingimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    BannerModel banner = bannerArrayList.get(currentimageindex - 1);
                    bannerClick(banner.getId(), banner.getOwnerId());
                } catch (Exception e) {
                    e.printStackTrace();
                }



              /*  AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);
                dialog.setView(dialogLayout);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.show();


                ImageView image = (ImageView) dialog.findViewById(R.id.goProDialogImage);
                //  Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),
                //      R.drawable.whygoprodialogimage);

                try {
                    String url = "https://www.localneighborhoodapp.com/images/banner/";
                    String urlmain = url + bannerArrayList.get(currentimageindex - 1).getImage();
                    Log.e(TAG, "AnimateandSlideShow: " + urlmain);
                    Picasso.with(mContext).load(urlmain).into(image);

                } catch (Exception e) {
                    System.out.println(e);
                }
*/

            }
        });
    }




    public void showDialoge( String titleText, String messageText, String image){
        final Dialog dialog = new Dialog(NotificationActivity.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.announce_popup_layout);
        // Set dialog title
        //dialog.setTitle("Category Add");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();



        Button done = (Button) dialog.findViewById(R.id.okBtn);

        TextView title = (TextView) dialog.findViewById(R.id.title);
        ImageView icon = (ImageView) dialog.findViewById(R.id.icon);

        TextView message = (TextView) dialog.findViewById(R.id.message);






        if(image.equals("")){
            icon.setImageResource(R.drawable.fairfield_icon);
        }else{
            Picasso.with(mContext).load(image).into(icon);
        }

        message.setText(messageText);
        title.setText(titleText);

        //   Log.e("text",text);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });



    }



    private void getNotification() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        //log.e("TUNT", preferenceHelper.getUserId());

        startAnim();
        binding.textviewNoNoti.setVisibility(View.GONE);
        Constant.retrofitService.getNotification(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        stopAnim();

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {
                         //   Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<NotificationModel>>() {
                            }.getType();

                            if (!notificationArrayList.isEmpty()) {
                                notificationArrayList.clear();
                            }
                            GsonBuilder builder = new GsonBuilder();
                            builder.registerTypeAdapter(Float.class, new FloatTypeAdapter());
                            Gson gson = builder.create();
                           // notificationArrayList = gson.fromJson(jsonObject.getString("data"), type);

                            String dataString = jsonObject.getString("data");


                            JSONArray jsonArray = new JSONArray(dataString);
                            Log.e("data",jsonArray+"");

                            for(int i=0;i<jsonArray.length();i++){

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                NewNotificationModel newNotificationModel = new NewNotificationModel();
                                newNotificationModel.setBody(jsonObject1.getString("body"));
                                newNotificationModel.setImage(jsonObject1.getString("image"));
                                newNotificationModel.setOwnerId(jsonObject1.getString("owner_id"));
                                newNotificationModel.setUserId(jsonObject1.getString("user_id"));
                                newNotificationModel.setTitle(jsonObject1.getString("title"));
                                newNotificationModel.setType(jsonObject1.getString("type"));

                                String notiType = jsonObject1.getString("type");
                                newNotificationModel.setUserName(jsonObject1.getString("user_name"));
                                newNotificationModel.setReviewBy(jsonObject1.getString("review_by"));
                                newNotificationModel.setReview(jsonObject1.getString("review"));
                                newNotificationModel.setId(jsonObject1.getString("id"));
                                if(notiType.equals("new_review") ||notiType.equals("review_request") || notiType.equals("new_business")  || notiType.equals("new_coupon") ) {
                                    newNotificationModel.setBusinessName(jsonObject1.getJSONObject("owner_data").getString("businessname"));

                                }
                                notificationArrayList.add(newNotificationModel);

                            }

                            Log.e("ArrayList", "" + notificationArrayList.size());
                            Log.e(TAG, "onResponse() called with: call = [" + call + "], localEventsArrayList = [" + notificationArrayList.size() + "]");
                            adapter.setNotificationModelList(notificationArrayList);
                            adapter.notifyDataSetChanged();
                        } else {
                            binding.textviewNoNoti.setVisibility(View.VISIBLE);
                        }
                    } else {

                        Toast.makeText(mContext , getString(R.string.please_try_again) , Toast.LENGTH_LONG).show();

                      //  binding.textviewNoNoti.setVisibility(View.VISIBLE);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    public class FloatTypeAdapter extends TypeAdapter<Float> {
        @Override
        public Float read(JsonReader reader) throws IOException {
            if(reader.peek() == JsonToken.NULL){
                reader.nextNull();
                return null;
            }
            String stringValue = reader.nextString();
            try{
                Float value = Float.valueOf(stringValue);
                return value;
            }catch(NumberFormatException e){
                return null;
            }
        }
        @Override
        public void write(JsonWriter writer, Float value) throws IOException {
            if (value == null) {
                writer.nullValue();
                return;
            }
            writer.value(value);
        }
    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    private void bannerClick(String bannerId, final String ownerId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", String.valueOf(bannerId));
        hashMap.put("owner_id", String.valueOf(ownerId));
        Constant.retrofitService.bannerClick(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        JSONObject jsonObject = new JSONObject(responseString);

                        if (jsonObject.getBoolean("status")) {
                            Intent intent = new Intent(mContext, ProviderDetails.class);
                            intent.putExtra("From", "Ad");
                            intent.putExtra("OWNERID", ownerId);
                            startActivity(intent);
                        } else {
                            // show message error
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utility.showAlert(mContext, "something went wrong");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.showAlert(mContext, "something went wrong");
            }
        });
    }

    private void getAllCoupons() {

        Constant.retrofitService.getAllbanners().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {
                       //     Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<BannerModel>>() {
                            }.getType();

                            if (!bannerArrayList.isEmpty()) {
                                bannerArrayList.clear();
                            }
                            bannerArrayList = gson.fromJson(jsonObject.getString("data"), type);
                            //log.e("ArrayList", "" + bannerArrayList.size());

                            try {
                                bannerStart();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    } else {


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void bannerStart() {
        try {
            // Create runnable for posting
            mUpdateResults = new Runnable() {
                public void run() {

                    if (bannerArrayList.size() == currentimageindex) {
                        currentimageindex = 0;
                        AnimateandSlideShow();
                    } else {
                        AnimateandSlideShow();
                    }


                }
            };

            int delay = 1000; // delay for 1 sec.
            int period = 2000; // repeat every 4 sec.

            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {
                    mHandler.post(mUpdateResults);
                }

            }, delay, period);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void AnimateandSlideShow() {



        try {
            String url = "https://www.localneighborhoodapp.com/images/banner/";

            String urlmain = url + bannerArrayList.get(currentimageindex).getImage();
            //log.e(TAG, "AnimateandSlideShow: " + urlmain);
            Picasso.with(mContext).load(urlmain).into(binding.slidingimage);
            //log.e(TAG, "AnimateandSlideShow: " + currentimageindex);

        // saveBannerview(bannerArrayList.get(currentimageindex).getId(), bannerArrayList.get(currentimageindex).getOwnerId());

            currentimageindex++;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onStop() {
        super.onStop();
        //log.e(TAG, "onStop: ");


        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //log.e(TAG, "onRestart: ");
        try {
            mHandler = new Handler();
            timer = new Timer();
            if (!bannerArrayList.isEmpty()){
                bannerStart();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void saveBannerview(String banner_id, String owner_id) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", banner_id);
        hashMap.put("owner_id", owner_id);

        //log.e(TAG, "saveBannerview: " + hashMap);

        Constant.retrofitService.saveBannerview(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
