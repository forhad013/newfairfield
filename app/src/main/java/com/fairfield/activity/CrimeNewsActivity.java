package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.fairfield.R;
import com.fairfield.databinding.ActivityCrimeNewsBinding;
import com.fairfield.model.BannerModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrimeNewsActivity extends AppCompatActivity {
    ActivityCrimeNewsBinding binding;
    private int scrollMax;
    private int scrollPos = 0;
    private TimerTask clickSchedule;
    private TimerTask scrollerSchedule;
    private TimerTask faceAnimationSchedule;
    private Button clickedButton = null;
    private Timer scrollTimer = null;
    private Timer clickTimer = null;
    private Timer faceTimer = null;
    private Boolean isFaceDown = true;
    private static final String TAG = "CrimeNewsActivity";
    private String[] imageNameArray = {"btn_fb", "banner", "btn_fb", "banner", "btn_fb", "banner", "btn_fb"};
    ImageView[] imgv;
    ArrayList<BannerModel> bannerArrayList;

    boolean leftToRight = true;
    int count = 0;
    private Gson gson;
    private Context mContext;
    private PreferenceHelper preferenceHelper;
    private Handler mHandler;
    private Runnable mUpdateResults;
    public int currentimageindex = 0;
    private Timer timer;
    private int[] IMAGE_IDS = {R.drawable.banner, R.drawable.btn_fb, R.drawable.like,
            R.drawable.new_logo};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_crime_news);
        setSupportActionBar(binding.myToolbar);
        bannerArrayList = new ArrayList<>();
        gson = new Gson();
        mContext = this;

        bannerArrayList = new ArrayList<>();
        preferenceHelper = new PreferenceHelper(mContext);
        timer = new Timer();
        mHandler = new Handler();


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });


        binding.llCypressCrime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrimeNewsActivity.this, NewsWebview.class);
                intent.putExtra("url", "http://www.topix.com/crime/cypress-tx");
                intent.putExtra("texttoolbar", getResources().getString(R.string.crimenews));
                startActivity(intent);
            }
        });

        binding.llCyFair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrimeNewsActivity.this, NewsWebview.class);
                //  intent.putExtra("url", "http://www.broadcastify.com/listen/ctid/2623");
                intent.putExtra("url", "http://www.broadcastify.com/listen/feed/13165");
                intent.putExtra("texttoolbar", getResources().getString(R.string.crimenews));

                startActivity(intent);
            }
        });


        binding.llNorthHarris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrimeNewsActivity.this, NewsWebview.class);
                intent.putExtra("url", "http://www.broadcastify.com/listen/feed/1364");
                intent.putExtra("texttoolbar", getResources().getString(R.string.crimenews));
                startActivity(intent);

            }
        });

        getAllCoupons();


        binding.slidingimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BannerModel banner = bannerArrayList.get(currentimageindex - 1);
                bannerClick(banner.getId(), banner.getOwnerId());
            }
        });
    }


    //---------------------------------------------------------------add code-----------------------------------------------------------------

    private void bannerClick(String bannerId, final String ownerId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", String.valueOf(bannerId));
        hashMap.put("owner_id", String.valueOf(ownerId));
        Constant.retrofitService.bannerClick(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        JSONObject jsonObject = new JSONObject(responseString);

                        if (jsonObject.getBoolean("status")) {
                            Intent intent = new Intent(mContext, ProviderDetails.class);
                            intent.putExtra("From", "Ad");
                            intent.putExtra("OWNERID", ownerId);
                            startActivity(intent);
                        } else {
                            // show message error
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utility.showAlert(mContext, "something went wrong");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.showAlert(mContext, "something went wrong");
            }
        });
    }

    private void getAllCoupons() {

        Constant.retrofitService.getAllbanners().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {

                            Type type = new TypeToken<List<BannerModel>>() {
                            }.getType();

                            if (!bannerArrayList.isEmpty()) {
                                bannerArrayList.clear();
                            }
                            bannerArrayList = gson.fromJson(jsonObject.getString("data"), type);


                            try {
                                bannerStart();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    } else {


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void bannerStart() {
        try {
            // Create runnable for posting
            mUpdateResults = new Runnable() {
                public void run() {

                    if (bannerArrayList.size() == currentimageindex) {
                        currentimageindex = 0;
                        AnimateandSlideShow();
                    } else {
                        AnimateandSlideShow();
                    }


                }
            };

            int delay = 1000; // delay for 1 sec.
            int period = 2000; // repeat every 4 sec.

            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {
                    mHandler.post(mUpdateResults);
                }

            }, delay, period);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void AnimateandSlideShow() {


        try {
            String url = "https://www.localneighborhoodapp.com/images/banner/";

            String urlmain = url + bannerArrayList.get(currentimageindex).getImage();
            Picasso.with(mContext).load(urlmain).into(binding.slidingimage);
            //   saveBannerview(bannerArrayList.get(currentimageindex).getId(), bannerArrayList.get(currentimageindex).getOwnerId());

            currentimageindex++;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onStop() {
        super.onStop();
        //  Log.e(TAG, "onStop: ");


        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        try {
            mHandler = new Handler();
            timer = new Timer();
            if (!bannerArrayList.isEmpty()) {
                bannerStart();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveBannerview(String banner_id, String owner_id) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", banner_id);
        hashMap.put("owner_id", owner_id);


        Constant.retrofitService.saveBannerview(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


}
