package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityVarificationBinding;
import com.fairfield.model.UserModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VarificationActivity extends AppCompatActivity {

    ActivityVarificationBinding binding;
    private PreferenceHelper helper;
    Context mContext;
    Gson gson;
    UserModel userModel;
    private static final String TAG = "VarificationActivity";
    private App app;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_varification);
        mContext = this;
        gson = new Gson();
        helper = new PreferenceHelper(mContext);
        userModel = new UserModel();
        app = App.getInstance();
        setSupportActionBar(binding.myToolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(" ");

        }
        binding.myToolbar.getBackground().setAlpha(0);
        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_white));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        binding.btnResendcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put(Constant.EMAIL, helper.getEmail());

                Utility.showProgressHUD(mContext);
                Constant.retrofitService.resendcodeForverify(hashMap).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Utility.hideProgressHud();

                        if (response.isSuccessful()) {
                            try {
                                String res = response.body().string();
                                Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + res + "]");

                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getBoolean("status")) {
                                    Utility.showLongToast(jsonObject.getString("message"), mContext);

                                } else {
                                    Utility.showAlert(mContext, jsonObject.getString("message"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                            Utility.showAlert(mContext, "Please try after some time.");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Utility.hideProgressHud();
                    }
                });

            }
        });


        binding.btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateCode()) {
                    return;
                }

                String code = binding.edittextCode.getText().toString().trim();
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put(Constant.EMAIL, helper.getEmail());
                hashMap.put("code", code);

                Utility.showProgressHUD(mContext);
                Constant.retrofitService.verifyEmailid(hashMap).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Utility.hideProgressHud();
                        if (response.isSuccessful()) {
                            try {
                                String res = response.body().string();
                                Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + res + "]");

                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getBoolean("status")) {

                                    userModel = gson.fromJson(jsonObject.getString("data"), UserModel.class);
                                    helper.putUserId(userModel.getUserId());
                                    helper.putEmail(userModel.getEmail());
                                    helper.putStatus(userModel.getStatus());
                                    app.userModel = userModel;
                                    Log.e(TAG, " userModel = [" + userModel + "]");


                                    Utility.showLongToast(jsonObject.getString("message"), mContext);

                                    Intent intent = new Intent(mContext, DashBoardActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    Utility.showAlert(mContext, jsonObject.getString("message"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                            Utility.showAlert(mContext, "Please try after some time.");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Utility.hideProgressHud();
                        Log.e(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");
                    }
                });




            }
        });


    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateCode() {
        if (binding.edittextCode.getText().toString().trim().isEmpty()) {
            binding.inputLayoutCode.setError(getString(R.string.err_msg_name));
            requestFocus(binding.edittextCode);
            return false;
        } else {
            binding.inputLayoutCode.setErrorEnabled(false);
        }

        return true;
    }
}
