package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityForgotPasswordBinding;
import com.fairfield.model.UserModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

   private ActivityForgotPasswordBinding binding;
    private  Context mContext;
    private  Gson gson;
    private PreferenceHelper helper;
    private   HashMap<String ,String> hashMap;
    private static final String TAG = "ForgotPasswordActivity";
    private UserModel userModel;
    private App app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        setSupportActionBar(binding.myToolbar);

        mContext =this;
        gson = new Gson();
        app = App.getInstance();
        userModel = new UserModel();
        helper = new PreferenceHelper(mContext);



        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(" ");
        }
        binding.myToolbar.getBackground().setAlpha(0);
        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_white));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        binding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendForm();

            }
        });


    }

    private void sendForm() {

        if (!validateEmail()) {
            return;
        }
        String emailid = binding.inputEmail.getText().toString().trim();
        hashMap = new HashMap<>();
       // hashMap.put(Constant.USER_ID, helper.getUserId());
        hashMap.put(Constant.EMAIL, emailid);

        forgotPassoword();

        // Toast.makeText(getActivity(), "Thank You!", Toast.LENGTH_SHORT).show();
    }
    private boolean validateEmail() {
        String email = binding.inputEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            binding.inputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(binding.inputEmail);
            return false;
        } else {
            binding.inputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
           getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private  void forgotPassoword(){

        Utility.showProgressHUD(mContext);
        Constant.retrofitService.forgotpassword(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utility.hideProgressHud();
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + res + "]");

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("status")) {

                          /*  userModel = gson.fromJson(jsonObject.getString("data"), UserModel.class);
                            helper.putUserId(userModel.getUserId());
                            helper.putEmail(userModel.getEmail());
                            helper.putStatus(userModel.getStatus());
                            app.userModel = userModel;
                            Log.e(TAG, " userModel = [" + userModel + "]");*/

                            Utility.showLongToast(jsonObject.getString("message"), mContext);

                            Intent intent = new Intent(mContext, SignupActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        } else {
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    Utility.showAlert(mContext, "Please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.hideProgressHud();
                Log.e(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");
            }
        });



    }
}
