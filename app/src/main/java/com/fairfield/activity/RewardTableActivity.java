package com.fairfield.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.databinding.DataBindingUtil;
import android.widget.Toast;

import com.fairfield.R;
import com.fairfield.adapter.RewardAdapter;
import com.fairfield.adapter.RewardTableAdapter;
import com.fairfield.adapter.RewardTableCellAdapter;
import com.fairfield.databinding.ActivityRewardBinding;
import com.fairfield.model.RewardModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.DownloadImageTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RewardTableActivity extends AppCompatActivity {

    private ActivityRewardBinding binding;
    private Gson gson;
    private static final String TAG = "RewardActivity";
    private RewardTableCellAdapter adapter;

    public int currentimageindex = 0;
    public Context mContext;
    ArrayList<RewardModel> rewardModels;
    public int reward_id;
    public HashMap<String, String> hashMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this , R.layout.activity_reward);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }


        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });

        mContext = this;

        reward_id = getIntent().getIntExtra("Reward_ID", 0);
        view_reward();

    }

    private  void view_reward(){

        hashMap = new HashMap<String, String>();
        hashMap.put("reward_id", String.valueOf(reward_id));
        Constant.retrofitService.view_reward(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) try {
                    String responseString = response.body().string();

                    JSONObject jsonObject = new JSONObject(responseString);
                    JSONArray commentsArray = jsonObject.getJSONArray("comments");

//                    Log.e("CommentsArray", commentsArray.toString());
//
//                    ArrayList<JSONObject> sortedObject = new ArrayList<JSONObject>();
//
//
//
//                    JSONArray sortedJsonArray = new JSONArray(jsonValues);
//                    Log.e("SortedArray", sortedJsonArray.toString());

                    rewardModels = new ArrayList<RewardModel>();
                    String title, comment, dateStr, image;
                    Date date;
                    RewardModel rewardModel;
                    for (int i = 0; i < commentsArray.length(); i++) {
                        JSONObject json = commentsArray.getJSONObject(i);
                        if (json.isNull("first_name"))
                            title = "";
                        else
                            title = json.getString("first_name");
                        comment = json.getString("comment");
                        if (json.isNull("image"))
                            image = "";
                        else
                            image = json.getString("image");
                        dateStr = json.getString("created_at").substring(0,10);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        date = dateFormat.parse(dateStr);
                        dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                        dateStr = dateFormat.format(date);
                        rewardModel = new RewardModel(0,title, false,image, dateStr, comment);
                        rewardModels.add(rewardModel);
                    }

                    adapter = new RewardTableCellAdapter(mContext, (List<RewardModel>)rewardModels);
                    binding.rewardTable.setAdapter(adapter);
                    binding.rewardTable.setLayoutManager(new LinearLayoutManager(mContext));
                    binding.rewardTable.setItemAnimator(new DefaultItemAnimator());
                    binding.rewardTable.addItemDecoration(new DividerItemDecoration(mContext, 0));
                    stopAnim();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Failed", "Failed");
                Toast.makeText(RewardTableActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.INVISIBLE);
        // or avi.smoothToHide();
    }

    class JSONComparator implements Comparator<JSONObject>
    {

        public int compare(JSONObject a, JSONObject b)
        {
            //valA and valB could be any simple type, such as number, string, whatever
            String valA = null;
            String valB = null;
            try {
                valA = a.getString("created_at");
                valB = b.getString("created_at");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return valA.compareTo(valB);
        }
    }
}
