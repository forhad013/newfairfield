package com.fairfield.activity;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fairfield.R;
import com.fairfield.adapter.EventCommentAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityEventDetailsBinding;
import com.fairfield.model.BannerModel;
import com.fairfield.model.CommentModel;
import com.fairfield.model.EventCommentModel;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.MarshMallowPermission;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.fairfield.widgets.CropImage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetailsActivity extends AppCompatActivity {

    private ActivityEventDetailsBinding binding;
    private Context mContext;
    App app;
    private static final String TAG = "EventDetailsActivity";
    private PreferenceHelper preferenceHelper;
    private EventCommentAdapter adapter;
    CommentModel commentModel;
    List<File> filesimage;
    File imageupload;

    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int SELECT_MESSAGES_CAMERA = 0x007;
    private String FileImagePath = null;
    private Uri fileUri; // file url to store image/video
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x3;
    static final int REQUEST_TAKE_PHOTO = 1;

    private Gson gson;
    public int position = 0;
    private File mFileTemp = new File(Environment.getExternalStorageDirectory() + "/photo.jpg");
    private MarshMallowPermission marshMallowPermission;
    private String realPathFromUri;
    private InputStream inputStream;
    private String FileSize = "0";

    ArrayList<EventCommentModel> commentModelArrayList;


    Bitmap bitm;


    public static String[] choose_from = {"Gallery", "Camera"};


    private Handler mHandler;
    private Runnable mUpdateResults;
    public int currentimageindex = 0;
    private Timer timer;
    private ArrayList<BannerModel> bannerArrayList;
    private int[] IMAGE_IDS = {R.drawable.banner, R.drawable.btn_fb, R.drawable.like,
            R.drawable.new_logo};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        app = App.getInstance();
        preferenceHelper = new PreferenceHelper(mContext);
        bannerArrayList = new ArrayList<>();
        timer = new Timer();
        mHandler = new Handler();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_event_details);
        setSupportActionBar(binding.myToolbar);
        binding.setEventdetails(app.localeventsModel);
        commentModel = new CommentModel();
        filesimage = new ArrayList<>();
        commentModelArrayList = new ArrayList<>();

        if (!commentModelArrayList.isEmpty()) {
            commentModelArrayList.clear();
        }

        marshMallowPermission = new MarshMallowPermission(mContext);
        mContext = this;

        gson = new Gson();


        binding.recyclerViewCommnet.setLayoutManager(new GridLayoutManager(mContext, 1));
        binding.recyclerViewCommnet.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerViewCommnet.setNestedScrollingEnabled(false);
        binding.recyclerViewCommnet.setHasFixedSize(false);

        adapter = new EventCommentAdapter(mContext, commentModelArrayList);
        binding.recyclerViewCommnet.setAdapter(adapter);

        showEventComment();

        getAllCoupons();
        if (!filesimage.isEmpty()) {
            filesimage.clear();
            commentModel = new CommentModel();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        try {
            //  String contact = app.localeventsModel.getContact();
            //  String changecont = contact.replace(',', '\n');
            //  binding.textviewContact.setText(changecont);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            if (app.localeventsModel.getInterested().equals("2")) {

                binding.imageviewInter.setImageResource(R.drawable.intersted);
                binding.imageviewNotinter.setImageResource(R.drawable.not_intersted);

            } else if (app.localeventsModel.getInterested().equals("1")) {

                binding.imageviewInter.setImageResource(R.drawable.select_intersted);
                binding.imageviewNotinter.setImageResource(R.drawable.not_intersted);
            } else {
                binding.imageviewInter.setImageResource(R.drawable.intersted);
                binding.imageviewNotinter.setImageResource(R.drawable.selcet_not_intersted);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.slidingimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BannerModel banner = bannerArrayList.get(currentimageindex - 1);
                bannerClick(banner.getId(), banner.getOwnerId());



              /*  AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);
                dialog.setView(dialogLayout);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.show();


                ImageView image = (ImageView) dialog.findViewById(R.id.goProDialogImage);
                //  Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),
                //      R.drawable.whygoprodialogimage);

                try {
                    String url = "https://www.localneighborhoodapp.com/images/banner/";
                    String urlmain = url + bannerArrayList.get(currentimageindex - 1).getImage();
                    Log.e(TAG, "AnimateandSlideShow: " + urlmain);
                    Picasso.with(mContext).load(urlmain).into(image);

                } catch (Exception e) {
                    System.out.println(e);
                }
*/

            }
        });

        binding.imageviewInter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ConnectivityReceiver.isConnected()) {


                    try {
                        if (app.localeventsModel.getInterested().equals("2")) {
                            app.localeventsModel.setInterested("1");
                            binding.imageviewInter.setImageResource(R.drawable.select_intersted);
                            binding.imageviewNotinter.setImageResource(R.drawable.not_intersted);
                            saveinterstedbyUser(app.localeventsModel.getId(), "1");
                        } else if ((app.localeventsModel.getInterested().equals("1"))) {
                            app.localeventsModel.setInterested("2");
                            binding.imageviewInter.setImageResource(R.drawable.intersted);
                            binding.imageviewNotinter.setImageResource(R.drawable.not_intersted);
                            saveinterstedbyUser(app.localeventsModel.getId(), "2");
                        } else {
                            app.localeventsModel.setInterested("1");
                            binding.imageviewInter.setImageResource(R.drawable.select_intersted);
                            binding.imageviewNotinter.setImageResource(R.drawable.not_intersted);
                            saveinterstedbyUser(app.localeventsModel.getId(), "1");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Utility.showSnackBar(mContext, binding.imageviewInter, getResources().getString(R.string.no_internet));

                }


            }
        });


        binding.imageviewNotinter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {

                    if (app.localeventsModel.getInterested().equals("2")) {
                        app.localeventsModel.setInterested("0");
                        binding.imageviewInter.setImageResource(R.drawable.intersted);
                        binding.imageviewNotinter.setImageResource(R.drawable.selcet_not_intersted);

                        saveinterstedbyUser(app.localeventsModel.getId(), "0");
                    } else if ((app.localeventsModel.getInterested().equals("1"))) {
                        app.localeventsModel.setInterested("0");

                        binding.imageviewInter.setImageResource(R.drawable.intersted);
                        binding.imageviewNotinter.setImageResource(R.drawable.selcet_not_intersted);

                        saveinterstedbyUser(app.localeventsModel.getId(), "0");
                    } else {
                        app.localeventsModel.setInterested("2");
                        binding.imageviewInter.setImageResource(R.drawable.intersted);
                        binding.imageviewNotinter.setImageResource(R.drawable.not_intersted);
                        saveinterstedbyUser(app.localeventsModel.getId(), "2");
                    }
                } else {

                    Utility.showSnackBar(mContext, binding.imageviewNotinter, getResources().getString(R.string.no_internet));
                }
            }
        });


        //--------------------------------------------------------comment show hide--------------------------------------------------------------------------------------
        binding.btnShowmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    Intent intent = new Intent(mContext, ShowAllEventComment.class);
                    startActivity(intent);

                } else {
                    Utility.showSnackBar(mContext, binding.imageviewNotinter, getResources().getString(R.string.no_internet));
                }


            }
        });

        //-----------------------------------------------------Comment and image upload-------------------------------------------------------------------------
        binding.fabPickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (marshMallowPermission.isPermissionForReadExtertalStorage()) {


                    if (filesimage.size() <= 4) {

                        openGallery();
                    } else {
                        Utility.showSnackBar(mContext, binding.editComment, "Please select minimum five image");
                        return;
                    }

                } else {
                    try {
                        marshMallowPermission.requestPermissionForReadExtertalStorage();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        binding.fabPickCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (marshMallowPermission.isPermissionForCamera_And_ReadExternal()) {


                    if (filesimage.size() <= 4) {

                        takePicture();
                    } else {
                        Utility.showSnackBar(mContext, binding.editComment, "Please select minimum five image");
                        return;
                    }


                } else {
                    try {
                        marshMallowPermission.requestPermissionForCamera_And_ReadExternal();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        binding.btnPostcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    upload();
                } else {
                    Utility.showSnackBar(mContext, binding.btnPostcomment, getResources().getString(R.string.no_internet));
                }

            }
        });


    }

    private void takePicture() {

      /*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");

        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, SELECT_MESSAGES_CAMERA);*/
        //pic = f;

        //  Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //  cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        //  startActivityForResult(cameraIntent, SELECT_MESSAGES_CAMERA);


        String fileName = System.currentTimeMillis() + ".jpg";
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, SELECT_MESSAGES_CAMERA);
    }


    /*private void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            } else {
                    *//*
                     * The solution is taken from here:
					 * http://stackoverflow.com/questions
					 * /10042695/how-to-get-camera-result-as-a-uri-in-data-folder
					 *//*
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {

            Log.d(TAG, "cannot take picture", e);
        }
    }*/


    /**
     * Checking device has camera hardware or not
     */
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }



    /*
 * Capturing Camera Image will lauch camera app requrest image capture
 */


    /**
     * Creating file uri to store image/video
     */

    /*
     * returning image / video
     */


    /**
     * Display image from a path to ImageView
     */
    private void previewCapturedImage() {
        try {


            // filesimage.add(mediaStorageDir);
            //  commentModel.setImages(filesimage);
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);

            // imgPreview.setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void startCropImage() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }


    private File createTempFile() {
        return new File(getExternalFilesDir(Environment.DIRECTORY_MOVIES), System.currentTimeMillis() + "_image.jpeg");
    }

    private void upload() {

        if (!validateCommentd()) {
            return;
        }
        Utility.showProgressHUD(mContext);
        String comment = binding.editComment.getText().toString();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        if (!filesimage.isEmpty()) {


            for (int i = 0; i < filesimage.size(); i++) {
                builder.addFormDataPart("images[]", commentModel.getImages().get(i).getName(), RequestBody.create(MediaType.parse("image*//*"), commentModel.getImages().get(i)));
            }

          /*  if (filesimage.size() <= 5) {


            } else {
                Utility.showSnackBar(mContext, binding.editComment, "Please select minimum five image");
                return;
            }*/
        }


        if (!filesimage.isEmpty()) {
            filesimage.clear();
            commentModel = new CommentModel();
        }
        builder.addFormDataPart(Constant.USER_ID, preferenceHelper.getUserId());
        builder.addFormDataPart("event_id", app.localeventsModel.getId());
        builder.addFormDataPart("comment", comment);
        final MultipartBody requestBody = builder.build();

        Constant.retrofitService.postMeme(requestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    try {
                        if (response.isSuccessful()) {
                            String res = response.body().string();



                            JSONObject jsonObject = new JSONObject(res);
                            if (jsonObject.getBoolean("status")) {

                                Utility.showSnackBar(mContext, binding.btnPostcomment ,getResources().getString(R.string.successfully_submit_review));

                                binding.llMain.removeAllViews();
                                binding.editComment.setText("");
                            }
                            Utility.hideProgressHud();
                            showEventComment();
                        } else {
                            binding.llMain.removeAllViews();
                            binding.editComment.setText("");

                            Utility.showAlert(mContext, "Please try after some time.");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private boolean validateCommentd() {
        if (binding.editComment.getText().toString().trim().isEmpty()) {

            Utility.showSnackBar(mContext, binding.editComment, getResources().getString(R.string.err_msg_comment));

            return false;
        }
        return true;
    }


    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {




        if (resultCode != RESULT_OK) {

            return;
        }

        Bitmap bitmap;

        switch (requestCode) {

            case REQUEST_CODE_GALLERY:

                try {
                    try {
                        Uri selectedImageUri = data.getData();
                        String s1 = data.getDataString();
                        //String s1 = selectedImageUri.getPath();





                        String selectedImagePath = getPath(selectedImageUri);
                        if (selectedImagePath == null && s1 != null) {
                            selectedImagePath = s1.replaceAll("file://", "");
                        }


                        long totalSpace = 0;

                        totalSpace = new File(selectedImagePath).length() / 1000;



                        if (totalSpace > 150) {
                            filesimage.add(new File(compressImage(selectedImagePath).trim()));
                            commentModel.setImages(filesimage);
                        } else {
                            filesimage.add(new File(selectedImagePath.trim()));
                            commentModel.setImages(filesimage);
                        }


                        Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(selectedImagePath.trim()));
                        int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);


                    /*    BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 8;

                     //   bitmap = BitmapFactory.decodeFile(selectedImagePath,options);
                        bitmap = BitmapFactory.decodeFile(selectedImagePath);
                        //   binding.imageUser.setImageBitmap(bitmap);

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();

                        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
*/

                        //    Log.e("encoded image ", encoded);

                        ImageView iv = new ImageView(mContext);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
                        iv.setLayoutParams(layoutParams);
                        iv.setImageBitmap(scaled);
                        iv.setPadding(5, 5, 5, 5);
                        iv.setScaleType(ImageView.ScaleType.FIT_XY);
                        binding.llMain.addView(iv);

                        binding.scrollviewEvent.post(new Runnable() {
                            @Override
                            public void run() {
                                binding.scrollviewEvent.fullScroll(View.FOCUS_DOWN);
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } catch (Exception e) {


                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:

                startCropImage();
                break;

            case SELECT_MESSAGES_CAMERA:
                File file = null;
                try {

                    if (getPath(fileUri) != null) {
                        FileImagePath = getPath(fileUri);
                        System.out.println("Image Path >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>: " + FileImagePath);




                        //   FileImagePath = getPath(getApplicationContext(), data.getData());


                        if (FileImagePath != null) {
                            file = new File(FileImagePath);
                        }
                        if (file != null) {
                            FileSize = String.valueOf(file.length());

                        }
                        long totalSpace = 0;

                        totalSpace = new File(FileImagePath).length() / 1000;

                        if (totalSpace > 150) {
                            filesimage.add(new File(compressImage(FileImagePath)));
                            commentModel.setImages(filesimage);
                        } else {
                            filesimage.add(file);
                            commentModel.setImages(filesimage);
                        }

                        Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(FileImagePath));
                        int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);


                        ImageView iv = new ImageView(mContext);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
                        iv.setLayoutParams(layoutParams);
                        iv.setImageBitmap(scaled);
                        iv.setPadding(5, 5, 5, 5);
                        iv.setScaleType(ImageView.ScaleType.FIT_XY);
                        binding.llMain.addView(iv);


                    } else {



                        Bitmap bitmap1 = (Bitmap) data.getExtras().get("data");
                        String cachePath = ""; // you still need a default value if not mounted
                        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !Environment.isExternalStorageRemovable()) {
                            if (getExternalCacheDir() != null) {
                                cachePath = getExternalCacheDir().getPath(); // most likely your null value
                            }
                        } else {
                            if (mContext.getCacheDir() != null) {
                                cachePath = mContext.getCacheDir().getPath();
                            }
                        }

                        String path = cachePath + "" + Calendar.getInstance().getTimeInMillis() + "";

                        File imageFile = savebitmap(bitmap1, path);
                        if (imageFile.exists()) {
                            FileImagePath = imageFile.getPath();
                            file = new File(FileImagePath);
                            FileSize = String.valueOf(file.length());
                            long totalSpace = 0;

                            totalSpace = new File(FileImagePath).length() / 1000;



                            if (totalSpace > 150) {
                                filesimage.add(new File(compressImage(FileImagePath)));
                                commentModel.setImages(filesimage);
                            } else {
                                filesimage.add(file);
                                commentModel.setImages(filesimage);
                            }


                            Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(FileImagePath));
                            int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                            Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);


                            ImageView iv = new ImageView(mContext);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
                            iv.setLayoutParams(layoutParams);
                            iv.setImageBitmap(scaled);
                            iv.setPadding(5, 5, 5, 5);
                            iv.setScaleType(ImageView.ScaleType.FIT_XY);
                            binding.llMain.addView(iv);
                        }
                    }
                } catch (Exception e) {


                    return;
                }


                break;
            case REQUEST_CODE_CROP_IMAGE:

                try {
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {

                        return;
                    }

                    long totalSpace = 0;
                    totalSpace = new File(path).length() / 1000;



                    if (totalSpace > 150) {
                        filesimage.add(new File(compressImage(path)));
                        commentModel.setImages(filesimage);
                    } else {
                        filesimage.add(new File(path));
                        commentModel.setImages(filesimage);
                    }

                    Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(path));
                    int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);

                   /* bitmap = BitmapFactory.decodeFile(path);
                    //   binding.imageUser.setImageBitmap(bitmap);


                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);


                    Log.e("encoded image ", encoded);*/


                    ImageView iv = new ImageView(mContext);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
                    iv.setLayoutParams(layoutParams);
                    iv.setImageBitmap(bitmapImage);
                    iv.setPadding(5, 5, 5, 5);
                    iv.setScaleType(ImageView.ScaleType.FIT_XY);
                    binding.llMain.addView(iv);

                    binding.scrollviewEvent.post(new Runnable() {
                        @Override
                        public void run() {
                            binding.scrollviewEvent.fullScroll(View.FOCUS_DOWN);
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

                //  controller.dishDetailed.setImage(new File(path));

                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Return the path of the file.
    public String getPath(Uri uri) {

        try {
            String[] projection = {MediaStore.Images.Media.DATA};

            Cursor cursor = managedQuery(uri, projection, null, null, null);

            if (cursor == null) {
                return null;
            }
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            cursor.moveToFirst();

            return cursor.getString(column_index);
        } catch (Exception e) {
            Toast.makeText(mContext, "Image is too big in resolution please try again", Toast.LENGTH_LONG).show();
            return null;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == marshMallowPermission.CAMERA_READ_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                if (filesimage.size() <= 4) {

                    takePicture();
                } else {
                    Utility.showSnackBar(mContext, binding.editComment, "Please select minimum five image");
                    return;
                }

            } else {
                Toast.makeText(mContext, "Please accept permission", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == marshMallowPermission.READ_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                showDialog(new String[]{"Gallery"});


                if (filesimage.size() <= 4) {

                    openGallery();
                } else {
                    Utility.showSnackBar(mContext, binding.editComment, "Please select minimum five image");
                    return;
                }


            } else {
                Toast.makeText(mContext, "Please accept permission", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void saveinterstedbyUser(String eventid, String value) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("event_id", eventid);
        hashMap.put("interested", value);



        Constant.retrofitService.saveEventbyUser(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {



            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {



            }
        });
    }


    private void showEventComment() {

        final HashMap<String, String> meMap = new HashMap<String, String>();
        meMap.put("event_id", app.localeventsModel.getId());
        Constant.retrofitService.showEventComments(meMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();

                        final JSONObject jsonObject = new JSONObject(responsestring);
                        if (jsonObject.getBoolean("status")) {


                            if (!commentModelArrayList.isEmpty()) {
                                commentModelArrayList.clear();
                            }

                            final Type type = new TypeToken<List<EventCommentModel>>() {
                            }.getType();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        commentModelArrayList = gson.fromJson(jsonObject.getString("data"), type);
                                        if (commentModelArrayList.size() > 1) {
                                            binding.llShowmore.setVisibility(View.VISIBLE);
                                        }



                                        adapter.setEventCommentModelList(commentModelArrayList.subList(0, 1));
                                        //  adapter.setEventCommentModelList(commentModelArrayList);
                                        adapter.notifyDataSetChanged();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        Utility.showAlert(mContext, "Please try after some time.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);


            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);


            } else if (orientation == 3) {
                matrix.postRotate(180);


            } else if (orientation == 8) {
                matrix.postRotate(270);


            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     */
    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {


        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static void copyFile(File src, File dst) throws IOException {
        FileInputStream var2 = new FileInputStream(src);
        FileOutputStream var3 = new FileOutputStream(dst);
        byte[] var4 = new byte[1024];

        int var5;
        while ((var5 = var2.read(var4)) > 0) {
            var3.write(var4, 0, var5);
        }
        var2.close();
        var3.close();
    }

    private File savebitmap(Bitmap bitmap, String filename) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;

        File file = new File(filename + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");


        }
        try {
            // make a new bitmap from your file
//            Bitmap bitmap = BitmapFactory.decodeFile(file.getName());

            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return file;

    }


    public static String compressImageShow(Context mContext, String imagePath) {
        final float maxHeight = 640.0f;
        final float maxWidth = 640.0f;

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        String path = MediaStore.Images.Media.insertImage(mContext.getContentResolver(), scaledBitmap, "Title", null);

//        return out.toString();
        return path;

    }

    private void bannerClick(String bannerId, final String ownerId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", String.valueOf(bannerId));
        hashMap.put("owner_id", String.valueOf(ownerId));
        Constant.retrofitService.bannerClick(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        JSONObject jsonObject = new JSONObject(responseString);

                        if (jsonObject.getBoolean("status")) {
                            Intent intent = new Intent(mContext, ProviderDetails.class);
                            intent.putExtra("From", "Ad");
                            intent.putExtra("OWNERID", ownerId);
                            startActivity(intent);
                        } else {
                            // show message error
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utility.showAlert(mContext, "something went wrong");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.showAlert(mContext, "something went wrong");
            }
        });
    }

    private void getAllCoupons() {
        Constant.retrofitService.getAllbanners().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {

                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<BannerModel>>() {
                            }.getType();

                            if (!bannerArrayList.isEmpty()) {
                                bannerArrayList.clear();
                            }
                            bannerArrayList = gson.fromJson(jsonObject.getString("data"), type);


                            try {
                                bannerStart();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    } else {


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void bannerStart() {
        try {
            // Create runnable for posting
            mUpdateResults = new Runnable() {
                public void run() {

                    if (bannerArrayList.size() == currentimageindex) {
                        currentimageindex = 0;
                        AnimateandSlideShow();
                    } else {
                        AnimateandSlideShow();
                    }

                }
            };

            int delay = 1000; // delay for 1 sec.
            int period = 2000; // repeat every 4 sec.

            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {
                    mHandler.post(mUpdateResults);
                }

            }, delay, period);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void AnimateandSlideShow() {


        try {
            String url = "https://www.localneighborhoodapp.com/images/banner/";

            String urlmain = url + bannerArrayList.get(currentimageindex).getImage();
            Picasso.with(mContext).load(urlmain).into(binding.slidingimage);

         //  saveBannerview(bannerArrayList.get(currentimageindex).getId(), bannerArrayList.get(currentimageindex).getOwnerId());

            currentimageindex++;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();



        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        try {
            mHandler = new Handler();
            timer = new Timer();
            if (!bannerArrayList.isEmpty()){
                bannerStart();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void saveBannerview(String banner_id, String owner_id) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", banner_id);
        hashMap.put("owner_id", owner_id);




        Constant.retrofitService.saveBannerview(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


}




