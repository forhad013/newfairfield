package com.fairfield.activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityFeedbackBinding;
import com.fairfield.model.CommentModel;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.MarshMallowPermission;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity {

    private static final String TAG = "FeedbackActivity";

    Context mContext;
    ActivityFeedbackBinding binding;
    private MarshMallowPermission marshMallowPermission;
    private String FileImagePath = null;
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int SELECT_MESSAGES_CAMERA = 0x007;
    private Uri fileUri;
    private PreferenceHelper preferenceHelper;
    private List<File> filesimage;
    private CommentModel commentModel;
    private String FileSize = "0";
    App app;

    int imageposton = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback);
        setSupportActionBar(binding.myToolbar);
        mContext = this;
        filesimage = new ArrayList<>();
        preferenceHelper = new PreferenceHelper(mContext);
        marshMallowPermission = new MarshMallowPermission(mContext);
        commentModel = new CommentModel();
        app = App.getInstance();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }
        // Log.e(TAG, "OwnerId>>>>>: " + app.ownerid);


        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, DashBoardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });


        binding.ratingbar.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {

                //  Log.e(TAG, "onRatingChanged: " + rating);
            }
        });


        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {


                    if (binding.btnSubmit.getTag().equals("submit")) {

                        upload();
                    } else {

                        editCommentupload();
                    }

                } else {
                    Utility.showSnackBar(mContext, binding.btnSubmit, getResources().getString(R.string.no_internet));
                }


            }
        });

        try {
            String from = getIntent().getExtras().getString("FROM");

            if (from.equals("edit")) {
                binding.setFeedback(app.feedbackModel);
                //setimage();

                binding.btnSubmit.setText("Save");
                binding.btnSubmit.setTag("save");
                setImageUpdate();
            } else {
                binding.btnSubmit.setText("Submit");
                binding.btnSubmit.setTag("submit");
                // Log.e(TAG, "by_edit: in else>>>>>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


//-----------------------------------------------------image pick from gallery and camera---------------------------------------------------------

        binding.fabPickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (marshMallowPermission.isPermissionForReadExtertalStorage()) {

                    //   Log.e(TAG, "size>>>>>>>>>>>>: " + app.feedbackModel.getImages().size());

                    if (app.feedbackModel.getImages().size() + filesimage.size() <= 4) {

                        openGallery();
                    } else {
                        Utility.showSnackBar(mContext, binding.edittextTitle, "Please select minimum five image");
                        return;
                    }

                } else {
                    try {
                        marshMallowPermission.requestPermissionForReadExtertalStorage();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        binding.fabPickCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (marshMallowPermission.isPermissionForCamera_And_ReadExternal()) {

                    if (app.feedbackModel.getImages().size() + filesimage.size() <= 4) {

                        takePicture();
                    } else {
                        Utility.showSnackBar(mContext, binding.edittextTitle, "Please select minimum five image");
                        return;
                    }

                } else {
                    try {
                        marshMallowPermission.requestPermissionForCamera_And_ReadExternal();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }


  /*  private  void setimage(){

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        Log.e(TAG, "onCreate: layoutManagerlayoutManagerlayoutManager   " + layoutManager + ":::::::::::::" + binding.recyclerViewCategory);
        binding.recyclerViewCategory.setLayoutManager(layoutManager);
        binding.recyclerViewCategory.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerViewCategory.setNestedScrollingEnabled(false);
        binding.recyclerViewCategory.setHasFixedSize(false);


        List<String> fileArrayList = new ArrayList<>();
        fileArrayList = app.feedbackModel.getImages();

        DiynamicImageAdapter diynamicImageAdapter = new DiynamicImageAdapter(mContext , fileArrayList);
        binding.recyclerViewCategory.setAdapter(diynamicImageAdapter);
    }*/

    private void setImageUpdate() {

        binding.llMain.removeAllViews();

        for (int i = 0; i < app.feedbackModel.getImages().size(); i++) {

            final FrameLayout frameLayout = new FrameLayout(mContext);
            frameLayout.setId(i);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
            frameLayout.setLayoutParams(layoutParams);
            layoutParams.setMargins(30, 0, 30, 0);
            ImageView iv = new ImageView(mContext);
            LinearLayout.LayoutParams imageview = new LinearLayout.LayoutParams(300, 300);
            iv.setLayoutParams(layoutParams);


            String baseurl_for_image = "https://www.localneighborhoodapp.com/assets/rating_review/";

            Picasso.with(mContext).load(baseurl_for_image + app.feedbackModel.getImages().get(i)).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(iv);

            iv.setScaleType(ImageView.ScaleType.FIT_XY);


            ImageView cross = new ImageView(mContext);
            cross.setId(i);
            cross.setTag(app.feedbackModel.getImages().get(i));
            //    Log.e(TAG, "id: " + i);

            LinearLayout.LayoutParams imageview_crossParams = new LinearLayout.LayoutParams(50, 50);
            cross.setLayoutParams(imageview_crossParams);
            cross.setImageResource(R.drawable.deleticon);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (ConnectivityReceiver.isConnected()) {
                        //  Log.e(TAG, "id>>>>>>>>>>>: " + view.getId());
                        //  Log.e(TAG, "image name : " + app.feedbackModel.getImages().get(view.getId()));


                        try {
                            for (int i = 0; i < app.feedbackModel.getImages().size(); i++) {


                                if (view.getTag().equals(app.feedbackModel.getImages().get(i))) {


                                    frameLayout.setVisibility(View.GONE);
                                    deleteImageOnServer(app.feedbackModel.getImages().get(i), i);

                                } else {

                                    //   Log.e(TAG, "onClick: >>>>>>>in else image");
                                }


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {
                        Utility.showSnackBar(mContext, binding.btnSubmit, getResources().getString(R.string.no_internet));
                    }


                }
            });


            frameLayout.addView(iv);
            frameLayout.addView(cross);
            binding.llMain.addView(frameLayout);

            binding.scrollviewFeedback.post(new Runnable() {
                @Override
                public void run() {
                    binding.scrollviewFeedback.fullScroll(View.FOCUS_DOWN);
                }
            });
        }

    }

    private void deleteImageOnServer(String imagename, final int i) {

        Utility.showProgressHUD(mContext);

        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put("owner_id", app.feedbackModel.getOwnerId());
        hashMap.put("user_id", app.feedbackModel.getUserId());
        hashMap.put("review_id", app.feedbackModel.getId());
        hashMap.put("image", imagename);


        Constant.retrofitService.deleteReviewImage(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("status")) {

                            app.feedbackModel.getImages().remove(i);
                            //      Log.e(TAG, "feedbackModel image size: " + app.feedbackModel.getImages().size());
                            //      Log.e(TAG, "feedbackModel images: " + app.feedbackModel.getImages().toString());


                            Utility.hideProgressHud();
                            Utility.showSnackBar(mContext, binding.btnSubmit, "picture successfully delete");

                        } else {
                            Utility.hideProgressHud();
                            Utility.showSnackBar(mContext, binding.btnSubmit, getResources().getString(R.string.please_try_again));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    Utility.hideProgressHud();

                    Utility.showSnackBar(mContext, binding.btnSubmit, getResources().getString(R.string.please_try_again));

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {


            }
        });
    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture() {


        String fileName = System.currentTimeMillis() + ".jpg";
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, SELECT_MESSAGES_CAMERA);

       /* Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        startActivityForResult(cameraIntent, SELECT_MESSAGES_CAMERA);*/
    }


    private boolean validateTitle() {
        if (binding.edittextTitle.getText().toString().trim().isEmpty()) {

            Utility.showSnackBar(mContext, binding.edittextTitle, getResources().getString(R.string.err_msg_title));

            //  Utility.showAlert(mContext, getString(R.string.err_msg_title));
            return false;
        }
        return true;
    }

    private boolean validateDesc() {
        if (binding.edittextDesc.getText().toString().trim().isEmpty()) {

            Utility.showSnackBar(mContext, binding.edittextDesc, getResources().getString(R.string.err_msg_des));
            //  Utility.showAlert(mContext, getString(R.string.err_msg_des));
            return false;
        }
        return true;
    }

    private boolean validaterating() {
        //   Log.e(TAG, "validaterating: " + binding.ratingbar.getRating());

        if (binding.ratingbar.getRating() == 0) {

            Utility.showSnackBar(mContext, binding.edittextDesc, getResources().getString(R.string.err_msg_rating));

            //     Utility.showAlert(mContext, getString(R.string.err_msg_rating));
            return false;
        }
        return true;
    }

    private void upload() {

        if (!validaterating()) {
            return;
        }

        if (!validateTitle()) {
            return;
        }
        if (!validateDesc()) {
            return;
        }


        try {
            Utility.showProgressHUD(mContext);
            // String comment = binding.editComment.getText().toString();

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            if (!filesimage.isEmpty()) {
                for (int i = 0; i < filesimage.size(); i++) {
                    builder.addFormDataPart("images[]", commentModel.getImages().get(i).getName(), RequestBody.create(MediaType.parse("image/*"), commentModel.getImages().get(i)));
                }
            }
            if (!filesimage.isEmpty()) {
                filesimage.clear();
                commentModel = new CommentModel();
            }

            // Log.e(TAG, "getOwnerId>>>>>: " + app.ownerid);

            builder.addFormDataPart(Constant.USER_ID, preferenceHelper.getUserId());
            builder.addFormDataPart("owner_id", app.ownerid);
            builder.addFormDataPart("rating", "" + binding.ratingbar.getRating());
            builder.addFormDataPart("review", binding.edittextDesc.getText().toString());
            builder.addFormDataPart("title", binding.edittextTitle.getText().toString());


            final MultipartBody requestBody = builder.build();

            Constant.retrofitService.submitRating_Review(requestBody).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {

                        try {
                            if (response.isSuccessful()) {
                                String res = response.body().string();
                                //     Log.e(TAG, "onResponse: " + res);


                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getBoolean("status")) {
                                    //    binding.llMain.removeAllViews();
                                    binding.edittextTitle.setText("");
                                    binding.edittextDesc.setText("");
                                    binding.ratingbar.setRating(0);

                                    Utility.hideProgressHud();
                                    Utility.showSnackBar(mContext, binding.edittextTitle, "Successfully edit your review");


                                    Intent intent = new Intent(mContext, ProviderDetails.class);
                                    intent.putExtra("From", "Feedback");
                                    intent.putExtra("OWNERID", app.ownerid);
                                    startActivity(intent);
                                    finish();


                                } else {
                                    Utility.hideProgressHud();
                                    Utility.showSnackBar(mContext, binding.edittextTitle, jsonObject.getString("message"));
                                }


                            } else {
                                // binding.llMain.removeAllViews();
                                //   binding.editComment.setText("");
                                Utility.hideProgressHud();
                                Utility.showAlert(mContext, "Please try after some time.");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void editCommentupload() {

        if (!validaterating()) {
            return;
        }

        if (!validateTitle()) {
            return;
        }
        if (!validateDesc()) {
            return;
        }


        try {
            Utility.showProgressHUD(mContext);
            // String comment = binding.editComment.getText().toString();

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            if (!filesimage.isEmpty()) {
                for (int i = 0; i < filesimage.size(); i++) {
                    builder.addFormDataPart("images[]", commentModel.getImages().get(i).getName(), RequestBody.create(MediaType.parse("image/*"), commentModel.getImages().get(i)));
                }
            }
            if (!filesimage.isEmpty()) {
                filesimage.clear();
                commentModel = new CommentModel();
            }

            //   Log.e(TAG, "getOwnerId>>>>>: " + app.ownerid);

            builder.addFormDataPart(Constant.USER_ID, preferenceHelper.getUserId());
            builder.addFormDataPart("owner_id", app.ownerid);
            builder.addFormDataPart("rating", "" + binding.ratingbar.getRating());
            builder.addFormDataPart("review", binding.edittextDesc.getText().toString());
            builder.addFormDataPart("title", binding.edittextTitle.getText().toString());
            builder.addFormDataPart("review_id", app.feedbackModel.getId());


            final MultipartBody requestBody = builder.build();
            Constant.retrofitService.submitRating_Review(requestBody).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {

                        try {
                            if (response.isSuccessful()) {
                                String res = response.body().string();
                                //      Log.e(TAG, "onResponse: " + res);


                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getBoolean("status")) {
                                    binding.llMain.removeAllViews();
                                    binding.edittextTitle.setText("");
                                    binding.edittextDesc.setText("");
                                    binding.ratingbar.setRating(0);

                                    imageposton = 0;
                                    Utility.hideProgressHud();
                                    Utility.showSnackBar(mContext, binding.edittextTitle, "Successfully edit your review");


                                    Intent intent = new Intent(mContext, ProviderDetails.class);
                                    intent.putExtra("From", "Feedback");
                                    intent.putExtra("OWNERID", app.ownerid);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Utility.hideProgressHud();
                                    Utility.showSnackBar(mContext, binding.edittextTitle, jsonObject.getString("message"));
                                }


                            } else {
                                // binding.llMain.removeAllViews();
                                //   binding.editComment.setText("");
                                Utility.hideProgressHud();
                                Utility.showAlert(mContext, "Please try after some time.");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == marshMallowPermission.CAMERA_READ_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (filesimage.size() <= 4) {

                    takePicture();
                } else {
                    Utility.showSnackBar(mContext, binding.edittextTitle, "Please select minimum five image");
                    return;
                }


            } else {
                Toast.makeText(mContext, "Please accept permission", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == marshMallowPermission.READ_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                showDialog(new String[]{"Gallery"});
                if (filesimage.size() <= 4) {

                    openGallery();
                } else {
                    Utility.showSnackBar(mContext, binding.edittextTitle, "Please select minimum five image");
                    return;
                }

            } else {
                Toast.makeText(mContext, "Please accept permission", Toast.LENGTH_SHORT).show();
            }
        }

    }

    //Return the path of the file.
    public String getPath(Uri uri) {

        try {
            String[] projection = {MediaStore.Images.Media.DATA};
            //   Log.e("OK 1", "" + projection);
            Cursor cursor = managedQuery(uri, projection, null, null, null);
            //   Log.e("OK 2", "" + cursor);
            if (cursor == null) {
                return null;
            }
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            //   Log.e("OK 3", "" + column_index);
            cursor.moveToFirst();
            //  Log.e("OK 4", "" + cursor.getString(column_index));
            return cursor.getString(column_index);
        } catch (Exception e) {
            Toast.makeText(mContext, "Image is too big in resolution please try again", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //  Log.e(TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");

        File file = null;
        if (resultCode != RESULT_OK) {

            return;
        }
        switch (requestCode) {

            case REQUEST_CODE_GALLERY:

                try {

                    try {
                        Uri selectedImageUri = data.getData();
                        String s1 = data.getDataString();
                        //        Log.e("GetPath", s1);
                        //        Log.e("OK", "" + selectedImageUri);

                        String selectedImagePath = getPath(selectedImageUri);
                        if (selectedImagePath == null && s1 != null) {
                            selectedImagePath = s1.replaceAll("file://", "");
                        }

                        long totalSpace = 0;

                        totalSpace = new File(selectedImagePath).length() / 1000;

                        //    Log.e(TAG, "onActivityResult: " + totalSpace);
                        if (totalSpace > 150) {
                            filesimage.add(new File(compressImage(selectedImagePath)));
                            commentModel.setImages(filesimage);
                            commentModel.setId(imageposton);

                            //    Log.e(TAG, "filesimage: " + filesimage.size());
                            //    Log.e(TAG, "commentModel: " + commentModel.getImages().toString());

                        } else {
                            filesimage.add(new File(selectedImagePath));
                            commentModel.setImages(filesimage);
                            commentModel.setId(imageposton);


                            //   Log.e(TAG, "filesimage: " + filesimage.size());
                            //  Log.e(TAG, "commentModel: " + commentModel.getImages().toString());
                        }

                        Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(selectedImagePath));
                        int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);

                        adddyamicimage(scaled);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {


                    // Log.e(TAG, "Error while creating temp file", e);
                }

                break;

            case SELECT_MESSAGES_CAMERA:

                try {
                    if (getPath(fileUri) != null) {
                        FileImagePath = getPath(fileUri);
                        System.out.println("Image Path >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>: " + FileImagePath);
                        //   Log.e(TAG, "SELECT_MESSAGES_CAMERA: in if");

                        if (FileImagePath != null) {
                            file = new File(FileImagePath);
                        }
                        if (file != null) {
                            FileSize = String.valueOf(file.length());

                        }
                        long totalSpace = 0;

                        totalSpace = new File(FileImagePath).length() / 1000;

                        //       Log.e(TAG, "onActivityResult: " + totalSpace);
                        if (totalSpace > 150) {
                            filesimage.add(new File(compressImage(FileImagePath)));
                            commentModel.setImages(filesimage);
                            commentModel.setId(imageposton);

                            //         Log.e(TAG, "filesimage: " + filesimage.size());
                            //         Log.e(TAG, "commentModel: " + commentModel.getImages().toString());
                        } else {
                            filesimage.add(file);
                            commentModel.setImages(filesimage);
                            commentModel.setId(imageposton);
                            //        Log.e(TAG, "filesimage: " + filesimage.size());
                            //        Log.e(TAG, "commentModel: " + commentModel.getImages().toString());
                        }

                        Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(FileImagePath));
                        int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);

                        adddyamicimage(scaled);


                    } else {

                        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                        String cachePath = ""; // you still need a default value if not mounted
                        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !Environment.isExternalStorageRemovable()) {
                            if (getExternalCacheDir() != null) {
                                cachePath = getExternalCacheDir().getPath(); // most likely your null value
                            }
                        } else {
                            if (mContext.getCacheDir() != null) {
                                cachePath = mContext.getCacheDir().getPath();
                            }
                        }

                        String path = cachePath + "" + Calendar.getInstance().getTimeInMillis() + "";

                        File imageFile = savebitmap(bitmap, path);
                        if (imageFile.exists()) {
                            FileImagePath = imageFile.getPath();
                            file = new File(FileImagePath);
                            FileSize = String.valueOf(file.length());
                            long totalSpace = 0;

                            totalSpace = new File(FileImagePath).length() / 1000;

                            //       Log.e(TAG, "onActivityResult: " + totalSpace);
                            if (totalSpace > 150) {
                                filesimage.add(new File(compressImage(FileImagePath)));
                                commentModel.setImages(filesimage);
                                commentModel.setId(imageposton);

                                //          Log.e(TAG, "filesimage: " + filesimage.size());
                                //         Log.e(TAG, "commentModel: " + commentModel.getImages().toString());
                            } else {
                                filesimage.add(file);
                                commentModel.setImages(filesimage);
                                commentModel.setId(imageposton);

                                //         Log.e(TAG, "filesimage: " + filesimage.size());
                                //          Log.e(TAG, "commentModel: " + commentModel.getImages().toString());
                            }


                            Bitmap bitmapImage = BitmapFactory.decodeFile(compressImage(FileImagePath));
                            int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
                            Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);


                            adddyamicimage(scaled);


                        }
                    }
                } catch (Exception e) {
                    //    Log.e(TAG, "Exception: " + e);
                    return;
                }


                break;


        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    //-------------------------------------------------------------------------------------------file comress---------------------------------------------------------------------------------


    public void adddyamicimage(Bitmap scaled) {

        final FrameLayout deleteframeLayout = new FrameLayout(mContext);
        LinearLayout.LayoutParams frahmelayoutParams = new LinearLayout.LayoutParams(300, 300);
        deleteframeLayout.setLayoutParams(frahmelayoutParams);


        ImageView imageview_select = new ImageView(mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(300, 300);
        imageview_select.setLayoutParams(layoutParams);
        imageview_select.setImageBitmap(scaled);
        imageview_select.setPadding(5, 5, 5, 5);
        imageview_select.setScaleType(ImageView.ScaleType.FIT_XY);


        ImageView delete = new ImageView(mContext);
        LinearLayout.LayoutParams imageview_crossParams = new LinearLayout.LayoutParams(50, 50);
        delete.setLayoutParams(imageview_crossParams);
        delete.setImageResource(R.drawable.deleticon);
        delete.setId(imageposton);
        delete.setTag(filesimage.get(imageposton).getName());

        int pos = imageposton;

        //  Log.e(TAG, "imageposton: " + pos);
        //  Log.e(TAG, "imageposton: " + delete.getId());
        imageposton++;

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   Log.e(TAG, "id in GALLERY>>>>>>>>>>>: " + view.getId());
                //   Log.e(TAG, "id in GALLERY>>>>>>>>>>>: " + view.getTag());

                int postition = 0;

                for (int i = 0; i < commentModel.getImages().size(); i++) {


                    if (view.getTag().equals(commentModel.getImages().get(i).getName())) {

                        commentModel.getImages().remove(i);
                        //   Log.e(TAG, "filesimage: " + filesimage.size());
                        //    Log.e(TAG, "commentModel: " + commentModel.getImages().toString());

                        deleteframeLayout.setVisibility(View.GONE);
                        imageposton--;
                    } else {

                        // Log.e(TAG, "onClick: >>>>>>>in else image");
                    }


                }



              /* filesimage.remove(view.getId());
                synchronized(filesimage){
                    filesimage.notify();
                }*/


            }
        });


        deleteframeLayout.addView(imageview_select);
        deleteframeLayout.addView(delete);
        binding.llMain.addView(deleteframeLayout);

    }


    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }


    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private File savebitmap(Bitmap bitmap, String filename) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;

        File file = new File(filename + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");
            // Log.e("file exist", "" + file + ",Bitmap= " + filename);
        }
        try {
            // make a new bitmap from your file
//            Bitmap bitmap = BitmapFactory.decodeFile(file.getName());

            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("file", "" + file);
        return file;

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(mContext, DashBoardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        //    finish();

    }
}