package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityEditProfileBinding;
import com.fairfield.model.UserModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    private ActivityEditProfileBinding binding;
    private Gson gson;
    private PreferenceHelper preferenceHelper;
    private Context mContext;
    private App app;
    private UserModel userModel;
    private static final String TAG = "EditProfileActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        gson = new Gson();
        mContext = this;
        preferenceHelper = new PreferenceHelper(mContext);
        app = App.getInstance();
        userModel = new UserModel();
        userModel = app.userModel;

        binding.setUser(userModel);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(" ");
        }
        binding.myToolbar.getBackground().setAlpha(0);
        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_white));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        binding.inputEmail.setEnabled(false);


        binding.btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                submitForm();
            }
        });
    }


        private void submitForm(){

            if (!validateName()) {
                return;
            }
           /* if (!validateContact()){
                return;
            }*/

            updateProfile();
        }


        private void updateProfile(){
                String fullname = binding.inputName.getText().toString().trim();
               // String contact = binding.inputPhonenumber.getText().toString().trim();


            HashMap<String , String> hashMap = new HashMap<>();
            hashMap.put(Constant.USER_ID ,preferenceHelper.getUserId());
            hashMap.put("first_name" ,fullname);
          //  hashMap.put("contact" ,"0000000000");


            Utility.showProgressHUD(mContext);
            Constant.retrofitService.updateProfile(hashMap).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Utility.hideProgressHud();
                    if (response.isSuccessful()) {
                        try {
                            String responseString = response.body().string();



                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.getBoolean("status")) {

                                userModel = gson.fromJson(jsonObject.getString("data"), UserModel.class);
                                app.userModel = userModel;



                                Intent intent = new Intent(mContext, DashBoardActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();

                            } else {
                                Utility.showAlert(mContext, jsonObject.getString("message"));
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utility.showAlert(mContext, "Please try after some time.");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Utility.hideProgressHud();
                }
            });
        }



    private boolean validateName() {
        if (binding.inputName.getText().toString().trim().isEmpty()) {
            binding.inputLayoutName.setError(getString(R.string.err_msg_name));
            requestFocus(binding.inputName);
            return false;
        } else {
            binding.inputLayoutName.setErrorEnabled(false);
        }

        return true;
    }


/*
    private boolean validateContact() {
        if (binding.inputPhonenumber.getText().toString().trim().isEmpty()) {
            binding.inputPhonenumber.setError(getString(R.string.err_msg_contact));
            requestFocus(binding.inputPhonenumber);
            return false;

        } else {
            binding.inputLayoutPhonenumber.setErrorEnabled(false);
        }

        return true;
    }
*/

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
           getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }



}
