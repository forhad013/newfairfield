package com.fairfield.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Context;
import android.widget.Toast;

import com.fairfield.R;
import com.fairfield.databinding.ActivityRewardCommentBinding;
import com.fairfield.utility.Constant;
import com.fairfield.utility.DownloadImageTask;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.MarshMallowPermission;
import com.fairfield.utility.Utility;
import com.fairfield.widgets.Util;

import android.support.v4.content.FileProvider;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RewardCommentActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityRewardCommentBinding binding;
    private Context mContext;
    private HashMap<String , String> hashMap;
    public int reward_id ;
    public static String TAG = "RewardCommentAcitivty";
    private MarshMallowPermission marshMallowPermission;

    private static int ENTERBTN_ID = 9910;
    private static int SUBMITBTN_ID = 9911;
    private static int GALLERYBTN_ID = 9912;
    private static int CAMERABTN_ID = 9913;
    private static int COMMENTBTN_ID = 9914;
    private PreferenceHelper preferenceHelper;

    public static final int RESULT_LOAD_IMAGE = 0x1;
    public static final int CAMERA_REQUEST = 0x2;
    public String imageUrl;
    public String mCurrentPhotoPath;

    public boolean isEntered;
    public int enteredNum = 0;

    public Uri selectedImage = null;
    public Bitmap uploadImage = null;

    private byte[] dataimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this , R.layout.activity_reward_comment);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }


        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });


        binding.btnEnter.setId(ENTERBTN_ID);
        binding.btnEnter.setOnClickListener(this);

        binding.btnSubmitcomment.setId(SUBMITBTN_ID);
        binding.btnSubmitcomment.setOnClickListener(this);

        binding.fabPickCamera.setId(CAMERABTN_ID);
        binding.fabPickCamera.setOnClickListener(this);

        binding.fabPickGallery.setId(GALLERYBTN_ID);
        binding.fabPickGallery.setOnClickListener(this);
        binding.imgComment.setVisibility(View.GONE);

        binding.commentBtn.setId(COMMENTBTN_ID);
        binding.commentBtn.setOnClickListener(this);


        mContext = this;
        preferenceHelper = new PreferenceHelper(mContext);
        marshMallowPermission = new MarshMallowPermission(mContext);

        reward_id = this.getIntent().getIntExtra("Reward_id", 0);
        is_user_entered_reward();

    }

    private void is_user_entered_reward() {

        startAnim();
        hashMap = new HashMap<String, String>();
        hashMap.put("reward_id", String.valueOf(reward_id));
        hashMap.put("user_id", preferenceHelper.getUserId());

        Constant.retrofitService.is_user_entered_reward(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()){
                    try {
                        String responseString = response.body().string();
                        if (responseString.equals("YES")) {
                            isEntered = true;
                            binding.btnEnter.setVisibility(View.GONE);
                            binding.enteredTxt.setVisibility(View.GONE);
                        }
                        else
                            isEntered = false;
                        view_reward();
                    } catch (Exception e) {
                        binding.btnEnter.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                } else binding.btnEnter.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                binding.btnEnter.setVisibility(View.GONE);

            }
        });
    }

    private  void view_reward(){

        hashMap = new HashMap<String, String>();
        hashMap.put("reward_id", String.valueOf(reward_id));
        Constant.retrofitService.view_reward(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) try {
                    String responseString = response.body().string();

                    JSONObject jsonObject = new JSONObject(responseString);
                    JSONObject rewardJson = jsonObject.getJSONObject("reward");

                    enteredNum = jsonObject.getInt("total_participants");
                    if (enteredNum > 1) {
                        binding.enteredTxt.setText(String.valueOf(enteredNum) + "  Entries");
                    } else {
                        binding.enteredTxt.setText(String.valueOf(enteredNum) + "  Entry");
                    }

                    String titleStr = rewardJson.getString("title");
                    binding.titleTxt.setText(titleStr);
                    binding.descTxt.setText(rewardJson.getString("description"));
                    if (rewardJson.getInt("can_comment") == 1) {
                        binding.commentBtn.setVisibility(View.VISIBLE);
                        binding.edtComment.setVisibility(View.VISIBLE);
                        binding.fabPickGallery.setVisibility(View.VISIBLE);
                        binding.fabPickCamera.setVisibility(View.VISIBLE);
                        binding.btnSubmitcomment.setVisibility(View.VISIBLE);
                    } else {
                        binding.commentBtn.setVisibility(View.GONE);
                        binding.edtComment.setVisibility(View.GONE);
                        binding.fabPickGallery.setVisibility(View.GONE);
                        binding.fabPickCamera.setVisibility(View.GONE);
                        binding.btnSubmitcomment.setVisibility(View.GONE);
                    }

                    String imageStr = rewardJson.getString("image");
                    if (imageStr == null) imageStr = "";

                    if (imageStr.isEmpty()) {
                        binding.titleImg.setVisibility(View.GONE);
                    } else {
                        binding.titleImg.setVisibility(View.VISIBLE);
                        imageStr = Constant.REWARDIMG_URL + imageStr;
                        new DownloadImageTask(binding.titleImg).execute(imageStr);
                    }


                    if (rewardJson.getInt("can_enter") != 1) {
                        binding.enteredTxt.setVisibility(View.GONE);
                        binding.btnEnter.setVisibility(View.GONE);
                    } else {
                        binding.enteredTxt.setVisibility(View.VISIBLE);
                        binding.btnEnter.setVisibility(View.VISIBLE);
                    }

                    if (isEntered) {
                        binding.enteredTxt.setAlpha(0.3f);
                        binding.btnEnter.setEnabled(false);
                        binding.btnEnter.setAlpha(0.3f);
                        binding.btnEnter.setText("Entered");
                    } else {
                        binding.enteredTxt.setAlpha(1f);
                        binding.btnEnter.setEnabled(true);
                        binding.btnEnter.setAlpha(1f);
                        binding.btnEnter.setText("Enter");
                    }


                    JSONArray commentsArray = jsonObject.getJSONArray("comments");
                    stopAnim();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Failed", "Failed");
                Toast.makeText(RewardCommentActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.INVISIBLE);
        // or avi.smoothToHide();
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == GALLERYBTN_ID) {
            if (marshMallowPermission.isPermissionForReadExtertalStorage()) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            } else {
                try {
                    marshMallowPermission.requestPermissionForReadExtertalStorage();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else if (view.getId() == CAMERABTN_ID) {

            if (marshMallowPermission.isPermissionForCamera_And_ReadExternal()) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        ex.printStackTrace();
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(RewardCommentActivity.this,
                                "com.neighborhood.app.pictures",
                                photoFile);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                }
            } else {
                try {
                    marshMallowPermission.requestPermissionForCamera_And_ReadExternal();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (view.getId() == ENTERBTN_ID) {
            add_reward_participant();
        } else if (view.getId() == COMMENTBTN_ID) {
            Intent mIntent = new Intent(this, RewardTableActivity.class);
            mIntent.putExtra("Reward_ID", reward_id);
            startActivity(mIntent);
        } else if (view.getId() == SUBMITBTN_ID) {
            uploadComment();
        }
    }


    public void uploadComment() {

        try {
            Utility.showProgressHUD(this);
            final MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            String url = Environment.getExternalStorageDirectory().getAbsolutePath() + "/LNA/image.png";

            if (uploadImage != null) {
                File file = new File(url);
                dataimage = getImageUri(this,uploadImage);
                FileUtils.writeByteArrayToFile(file, dataimage);
                builder.addFormDataPart("image", "image.png", RequestBody.create(MediaType.parse("image/*"), file));
            } else {
                if (binding.edtComment.getText().toString().isEmpty()) {
                    AlertDialog.Builder build = new AlertDialog.Builder(mContext);
                    build.setTitle("")
                            .setMessage("You must submit a comment\n or a photo.")
                            .create();
                    build.show();
                    Utility.hideProgressHud();
                    return;
                }
            }
            uploadImage = null;
            builder.addFormDataPart(Constant.USER_ID, preferenceHelper.getUserId());
            builder.addFormDataPart("reward_id", String.valueOf(reward_id));
            builder.addFormDataPart("comment", binding.edtComment.getText().toString());
            final MultipartBody requestBody = builder.build();
            Constant.retrofitService.uploadComment(requestBody).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful()) {
                            String responseStr = response.body().toString();
                            binding.edtComment.setText("");
                            binding.imgComment.setVisibility(View.GONE);
                        } else {
                            Utility.hideProgressHud();
                            Toast.makeText(RewardCommentActivity.this, "Failded To Upload Comment", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Utility.hideProgressHud();
                    uploadImage = null;
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(TAG, "Failure Detected");
                    Utility.hideProgressHud();
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "Exception Detected");
            Utility.hideProgressHud();
        }
    }

    private void add_reward_participant() {

        startAnim();
        hashMap = new HashMap<String, String>();
        hashMap.put("reward_id", String.valueOf(reward_id));
        hashMap.put("user_id", preferenceHelper.getUserId());

        Constant.retrofitService.add_reward_participant(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) try {
                    String responseString = response.body().string();
                    JSONObject jsonObject = new JSONObject(responseString);
                    Boolean status = jsonObject.getBoolean("status");
                    if (status) {

                        enteredNum += 1;
                        if (enteredNum > 1) {
                            binding.enteredTxt.setText(String.valueOf(enteredNum) + "  Entries");
                        } else {
                            binding.enteredTxt.setText(String.valueOf(enteredNum) + "  Entry");
                        }

                        binding.enteredTxt.setAlpha(0.3f);
                        binding.btnEnter.setEnabled(true);
                        binding.btnEnter.setAlpha(0.3f);
                        binding.btnEnter.setText("Entered");
                    }
                    stopAnim();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == marshMallowPermission.CAMERA_READ_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        ex.printStackTrace();
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(RewardCommentActivity.this,
                                "com.neighborhood.app.pictures",
                                photoFile);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                }
            } else {
                Toast.makeText(mContext, "Please accept permission", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == marshMallowPermission.READ_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                } else {
                    Toast.makeText(mContext, "Please accept permission", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(mContext, "Please accept permission", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            selectedImage = data.getData();
            imageUrl = getRealPathFromURI(selectedImage, this);
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            //dataimage = getImageUri(this,(Bitmap)(BitmapFactory.decodeFile(picturePath)));
            Bitmap bitmap = (Bitmap)BitmapFactory.decodeFile(picturePath);
            try {
                ExifInterface ei = new ExifInterface(picturePath);
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(bitmap, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(bitmap, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(bitmap, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = bitmap;
                }
                binding.imgComment.setVisibility(View.VISIBLE);
//                uploadImage = rotatedBitmap;//scaleBitmap(rotatedBitmap);
                uploadImage = scaleBitmap(rotatedBitmap);
                binding.imgComment.setImageBitmap(uploadImage);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {

            int targetW = getWindowManager().getDefaultDisplay().getWidth() - 40;
            //int targetH = binding.imgComment.getHeight();

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            //int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
            int scaleFactor = photoW/targetW;

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

            try {
                ExifInterface ei = new ExifInterface(mCurrentPhotoPath);
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(bitmap, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(bitmap, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(bitmap, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL
                            :
                    default:
                        rotatedBitmap = bitmap;
                }

                binding.imgComment.setVisibility(View.VISIBLE);
                uploadImage = scaleBitmap(rotatedBitmap);
                binding.imgComment.setImageBitmap(uploadImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap scaleBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        int maxWidth = getWindowManager().getDefaultDisplay().getWidth() - 40;
        int maxHeight = (int)((float)maxWidth*(float)height/(float)width);


        bm = Bitmap.createScaledBitmap(bm, maxWidth, maxHeight, true);
        return bm;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public byte[] getImageUri(Context inContext, Bitmap inImage) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        byte[] array = bytes.toByteArray();
        return array;
    }

    public Uri getImageUrl(Context inContext, Bitmap inImage) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        Uri url = null;
        try {
            url = Uri.parse(path);
        } catch (Exception e) {
            this.finish();
        }
        return url;
    }

    public String getRealPathFromURI(Uri contentURI, Activity context) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = context.managedQuery(contentURI, projection, null,
                null, null);
        if (cursor == null)
            return null;
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        if (cursor.moveToFirst()) {
            String s = cursor.getString(column_index);
            // cursor.close();
            return s;
        }
        // cursor.close();
        return null;
    }

}
