package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.fairfield.R;
import com.fairfield.adapter.ProvidersAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivitySearchBinding;
import com.fairfield.model.ProvidersModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {
    ActivitySearchBinding binding;

    private static final String TAG = "SearchActivity";
    private MaterialSearchView searchView;
    App app;
    Context mContext;
    AVLoadingIndicatorView avi;
    TextView textview_nodata;


    RecyclerView recyclerView;
    private HashMap<String, String> hashMap;
    private ArrayList<ProvidersModel> newprovidersModelArrayList;
    private Gson gson;
    private ProvidersAdapter adapter;
    private PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        app = App.getInstance();
        mContext = this;
        gson = new Gson();
        newprovidersModelArrayList = new ArrayList<>();
        preferenceHelper = new PreferenceHelper(mContext);
        searchView = new MaterialSearchView(mContext);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_search);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Search for Services");
        }

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        textview_nodata = (TextView) findViewById(R.id.textview_nodata);
        searchView.setVoiceSearch(true);
        //   searchView.setCursorDrawable(R.drawable.color_cursor_white);
        searchView.setEllipsize(true);



        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_fixer);

        getProvidersDatasearch("" ,"0");
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.getLayoutManager().setAutoMeasureEnabled(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);




        adapter = new ProvidersAdapter(mContext, newprovidersModelArrayList);
        recyclerView.setAdapter(adapter);


        ((ProvidersAdapter) recyclerView.getAdapter()).setOnItemClickListener(new ProvidersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
             //   Log.e(TAG, "onItemClick() called with: view = [" + ((ProvidersAdapter) recyclerView.getAdapter()) + "], position = [" + position + "]");
                app.providersModel = adapter.getProvidersModel().get(position);
              //  Log.e(TAG, "onItemClick() Des>>>>>>>>>>>>>>>= [" + app.providersModel + "]");

             //   Log.e(TAG, "onItemClick: click ");

              /*  Intent intent = new Intent(mContext, CategoryDetail.class);
                startActivity(intent);*/

                Intent intent = new Intent(mContext, ProviderDetails.class);
                startActivity(intent);
            }
        });


        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

            //   Log.e(TAG, "onQueryTextSubmit() called with: query = [" + query + "]");
                getProvidersDatasearch(query , "1");

                searchView.hideKeyboard(searchView);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
             //   Log.e(TAG, "onQueryTextChange() called with: newText = [" + query + "]");
                getProvidersDatasearch(query , "0");

                return false;
            }
        });


    }

    public void getProvidersData(String query) {
        startAnim();

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("search_keyword", query);


        Constant.retrofitService.searchBusinessOwner(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);
                        stopAnim();
                        if (jsonObject.getBoolean("status")) {
                       //     Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<ProvidersModel>>() {
                            }.getType();

                            if (!newprovidersModelArrayList.isEmpty()) {
                                newprovidersModelArrayList.clear();
                            }
                            newprovidersModelArrayList = gson.fromJson(jsonObject.getString("data"), type);

                        //    Log.e("ArrayList", "" + newprovidersModelArrayList.size());
                        //    Log.e(TAG, "onResponse() called with: call = [" + call + "], localEventsArrayList = [" + newprovidersModelArrayList.size() + "]");
                            adapter.setProvidersModelList(newprovidersModelArrayList);
                            adapter.notifyDataSetChanged();
                        } else {

                            stopAnim();
                        }
                    } else {

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            //    Log.e(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]", t);

            }
        });
    }


    public void getProvidersDatasearch(String query , String value) {


        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("search_keyword", query);
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("value_match" ,value);

        Constant.retrofitService.searchBusinessOwner(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {
                       //     Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<ProvidersModel>>() {
                            }.getType();

                            if (!newprovidersModelArrayList.isEmpty()) {
                                newprovidersModelArrayList.clear();
                            }


                            newprovidersModelArrayList = gson.fromJson(jsonObject.getString("data"), type);
                            textview_nodata.setVisibility(View.GONE);
                        //    Log.e("ArrayList", "" + newprovidersModelArrayList.size());
                         //   Log.e(TAG, "onResponse() called with: call = [" + call + "], localEventsArrayList = [" + newprovidersModelArrayList.size() + "]");
                            adapter.setProvidersModelList(newprovidersModelArrayList);
                            adapter.notifyDataSetChanged();
                        } else {


                            if (!newprovidersModelArrayList.isEmpty()) {
                                newprovidersModelArrayList.clear();
                            }
                            textview_nodata.setVisibility(View.VISIBLE);

                            adapter.setProvidersModelList(newprovidersModelArrayList);
                            adapter.notifyDataSetChanged();
                        }
                    } else {


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            //    Log.e(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]", t);

            }
        });
    }

    void startAnim() {
        avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        searchView.showSearch();

        // searchView.setHint(" ");
        searchView.setHintTextColor(Color.GRAY);
        //searchView.setBackgroundColor(getResources().getColor(R.color.white));

       // searchView.setBackgroundColor(Color.GRAY);

        return true;
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        searchView.requestFocus();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
      //  Log.e(TAG, "onResume: ");
        getProvidersDatasearch("" ,"0");
    }
}
