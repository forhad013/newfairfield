package com.fairfield.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.fairfield.R;
import com.fairfield.databinding.ActivityTermAndConditionBinding;

public class TermAndConditionActivity extends AppCompatActivity {

    private static final String TAG = "TermAndConditionActivity";
    ActivityTermAndConditionBinding binding;
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_term_and_condition);
        mContext = this;

        binding.webView.loadUrl("https://www.localneighborhoodapp.com/user/terms_condition");

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
