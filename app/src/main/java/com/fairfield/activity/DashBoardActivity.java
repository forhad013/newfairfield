package com.fairfield.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityDashBoardBinding;
import com.fairfield.utility.CheckNetworkConnectivity;
import com.fairfield.utility.ConnectivityReceiver;
import com.fairfield.utility.Constant;
import com.fairfield.utility.FirebaseSubscribe;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.fairfield.Intentservice.MyAlarmReceiver;

public class DashBoardActivity extends AppCompatActivity {

    ActivityDashBoardBinding binding;
    Context mContext;
    private static final String TAG = "DashBoardActivity";
    private App app;
    private PreferenceHelper preferenceHelper;
    //int badgeCount = 0;
    private CheckNetworkConnectivity mCheckNetworkConnectivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        app = App.getInstance();
        preferenceHelper = new PreferenceHelper(mContext);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dash_board);
        mCheckNetworkConnectivity = new CheckNetworkConnectivity(
                getApplicationContext());

        setSupportActionBar(binding.myToolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }
        binding.myToolbar.getBackground().setAlpha(0);


        try {
            //  scheduleGeofencing();

            // startService(new Intent(this, MyService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }



        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);


        Log.e(TAG, "showwelcome: " + preferenceHelper.getShowDialogCount());

        if (preferenceHelper.getShowDialogCount() == 0) {

            preferenceHelper.putShowDialogCount(1);
           // showWelcomeDialog();
        }


        try {
            FirebaseMessaging.getInstance().subscribeToTopic(preferenceHelper.getUserId() + "");
        } catch (Exception e) {
            e.printStackTrace();
        }


        binding.imageviewPromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {

                    Intent intent = new Intent(mContext, PramotionActivity.class);
                    startActivity(intent);

                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }

            }
        });

        binding.imageviewFacebookgroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {

                    Intent intent = new Intent(mContext, FacebookGroupActivity.class);
                    intent.putExtra("url", "https://www.facebook.com/groups/20947417401/");
                    startActivity(intent);

                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));

                }

            }
        });

        binding.imageviewImportant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {

                    Intent intent = new Intent(mContext, ImportantActivity.class);
                    startActivity(intent);

                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
            }
        });


        binding.imageviewBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ConnectivityReceiver.isConnected()) {

                    Intent intent = new Intent(mContext, BusinessActivity.class);
                    startActivity(intent);

                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
            }
        });

        binding.imageviewLocalevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {

                    Intent intent = new Intent(mContext, LocalEventActivity.class);
                    startActivity(intent);


                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
            }
        });

        binding.imageviewNewsfeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {
                    Intent intent = new Intent(mContext, NewsFeedActivity.class);
                    intent.putExtra("Fragment", "NewBusinessFragment");
                    startActivity(intent);
                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
            }
        });


        binding.imageviewCrimenews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {

                    Intent intent = new Intent(mContext, CrimeNewsActivity.class);
                    startActivity(intent);

                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
            }
        });


        binding.imageviewReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {

                    Intent intent = new Intent(mContext, RewardActivity.class);
                    startActivity(intent);
                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
            }
        });

        binding.imageviewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    Intent intentSearch = new Intent(mContext, SearchActivity.class);
                    startActivity(intentSearch);

                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
            }
        });

        binding.imageviewSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {

                    Intent intent = new Intent(mContext, SupportActivity.class);
                    startActivity(intent);
                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
            }
        });


        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String count = intent.getStringExtra("count");


                try {

                    setCountNotification(count);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        LocalBroadcastManager.getInstance(mContext).registerReceiver(receiver, new IntentFilter("notification"));

        Log.e("prev",preferenceHelper.getshareprefdataBoolean(PreferenceHelper.PREVIOUS_USER)+"");

        if(!preferenceHelper.getshareprefdataBoolean(PreferenceHelper.PREVIOUS_USER)) {
            FirebaseSubscribe firebaseSubscribe = new FirebaseSubscribe(getApplicationContext());

            firebaseSubscribe.subscribeToTopics();
        }
    }

    private void setCountNotification(String count) {

        invalidateOptionsMenu();

    }


    @Override
    protected void onResume() {
        checkAnnouncement();
        super.onResume();
    }

    public void checkAnnouncement() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", new PreferenceHelper(mContext).getUserId());
        //  Log.e(TAG, "checkUser() called" + hashMap);

        //Utility.showProgressHUD(mContext);
        Constant.retrofitService.checkAnnouncement(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
               // Utility.hideProgressHud();
                if (response.isSuccessful()) {
                    try {
                        String responseString = response.body().string();
                        //           Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + responseString + "]");

                        JSONObject jsonObject = new JSONObject(responseString);
                     //   if (jsonObject.getString("type").equals("new_announcement")) {

                        if(jsonObject.toString().equals("{}")){
                        Log.e("jsonObject",jsonObject+"");

                          //  String owner_id = jsonObject.getString("owner_id");
                            String image =   jsonObject.getString("image");

                            String title = jsonObject.getString("title");
                            String msg =   jsonObject.getString("body")+"";


                            String orginalLink = Constant.IMAGE_URL+"notifications/"+image;

                            showDialoge(title,msg,orginalLink);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(mContext, "Please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            //    Utility.hideProgressHud();
            }
        });
    }

    public void showDialoge( String titleText, String messageText, String image){
        final Dialog dialog = new Dialog(DashBoardActivity.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Log.e("image",image);
        dialog.setContentView(R.layout.announce_popup_layout);
        // Set dialog title
        //dialog.setTitle("Category Add");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();



        Button done = (Button) dialog.findViewById(R.id.okBtn);

        TextView title = (TextView) dialog.findViewById(R.id.title);
        ImageView icon = (ImageView) dialog.findViewById(R.id.icon);

        TextView message = (TextView) dialog.findViewById(R.id.message);






        if(image.equals("")){
            icon.setImageResource(R.drawable.fairfield_icon);
        }else{
            Picasso.with(mContext).load(image).into(icon);
        }

        message.setText(messageText);
        title.setText(titleText);

        //   Log.e("text",text);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });



    }



   /* private Drawable buildCounterDrawable(int Counttask, int backgroundImageId) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.counter_menuitem_layout, null);
        view.setBackgroundResource(backgroundImageId);


        if (Counttask == 0) {
            View counterTextPanel = view.findViewById(R.id.counterValuePanel);
            counterTextPanel.setVisibility(View.GONE);
        } else {
            TextView textView = (TextView) view.findViewById(count);
            textView.setText("" + Counttask);
        }

        view.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        return new BitmapDrawable(getResources(), bitmap);
    }
*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_share:
                inviteFriends();
                break;


            case R.id.settingAction:
                Intent intentSearch = new Intent(mContext, SettingPageActivity.class);
                startActivity(intentSearch);

                break;

            case R.id.item_samplebadge:
                app.badgeCount = 0;
                ActionItemBadge.update(item, app.badgeCount);
                invalidateOptionsMenu();
                if (ConnectivityReceiver.isConnected()) {
                    Intent intentnoti = new Intent(mContext, NotificationActivity.class);
                    startActivity(intentnoti);
                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.editprofilerequestAction:
                if (ConnectivityReceiver.isConnected()) {
                    Intent intent = new Intent(mContext, EditProfileActivity.class);
                    startActivity(intent);
                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }

                break;

            case R.id.changepasswordtAction:

                if (ConnectivityReceiver.isConnected()) {
                    Intent intent_change_pass = new Intent(mContext, ChangePassword.class);
                    startActivity(intent_change_pass);

                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }


                break;

           /* case R.id.feedbackrequestAction:

                Intent intent_feedback = new Intent(mContext, FeedbackActivity.class);
                startActivity(intent_feedback);

                break;
*/
            case R.id.savedevents_Action:
                if (ConnectivityReceiver.isConnected()) {
                    Intent intent_saved_events = new Intent(mContext, SavedEventsActivity.class);
                    startActivity(intent_saved_events);
                } else {

                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.savedcouponsAction:
                if (ConnectivityReceiver.isConnected()) {
                    Intent intent_saved_coupons = new Intent(mContext, SavedCouponsActivity.class);
                    startActivity(intent_saved_coupons);
                } else {
                    Utility.showSnackBar(mContext, binding.imageviewPromotion, getResources().getString(R.string.no_internet));
                }
                break;

            case R.id.logoutAction:

                try {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(preferenceHelper.getUserId() + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                app = new App();
                Utility.clearAllSharedPreferences(mContext);
                preferenceHelper.Logout();
                Intent intentlogout = new Intent(mContext, SplashActivity.class);
                intentlogout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentlogout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                intentlogout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentlogout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentlogout);
                finish();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard, menu);

        //you can add some logic (hide it if the count == 0)
        if (app.badgeCount > 0) {
            ActionItemBadge.update(this, menu.findItem(R.id.item_samplebadge), getResources().getDrawable(R.drawable.icon_notification), ActionItemBadge.BadgeStyles.BLUE, app.badgeCount);
        } else {
            // ActionItemBadge.hide(menu.findItem(R.id.item_samplebadge));

            ActionItemBadge.update(this, menu.findItem(R.id.item_samplebadge), getResources().getDrawable(R.drawable.icon_notification), ActionItemBadge.BadgeStyles.BLUE, null);
        }


        return super.onCreateOptionsMenu(menu);
    }

    void inviteFriends() {
        //InviteFriendsActivity.start(this);

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
      /*  sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi,\n" +
                " I am inviting you to join Fairfiald, It  is a social networking , where you can Chat, Group Video call to your friends and Enjoy.\n" +
                "where you can Chat, Group Video call to your friends and Enjoy.\n" + "http://web.kholkedekho.com/download-app/" );
*/
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi,\n" +
                "I am inviting you to join Fairfiald, It  " + "https://play.google.com/store/apps/details?id=com.fairfield");

        //  sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, url_to_share);
        startActivity(Intent.createChooser(sharingIntent, "Share Using"));
    }

/*
    public void scheduleGeofencing() {
        // Construct an intent that will execute the AlarmReceiver
        Intent intent = new Intent(getApplicationContext(), MyAlarmReceiver.class);
        // Create a PendingIntent to be triggered when the alarm goes off
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, MyAlarmReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Setup periodic alarm every every half hour from this point onwards

        long firstMillis = System.currentTimeMillis(); // alarm is set right away
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        // First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
        // Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY
        // alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis,
        //        firstMillis + 5000, pIntent);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis(), 6000,
                pIntent);
    }
*/


    private void showWelcomeDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
        builder.setNegativeButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        final AlertDialog dialog = builder.create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.row_welcomedialog, null);
        dialog.setView(dialogLayout);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.show();


    }


}
