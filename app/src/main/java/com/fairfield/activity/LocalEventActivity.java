package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.fairfield.R;
import com.fairfield.adapter.LocalEventsAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityLocalEventBinding;
import com.fairfield.model.BannerModel;
import com.fairfield.model.LocalEventsModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocalEventActivity extends AppCompatActivity {

    private ActivityLocalEventBinding binding;
    private Context mContext;
    private PreferenceHelper preferenceHelper;
    private HashMap<String, String> hashMap;
    private static final String TAG = "LocalEventActivity";
    private ArrayList<LocalEventsModel> localEventsArrayList;
    private Gson gson;
    private LocalEventsAdapter adapter;
    private App app;
    Handler mHandler;
    Runnable mUpdateResults;
    private Bitmap imagegd;
    int[] IMAGE_IDS = {R.drawable.banner, R.drawable.btn_fb, R.drawable.like,
            R.drawable.new_logo};


    public int currentimageindex = 0;
    Timer timer;
    TimerTask task;


    ImageView[] imgv;
    ArrayList<BannerModel> bannerArrayList;
    //MyCounter MCObject;
    boolean leftToRight = true;
    int count = 0;
    int token = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_local_event);
        setSupportActionBar(binding.myToolbar);
        mContext = this;
        gson = new Gson();
        app = App.getInstance();
        preferenceHelper = new PreferenceHelper(mContext);
        localEventsArrayList = new ArrayList<>();
        bannerArrayList = new ArrayList<>();
        timer = new Timer();
        mHandler = new Handler();


        try {
            token = getIntent().getIntExtra("token", 0);
        }catch (Exception e){

        }


        if (!localEventsArrayList.isEmpty()) {
            localEventsArrayList.clear();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        binding.recyclerViewLocalevent.setLayoutManager(new GridLayoutManager(mContext, 2));
        binding.recyclerViewLocalevent.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerViewLocalevent.setNestedScrollingEnabled(false);
        binding.recyclerViewLocalevent.setHasFixedSize(false);

        adapter = new LocalEventsAdapter(mContext, localEventsArrayList);
        binding.recyclerViewLocalevent.setAdapter(adapter);
        getLocalEvents();

        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(token==1){
                    Intent intent = new Intent(mContext, DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    finish();
                }
            }
        });


        ((LocalEventsAdapter) binding.recyclerViewLocalevent.getAdapter()).setOnItemClickListener(new LocalEventsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
             //   Log.e(TAG, "onItemClick() called with: view = [" + ((LocalEventsAdapter) binding.recyclerViewLocalevent.getAdapter()) + "], position = [" + position + "]");
                app.localeventsModel = adapter.getLocalEventsModel().get(position);
            //    Log.e(TAG, "onItemClick() Des>>>>>>>>>>>>>>>= [" + app.localeventsModel + "]");

            //    Log.e(TAG, "onItemClick: click ");

              /*  Intent intent = new Intent(mContext, CategoryDetail.class);
                startActivity(intent);*/

                Intent intent = new Intent(mContext, EventDetailsActivity.class);
                startActivity(intent);
            }
        });


        binding.slidingimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BannerModel banner = bannerArrayList.get(currentimageindex - 1);
                bannerClick(banner.getId(), banner.getOwnerId());



              /*  AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);
                dialog.setView(dialogLayout);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.show();


                ImageView image = (ImageView) dialog.findViewById(R.id.goProDialogImage);
                //  Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),
                //      R.drawable.whygoprodialogimage);

                try {
                    String url = "https://www.localneighborhoodapp.com/images/banner/";
                    String urlmain = url + bannerArrayList.get(currentimageindex - 1).getImage();
                    Log.e(TAG, "AnimateandSlideShow: " + urlmain);
                    Picasso.with(mContext).load(urlmain).into(image);

                } catch (Exception e) {
                    System.out.println(e);
                }
*/

            }
        });


    }


    private void AnimateandSlideShow() {



        try {
            String url = "https://www.localneighborhoodapp.com/images/banner/";

            String urlmain = url + bannerArrayList.get(currentimageindex).getImage();
           // Log.e(TAG, "AnimateandSlideShow: " + urlmain);
            Picasso.with(mContext).load(urlmain).into(binding.slidingimage);
          //  Log.e(TAG, "AnimateandSlideShow: " + currentimageindex);

          //saveBannerview(bannerArrayList.get(currentimageindex).getId(), bannerArrayList.get(currentimageindex).getOwnerId());

            currentimageindex++;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void getLocalEvents() {
        startAnim();

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        binding.textviewNoEvent.setVisibility(View.GONE);
        Constant.retrofitService.getLocalEvents(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);
                        stopAnim();
                        if (jsonObject.getBoolean("status")) {
                            //Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<LocalEventsModel>>() {
                            }.getType();

                            if (!localEventsArrayList.isEmpty()) {
                                localEventsArrayList.clear();
                            }
                            localEventsArrayList = gson.fromJson(jsonObject.getString("data"), type);

                           // Log.e("ArrayList", "" + localEventsArrayList.size());
                          //  Log.e(TAG, "onResponse() called with: call = [" + call + "], localEventsArrayList = [" + localEventsArrayList.size() + "]");
                            adapter.setLocalEventsModelList(localEventsArrayList);
                            adapter.notifyDataSetChanged();

                            getAllCoupons();
                        } else {
                            stopAnim();

                            binding.textviewNoEvent.setVisibility(View.VISIBLE);
                        }
                    } else {
                        stopAnim();
                        binding.textviewNoEvent.setVisibility(View.VISIBLE);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

             //   Log.e(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]", t);

            }
        });
    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

    private void bannerClick(String bannerId, final String ownerId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", String.valueOf(bannerId));
        hashMap.put("owner_id", String.valueOf(ownerId));
        Constant.retrofitService.bannerClick(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        JSONObject jsonObject = new JSONObject(responseString);

                        if (jsonObject.getBoolean("status")) {
                            Intent intent = new Intent(mContext, ProviderDetails.class);
                            intent.putExtra("From", "Ad");
                            intent.putExtra("OWNERID", ownerId);
                            startActivity(intent);
                        } else {
                            // show message error
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utility.showAlert(mContext, "something went wrong");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.showAlert(mContext, "something went wrong");
            }
        });
    }

    private void getAllCoupons() {

        Constant.retrofitService.getAllbanners().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {
                         //   Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<BannerModel>>() {
                            }.getType();

                            if (!bannerArrayList.isEmpty()) {
                                bannerArrayList.clear();
                            }
                            bannerArrayList = gson.fromJson(jsonObject.getString("data"), type);
                        //    Log.e("ArrayList", "" + bannerArrayList.size());

                            try {
                                bannerStart();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    } else {


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void bannerStart() {
        try {
            // Create runnable for posting
            mUpdateResults = new Runnable() {
                public void run() {

                    if (bannerArrayList.size() == currentimageindex) {
                        currentimageindex = 0;
                        AnimateandSlideShow();
                    } else {
                        AnimateandSlideShow();
                    }


                }
            };

            int delay = 1000; // delay for 1 sec.
            int period = 2000; // repeat every 4 sec.

            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {
                    mHandler.post(mUpdateResults);
                }

            }, delay, period);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
       // Log.e(TAG, "onStop: ");


        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
      //  Log.e(TAG, "onRestart: ");
        try {
            mHandler = new Handler();
            timer = new Timer();
            if (!bannerArrayList.isEmpty()){
                bannerStart();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void saveBannerview(String banner_id, String owner_id) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", banner_id);
        hashMap.put("owner_id", owner_id);

      //  Log.e(TAG, "saveBannerview: " + hashMap);

        Constant.retrofitService.saveBannerview(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    //------------------------------------------------------------------------------------------------------------------for add--------------------------------------------------------

/*
    private void getAllCoupons() {
        Constant.retrofitService.getAllbanners().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {
                            Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<BannerModel>>() {
                            }.getType();

                            if (!bannerArrayList.isEmpty()) {
                                bannerArrayList.clear();
                            }
                            bannerArrayList = gson.fromJson(jsonObject.getString("data"), type);
                            Log.e("ArrayList", "" + bannerArrayList.size());

                            try {
                                setBannerimageView();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                        }
                    } else {

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    public void setBannerimageView() {
        imgv = new ImageView[bannerArrayList.size()];
        for (int j = 0; j < bannerArrayList.size(); j++) {
            imgv[j] = new ImageView(this);

            ImageView img = new ImageView(LocalEventActivity.this);
            LinearLayout.LayoutParams para = new LinearLayout.LayoutParams(600, 200);
            para.leftMargin = 10;
            para.topMargin = 5;
            imgv[j].setLayoutParams(para);

            imgv[j].setScaleType(ImageView.ScaleType.FIT_XY);
            //  imgv[j].setOnClickListener(this);
            //   imgv[j].setBackgroundResource(R.drawable.banner);

            String url = "https://www.localneighborhoodapp.com/images/banner/";

            String urlmain = url + bannerArrayList.get(j).getImage();
            Log.e(TAG, "AnimateandSlideShow: " + urlmain);
            Picasso.with(mContext).load(urlmain).into(imgv[j]);
            Log.e(TAG, "AnimateandSlideShow: " + j);
            binding.horizontalOuterLayout.addView(imgv[j], para);

          *//*  images.add(Imgid[j].toString());

            System.out.println("string arraylist@@@@@@@" + images.get(j));*//*
        }
        MCObject = new MyCounter(bannerArrayList.size(), 1);
        MCObject.start();
    }


    public class MyCounter extends CountDownTimer {

        public MyCounter(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            if (leftToRight == true) {

                MCObject.cancel();
                //if (gallery.getScrollX() > (((imgv.length - 1) * multiplyValue)+addValue)) {

                if (binding.horizontalScrollview.getScrollX() > ((imgv.length - 1) * 800) + bannerArrayList.size()) {
                    count = ((imgv.length - 1) * 800);
                    leftToRight = false;
                    getCount();
                    MCObject.start();

                }
                if (count != binding.horizontalScrollview.getScrollX()) {
                    binding.horizontalScrollview.scrollBy(1, 0);

                    count++;
                    MCObject.start();
                } else {
                    count = ((imgv.length - 1) * 800);
                    leftToRight = false;
                    getCount();
                    MCObject.start();
                }
            } else {
                MCObject.cancel();
                if (binding.horizontalScrollview.getScrollX() <= 0) {
                    count = -2;
                    leftToRight = true;
                    MCObject = new MyCounter(bannerArrayList.size(), 1);
                    MCObject.start();
                }
                if (count != 0) {
                    binding.horizontalScrollview.scrollBy(-1, 0);
                    count--;
                    MCObject.start();
                } else {
                    count = -2;
                    leftToRight = true;
                    MCObject = new MyCounter(bannerArrayList.size(), 1);
                    MCObject.start();
                }
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }
    }


    private void getCount() {
        Log.e(TAG, "getCount: >>>>>>>>>>>>>>");
    }*/

}

