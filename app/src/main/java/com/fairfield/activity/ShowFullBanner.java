package com.fairfield.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.fairfield.R;
import com.fairfield.adapter.BannerPagerAdapter;
import com.fairfield.adapter.CouponPagerAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityShowFullBannerBinding;
import com.fairfield.databinding.ActivityShowFullCouponBinding;

public class ShowFullBanner extends AppCompatActivity {

    private ActivityShowFullBannerBinding binding;

    private BannerPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_show_full_banner);
        setSupportActionBar(binding.myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(" ");
        }
        binding.myToolbar.getBackground().setAlpha(0);
        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_white));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        adapter = new BannerPagerAdapter(getSupportFragmentManager(), App.getInstance().banners);
        binding.pagerBanner.setAdapter(adapter);
        binding.pagerBanner.setCurrentItem(App.getInstance().bannerIndex);
    }
}
