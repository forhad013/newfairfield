package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;

import com.fairfield.R;
import com.fairfield.adapter.ProvidersAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityBusinessProvidersBinding;
import com.fairfield.model.ProvidersModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessProviders extends AppCompatActivity {


    private ActivityBusinessProvidersBinding  binding;
    private Context mContext;
    private PreferenceHelper preferenceHelper;
    private HashMap<String , String> hashMap;
    private static final String TAG = "BusinessProviders";
    private ArrayList<ProvidersModel> providersModelArrayList;
    private Gson gson;
    private ProvidersAdapter adapter;
    private App app;
    String Subcategoryid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_business_providers);
        setSupportActionBar(binding.myToolbar);
        mContext = this;
        gson = new Gson();
        app = App.getInstance();
        preferenceHelper = new PreferenceHelper(mContext);

        Subcategoryid= getIntent().getExtras().getString("Subcategoryid");


        providersModelArrayList = new ArrayList<>();
        if(!providersModelArrayList.isEmpty()){
            providersModelArrayList.clear();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        binding.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));
        binding.recyclerView.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerView.setNestedScrollingEnabled(false);
        binding.recyclerView.setHasFixedSize(false);

        adapter = new ProvidersAdapter(mContext, providersModelArrayList);
        binding.recyclerView.setAdapter(adapter);
        getProvidersData();
        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ((ProvidersAdapter)  binding.recyclerView.getAdapter()).setOnItemClickListener(new ProvidersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                app.providersModel = adapter.getProvidersModel().get(position);



                Intent intent = new Intent(mContext, ProviderDetails.class);
                startActivity(intent);
            }
        });
    }


    public void getProvidersData(){
        startAnim();

        HashMap<String , String> hashMap = new HashMap<>();
        hashMap.put(Constant.SUBCATEGORY ,Subcategoryid);
        binding.textviewNo.setVisibility(View.GONE);

        Constant.retrofitService.getProvidersData(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);
                        stopAnim();
                        if (jsonObject.getBoolean("status")) {


                            Type type = new TypeToken<List<ProvidersModel>>() {
                            }.getType();

                            if (!providersModelArrayList.isEmpty()) {
                                providersModelArrayList.clear();
                            }
                            providersModelArrayList = gson.fromJson(jsonObject.getString("data"), type);
                            adapter.setProvidersModelList(providersModelArrayList);
                            adapter.notifyDataSetChanged();
                        } else {
                            stopAnim();
                            binding.textviewNo.setVisibility(View.VISIBLE);
                        }
                    }else {
                        stopAnim();
                        binding.textviewNo.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }
}
