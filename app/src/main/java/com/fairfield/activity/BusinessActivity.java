package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ExpandableListView;

import com.fairfield.R;
import com.fairfield.adapter.ExpandableListAdapter;
import com.fairfield.databinding.ActivityBusinessBinding;
import com.fairfield.model.BusinessCategoryModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rohit rathore rohitrathore8407@gmail.com on 10/12/2017.
 */

public class BusinessActivity extends AppCompatActivity {

    ActivityBusinessBinding binding;
    Context mContext;
    ArrayList<BusinessCategoryModel> businessArrayList;
    Gson gson;
    private PreferenceHelper preferenceHelper;
    private static final String TAG = "BusinessActivity";
    ExpandableListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        gson = new Gson();
        preferenceHelper = new PreferenceHelper(mContext);
        businessArrayList = new ArrayList<>();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_business);
        setSupportActionBar(binding.myToolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        getBusinessproviderlist();
        listAdapter = new ExpandableListAdapter(this, businessArrayList);

        // setting list adapter
        binding.lvExp.setAdapter(listAdapter);

        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        binding.serchView.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                query = query.toString().toLowerCase();

                final List<BusinessCategoryModel> filteredList = new ArrayList<>();

                for (int i = 0; i < businessArrayList.size(); i++) {

                    final String text = businessArrayList.get(i).getName().toLowerCase();
                    if (text.contains(query)) {
                        filteredList.add(businessArrayList.get(i));
                    }
                }

                listAdapter.setlistDataHeaderList(filteredList);
                listAdapter.notifyDataSetChanged();

            }
        });


        /// Listview on child click listener
        binding.lvExp.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                Intent intent = new Intent(mContext, BusinessProviders.class);
                intent.putExtra("Subcategoryid", businessArrayList.get(groupPosition).getSubcategories().get(childPosition).getId());
                startActivity(intent);

                return false;
            }
        });


    }


    private void getBusinessproviderlist() {

        startAnim();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        binding.textviewNoProv.setVisibility(View.GONE);


        Constant.retrofitService.getBusinessdata().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        stopAnim();
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("status")) {
                            Type type = new TypeToken<List<BusinessCategoryModel>>() {
                            }.getType();

                            if (!businessArrayList.isEmpty()) {
                                businessArrayList.clear();
                            }

                            businessArrayList = gson.fromJson(jsonObject.getString("data"), type);


                            listAdapter.setlistDataHeaderList(businessArrayList);
                            listAdapter.notifyDataSetChanged();
                            // addview();
                        } else {
                            stopAnim();
                            binding.textviewNoProv.setVisibility(View.VISIBLE);
                        }


                    } else {
                        binding.textviewNoProv.setVisibility(View.VISIBLE);
                        stopAnim();

                        //   Utility.showAlert(mContext, jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {



            }
        });
    }

    public void addTextListener() {

        binding.serchView.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                query = query.toString().toLowerCase();

                final List<BusinessCategoryModel> filteredList = new ArrayList<>();

                for (int i = 0; i < businessArrayList.size(); i++) {

                    final String text = businessArrayList.get(i).getName().toLowerCase();
                    if (text.contains(query)) {

                        filteredList.add(businessArrayList.get(i));
                    }
                }

                listAdapter.setlistDataHeaderList(filteredList);
                listAdapter.notifyDataSetChanged();

                //  mRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                //    mAdapter = new SimpleAdapter(filteredList, MainActivity.this);
                //   mRecyclerView.setAdapter(mAdapter);
                //  mAdapter.notifyDataSetChanged();  // data set changed
            }
        });

    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }
}
