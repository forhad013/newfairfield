package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityPramotionBinding;
import com.fairfield.model.Coupondatum;
import com.fairfield.model.Profile;
import com.fairfield.model.ProfileSave;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.view.MaterialIntroView;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PramotionActivity extends AppCompatActivity {

    private Context mContext;
    Gson gson;
    private static final String TAG = "PramotionActivity";

    Realm myRealm;
    private ArrayList<Coupondatum> CoupansArrayList;
    public static MyAppAdapter myAppAdapter;
    public static ViewHolder viewHolder;
    ArrayList<Coupondatum> likeid;
    ArrayList<Coupondatum> couponViewlist;
    private String[] toppings;
    private PreferenceHelper preferenceHelper;
    private JSONArray jsonArraylike;
    private JSONArray jsonArrayViewCoupon;

    // private AVLoadingIndicatorView avLoadingIndicatorView;

    // private SwipeFlingAdapterView flingContainer;

    private ActivityPramotionBinding binding;

    int token = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_pramotion);

        setSupportActionBar(binding.myToolbar);

        try {
            token = getIntent().getIntExtra("token", 0);
        }catch (Exception e){

        }

        // fragmentManager = getFragmentManager();
        // fragmentTransaction = fragmentManagerger.SupportFragmentManager().beginTransaction();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }


        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_white));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(token==1){
                    Intent intent = new Intent(mContext, DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    finish();
                }
            }
        });

        mContext = getApplicationContext();
        gson = new Gson();
        myRealm = App.getRealmDatabaseInstance();
        likeid = new ArrayList<>();
        couponViewlist = new ArrayList<>();

        jsonArraylike = new JSONArray();
        jsonArrayViewCoupon = new JSONArray();


        if (!likeid.isEmpty()) {
            likeid.clear();
        }

        if (!couponViewlist.isEmpty()) {
            couponViewlist.clear();
        }


        preferenceHelper = new PreferenceHelper(mContext);
        CoupansArrayList = new ArrayList<>();

        if (!CoupansArrayList.isEmpty()) {
            CoupansArrayList.clear();
        }


        //   flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);
        //   avLoadingIndicatorView = (com.wang.avi.AVLoadingIndicatorView) findViewById(R.id.avi);

        getdata();


        new MaterialIntroView.Builder(this)
                .enableDotAnimation(true)
                .enableIcon(true)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(5000)
                .enableFadeAnimation(true)
                .performClick(true)
                .setInfoText("Swipe right for saved the coupon\n" +
                        "& Swipe Left to ignore it")
                .setTarget(binding.frame)
                .setUsageId("intro_cardhj") //THIS SHOULD BE UNIQUE ID
                .show();



        binding.frame.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {

                Log.e(TAG, "removeFirstObjectInAdapter: " + myAppAdapter.getCount());
                if (myAppAdapter.getCount() == 1) {

                    binding.textviewNotcoupon.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onLeftCardExit(Object dataObject) {

                Log.e(TAG, "onLeftCardExit: " + CoupansArrayList.get(0).toString());
                try {

                    couponViewlist.add(CoupansArrayList.get(0));
                    CoupansArrayList.remove(0);


                    myAppAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
            }

            @Override
            public void onRightCardExit(Object dataObject) {

                try {
                    couponViewlist.add(CoupansArrayList.get(0));
                    likeid.add(CoupansArrayList.get(0));
                    Log.e(TAG, "onRightCardExit: " + CoupansArrayList.get(0).toString());

                    if (myAppAdapter.getCount() < 1) {

                        binding.textviewNotcoupon.setVisibility(View.VISIBLE);
                    }

                    CoupansArrayList.remove(0);
                    myAppAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {

            }

            @Override
            public void onScroll(float scrollProgressPercent) {

                View view = binding.frame.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });


        // Optionally add an OnItemClickListener
        binding.frame.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {

            /*    View view = binding.frame.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);

                Log.e(TAG, "onItemClicked: " + CoupansArrayList.get(itemPosition).getCouponName());
                myAppAdapter.notifyDataSetChanged();


                Intent intent = new Intent(mContext, BusinessActivity.class);
                startActivity(intent);*/
            }
        });

        binding.btnQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.showAlert(PramotionActivity.this, "", getString(R.string.promotion_help));
            }
        });

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendAlllikeid();

                Intent intent = new Intent(mContext, SavedCouponsActivity.class);
                startActivity(intent);
            }
        });
    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

   /* private void checkCouponData() {
        RealmResults<Profile> profiles = myRealm.where(Profile.class).findAll();
        if (profiles.isEmpty()) {
            getdata();
        } else {
            getLocaldata();
        }
    }

    private void getLocaldata() {
        if (!profileArrayList.isEmpty()) {
            {
                profileArrayList.clear();
            }
        }
        RealmResults<Profile> profiles = myRealm.where(Profile.class).findAll();
        Log.e(TAG, "RealmResults: " + profiles.toString());

        profileArrayList = new ArrayList(myRealm.where(Profile.class).findAll());
        Log.e(TAG, "getLocaldata: " + profileArrayList);
        myAppAdapter = new MyAppAdapter(profileArrayList, PramotionActivity.this);
        binding.frame.setAdapter(myAppAdapter);
        myAppAdapter.notifyDataSetChanged();
    }*/


    private void getdata() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());

        startAnim();
        Constant.retrofitService.allCoupan(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.getBoolean("status")) {
                        Type type = new TypeToken<List<Coupondatum>>() {
                        }.getType();

                        CoupansArrayList = gson.fromJson(jsonObject.getString("data"), type);
                        Log.d(TAG, " CoupansArrayList = [" + CoupansArrayList.size() + "]");
                        stopAnim();
                        setadpater();
                    } else {
                        stopAnim();
                        binding.textviewNotcoupon.setVisibility(View.VISIBLE);
                    }
                    // addview();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");
            }
        });
    }


    private void deletCoupan(String id) {
        final RealmResults<Profile> rows = myRealm.where(Profile.class).equalTo("name", id).findAll();

        myRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                rows.deleteAllFromRealm();
            }
        });
    }

    private void saveCoupon(Profile profile) {

        ProfileSave profileSave = new ProfileSave();
        profileSave.setAge(profile.getAge());
        profileSave.setImageUrl(profile.getImageUrl());
        profileSave.setLocation(profile.getLocation());
        profileSave.setName(profile.getName());

        myRealm.beginTransaction();
        myRealm.copyToRealmOrUpdate(profileSave);

        Log.e(TAG, "saveCoupon: " + profile);
        myRealm.commitTransaction();
    }


    private void setadpater() {
        Log.e(TAG, "setadpater: ");

        myAppAdapter = new MyAppAdapter(CoupansArrayList, PramotionActivity.this);
        binding.frame.setAdapter(myAppAdapter);
        myAppAdapter.notifyDataSetChanged();
    }


    public static class ViewHolder {
        public static FrameLayout background;
        public TextView DataText, Des;
        public ImageView cardImage;

    }

    public class MyAppAdapter extends BaseAdapter {


        public ArrayList<Coupondatum> coupansList;
        public Context context;

        private MyAppAdapter(ArrayList<Coupondatum> apps, Context context) {
            this.coupansList = apps;
            this.context = context;


            Log.e(TAG, "MyAppAdapter: " + coupansList.size());
        }

        @Override
        public int getCount() {


            return coupansList.size();

        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;

            Log.e(TAG, "getCount: " + getCount());

            if (rowView == null) {

                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.item, parent, false);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.DataText = (TextView) rowView.findViewById(R.id.bookText);
                viewHolder.background = (FrameLayout) rowView.findViewById(R.id.background);
                viewHolder.cardImage = (ImageView) rowView.findViewById(R.id.cardImage);
                viewHolder.Des = (TextView) rowView.findViewById(R.id.des);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            viewHolder.DataText.setText(coupansList.get(position).getCouponName() + "");
            viewHolder.Des.setText(coupansList.get(position).getDescription());

            Log.e(TAG, "getView: " + getResources().getString(R.string.imageurl_coupon) + coupansList.get(position).getImage());
            Picasso.with(mContext)
                    .load(getResources().getString(R.string.imageurl_coupon) + coupansList.get(position).getImage().trim()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder)
                    .into(viewHolder.cardImage);

            //     Glide.with(PramotionActivity.this).load().into(viewHolder.cardImage);

            return rowView;
        }

    }

    public void sendAlllikeid() {

        try {

            for (Coupondatum showCoupansModel : couponViewlist) {

                Log.e(TAG, "sendAlllikeid: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(showCoupansModel.getOwnerId(), showCoupansModel.getId());

                    jsonArrayViewCoupon.put(jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Log.e(TAG, "jsonArrayViewCoupon: " + jsonArrayViewCoupon);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            for (Coupondatum showCoupansModel : likeid) {

                Log.e(TAG, "sendAlllikeid: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(showCoupansModel.getOwnerId(), showCoupansModel.getId());

                    jsonArraylike.put(jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            Log.e(TAG, "sendAlllikeid: " + jsonArraylike);
        } catch (Exception e) {
            e.printStackTrace();
        }


     /*   try {
            gson = new Gson();

            String listString = gson.toJson(likeid,
                    new TypeToken<ArrayList<String>>() {
                    }.getType());

            jsonArraydata = new JSONArray(listString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
*/


        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());

        Log.e(TAG, "coupon_id: " + jsonArraylike.toString());
        Log.e(TAG, "view_coupon: " + jsonArrayViewCoupon.toString());
        hashMap.put("coupon_id", jsonArraylike.toString());
        hashMap.put("view_coupon", jsonArrayViewCoupon.toString());


        Constant.retrofitService.likeCouponByuser(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    if (!couponViewlist.isEmpty()) {
                        couponViewlist.clear();

                    }
                    if (!likeid.isEmpty()) {
                        likeid.clear();
                    }

                    jsonArraylike = new JSONArray("[]");
                    jsonArrayViewCoupon = new JSONArray("[]");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + response.body().string() + "]");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");

            }
        });
    }

    @Override
    protected void onResume() {

        super.onResume();

        Log.e(TAG, "onResume: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        sendAlllikeid();
        Log.e(TAG, "onStop: ");
    }

    public static JSONArray sortJsonArray(JSONArray array) {

        List<JSONObject> jsons = new ArrayList<JSONObject>();
        for (int i = 0; i < array.length(); i++) {
            try {
                jsons.add(array.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Collections.sort(jsons, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject lhs, JSONObject rhs) {
                String lid = null;
                String rid = null;
                try {
                    lid = lhs.getString("ownerId");
                    rid = rhs.getString("comment_id");
                    // Here you could parse string id to integer and then compare.
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return lid.compareTo(rid);
            }
        });


        return new JSONArray(jsons);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.e(TAG, "onRestart: ");
    }

    @Override
    protected void onPause() {

        super.onPause();
        Log.e(TAG, "onPause: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: ");
    }
}
