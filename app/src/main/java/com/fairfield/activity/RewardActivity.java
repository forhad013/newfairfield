package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.fairfield.R;
import com.fairfield.adapter.RewardAdapter;
import com.fairfield.databinding.ActivityRewardBinding;
import com.fairfield.model.BannerModel;
import com.fairfield.model.RewardDataModel;
import com.fairfield.model.RewardModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;

import bolts.Bolts;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class RewardActivity extends AppCompatActivity implements RewardAdapter.RecyclerViewClickListener {

    private ActivityRewardBinding binding;
    private Gson gson;
    private static final String TAG = "RewardActivity";
    private RewardAdapter adapter;

    public int currentimageindex = 0;
    public Context mContext;
    ArrayList<RewardModel> rewardModels;

    int token = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this , R.layout.activity_reward);
        try {
            token = getIntent().getIntExtra("token", 0);
        }catch (Exception e){

        }
        setSupportActionBar(binding.myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }


        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(token==1){
                            Intent intent = new Intent(mContext, DashBoardActivity.class);
                            startActivity(intent);
                            finish();
                        }else {
                            finish();
                        }
                    }
                });

        mContext = this;

        getRewardData();
    }

    private  void getRewardData(){
        startAnim();

        Constant.retrofitService.getrewarddata().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) try {
                    String responseString = response.body().string();
                    Log.e(TAG, responseString);

                    JSONArray jsonArray = new JSONArray(responseString);
                    JSONObject jsonObject = null;
                    rewardModels = new ArrayList<RewardModel>();
                    RewardModel rewardModel;
                    Boolean showTitle = false;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        int mId = Integer.parseInt(jsonObject.getString("id"));
                        String mTitle = jsonObject.getString("title");
                        showTitle = false;
                        if (!jsonObject.isNull("show_title")) {
                            showTitle = (jsonObject.getInt("show_title")==1)? true : false;
                        }

                        String mImage = jsonObject.getString("image");
                        rewardModel = new RewardModel(mId, mTitle, showTitle, mImage, "", "");
                        rewardModels.add(rewardModel);
                    }

                    adapter = new RewardAdapter(mContext, (List<RewardModel>)rewardModels, (RewardAdapter.RecyclerViewClickListener)(RewardActivity.this));
                    binding.rewardTable.setAdapter(adapter);
                    binding.rewardTable.setLayoutManager(new LinearLayoutManager(mContext));
                    binding.rewardTable.setItemAnimator(new DefaultItemAnimator());
                    binding.rewardTable.addItemDecoration(new DividerItemDecoration(mContext, 0));
                    stopAnim();

//                        Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + responseString + "]");
//                        JSONObject jsonObject =new JSONObject(responseString);
//                        if (jsonObject.getBoolean("status")){
//                            rewardDataModel  = gson.fromJson(jsonObject.getString("data") , RewardDataModel.class);
//                            binding.setReward(rewardDataModel);
//
//
//                            Log.e(TAG, "getSmallContant: "+jsonObject.getJSONObject("data").get("content"));
//                            Log.e(TAG, "getSmallContant: "+jsonObject.getJSONObject("data").get("small_content"));
//
//                            Log.e(TAG, "getSmallContant: "+rewardDataModel.getSmallContant());
//                            Log.e(TAG, "getContant: "+rewardDataModel.getContant());
//                            binding.textviewSmallcontanct.setText(jsonObject.getJSONObject("data").get("small_content").toString());
//                            binding.textviewBigContant.setText(jsonObject.getJSONObject("data").get("content").toString());
//                            binding.imageviewReward.setVisibility(View.VISIBLE);
//
//                            stopAnim();
//                        }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.INVISIBLE);
        // or avi.smoothToHide();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {

        Intent mIntent = new Intent(RewardActivity.this, RewardCommentActivity.class);
        mIntent.putExtra("Reward_id", rewardModels.get(position).mId);
        RewardActivity.this.startActivity(mIntent);
    }
}
