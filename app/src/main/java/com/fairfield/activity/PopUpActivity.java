package com.fairfield.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fairfield.R;
import com.fairfield.utility.Constant;
import com.squareup.picasso.Picasso;

public class PopUpActivity extends AppCompatActivity {

    String titleText;
    String bodyText;
    String image;
    String notificationId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up);


        titleText = getIntent().getStringExtra("titleText");
        bodyText = getIntent().getStringExtra("bodyText");
        image = getIntent().getStringExtra("image");
        notificationId = getIntent().getStringExtra("notificationId");

        String orginalLink = Constant.IMAGE_URL+"notifications/"+image;
        showDialoge(titleText,bodyText,orginalLink);

    }

    public void showDialoge( String titleText, String messageText, String image) {
        final Dialog dialog = new Dialog(PopUpActivity.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.announce_popup_layout);
        // Set dialog title
        //dialog.setTitle("Category Add");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();


        Button done = (Button) dialog.findViewById(R.id.okBtn);

        TextView title = (TextView) dialog.findViewById(R.id.title);
        ImageView icon = (ImageView) dialog.findViewById(R.id.icon);

        TextView message = (TextView) dialog.findViewById(R.id.message);

        Log.e("image",image);

        if (image.equals("")) {
            icon.setImageResource(R.drawable.fairfield_icon);
        } else {
            Picasso.with(getApplicationContext()).load(image).into(icon);
        }

        message.setText(messageText);
        title.setText(titleText);

        //   Log.e("text",text);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent  intent = new Intent(PopUpActivity.this, DashBoardActivity.class);
                startActivity(intent);
                finish();


            }
        });

    }

    }