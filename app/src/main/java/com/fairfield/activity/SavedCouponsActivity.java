package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.fairfield.R;
import com.fairfield.adapter.SaveCouponAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivitySavedCouponsBinding;
import com.fairfield.model.BannerModel;
import com.fairfield.model.Coupondatum;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.NumberPicker.OnScrollListener.SCROLL_STATE_IDLE;
import static android.widget.NumberPicker.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL;

/**
 * Created by Argalon on 6/19/2017.
 */
// implements  RecyclerView.OnItemTouchListener
public class SavedCouponsActivity extends AppCompatActivity {

    ActivitySavedCouponsBinding binding;
    Realm myRealm;
    Context mContext;
    private static final String TAG = "SavedCouponsActivity";
    SaveCouponAdapter adapter;
    private GestureDetectorCompat gestureDetector;
    ArrayList<Coupondatum> saveCoupnaArrayList;
    Gson gson;
    PreferenceHelper preferenceHelper;
    JSONObject jsonObject;
    App app;



    private Handler mHandler;
    private Runnable mUpdateResults;
    public int currentimageindex = 0;
    private Timer timer;
    private ArrayList<BannerModel> bannerArrayList;
    private int[] IMAGE_IDS = {R.drawable.banner, R.drawable.btn_fb, R.drawable.like,
            R.drawable.new_logo};




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myRealm = App.getRealmDatabaseInstance();
        mContext = this;
        gson = new Gson();
        preferenceHelper = new PreferenceHelper(mContext);
        app = App.getInstance();


        bannerArrayList = new ArrayList<>();
        timer = new Timer();
        mHandler = new Handler();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_saved_coupons);
        setSupportActionBar(binding.myToolbar);


        saveCoupnaArrayList = new ArrayList<>();

        if (!saveCoupnaArrayList.isEmpty()) {
            saveCoupnaArrayList.clear();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }
        //   binding.recyclerViewSavecoupans.addOnItemTouchListener(this);
        //  gestureDetector = new GestureDetectorCompat(this, new RecyclerViewBenOnGestureListener());

        binding.recyclerViewSavecoupans.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                final Picasso picasso = Picasso.with(mContext);

                if (scrollState == SCROLL_STATE_IDLE || scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    picasso.resumeTag(mContext);
                } else {
                    picasso.pauseTag(mContext);
                }
            }
        });

        binding.recyclerViewSavecoupans.setLayoutManager(new GridLayoutManager(mContext, 2));
        binding.recyclerViewSavecoupans.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerViewSavecoupans.setNestedScrollingEnabled(false);
        binding.recyclerViewSavecoupans.setHasFixedSize(false);

        adapter = new SaveCouponAdapter(mContext, saveCoupnaArrayList);
        binding.recyclerViewSavecoupans.setAdapter(adapter);

        getSaveCouponData();
        getAllCoupons();

        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, DashBoardActivity.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            }
        });


        ((SaveCouponAdapter) binding.recyclerViewSavecoupans.getAdapter()).setOnItemClickListener(new SaveCouponAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
             //   Log.e(TAG, "onItemClick() called with: view = [" + ((SaveCouponAdapter) binding.recyclerViewSavecoupans.getAdapter()) + "], position = [" + position + "]");
//                app.showCoupansModel = adapter.getProfileSave().get(position);
                app.coupons = adapter.getProfileSave();
                app.couponIndex = position;
                app.showBusinessInfo = true;

                Intent intent = new Intent(mContext, ShowFullCoupon.class);
                startActivity(intent);

            }
        });




        binding.slidingimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BannerModel banner = bannerArrayList.get(currentimageindex - 1);
                bannerClick(banner.getId(), banner.getOwnerId());



              /*  AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);
                dialog.setView(dialogLayout);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.show();


                ImageView image = (ImageView) dialog.findViewById(R.id.goProDialogImage);
                //  Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),
                //      R.drawable.whygoprodialogimage);

                try {
                    String url = "https://www.localneighborhoodapp.com/images/banner/";
                    String urlmain = url + bannerArrayList.get(currentimageindex - 1).getImage();
                    Log.e(TAG, "AnimateandSlideShow: " + urlmain);
                    Picasso.with(mContext).load(urlmain).into(image);

                } catch (Exception e) {
                    System.out.println(e);
                }
*/

            }
        });
    }


    private void getSaveCouponData() {
        startAnim();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());

        binding.textviewNoSavedCoupon.setVisibility(View.GONE);
        Constant.retrofitService.show_save_coupons(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {

                        String res = response.body().string();
                        jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("status")) {

                            stopAnim();
                            Type type = new TypeToken<List<Coupondatum>>() {
                            }.getType();


                            if (!saveCoupnaArrayList.isEmpty()) {
                                saveCoupnaArrayList.clear();
                            }

                            saveCoupnaArrayList = gson.fromJson(jsonObject.getString("data"), type);
                          //  Log.e(TAG, " CoupansArrayList = [" + saveCoupnaArrayList.size() + "]");

                            adapter.setProfileSaveList(saveCoupnaArrayList);
                            adapter.notifyDataSetChanged();


                            // addview();
                        } else

                        {
                            binding.textviewNoSavedCoupon.setVisibility(View.VISIBLE);
                            stopAnim();
                        }


                    } else {
                        //     binding.textviewNoSavedCoupon.setVisibility(View.VISIBLE);
                        //    stopAnim();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

              //  Log.e(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");

            }
        });
    }


    /* private void getSaveCouponData() {
         RealmResults<ProfileSave> profiles = myRealm.where(ProfileSave.class).findAll();

         if (profiles.isEmpty()) {
         } else {
             saveArrayList = new ArrayList(myRealm.where(ProfileSave.class).findAll());
             adapter.setProfileSaveList(saveArrayList);
             adapter.notifyDataSetChanged();
         }


         Log.e(TAG, "checkCouponData: " + profiles.toString());
     }
 */
   /* @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

*/
    private void ToggleSelection(int position) {

        //   String title = String.format("%s of %s selected", adapter.getSelectedItemCount(), adapter.getContactlist().size());
        //  getSupportActionBar().setSubtitle(title);
        adapter.toggleSelection(position);

    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }

   /* private class RecyclerViewBenOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            try {
                View view = binding.recyclerViewSavecoupans.findChildViewUnder(e.getX(), e.getY());
                int currentPosition = binding.recyclerViewSavecoupans.getChildAdapterPosition(view);
                ShowCoupansModel showCoupansModel = adapter.getItem(currentPosition);

                 *//*   if (actionMode != null) {
                        return;
                    }
                    actionMode = getActivity().startActionMode(ConversationsFragment.this);*//*
                    ToggleSelection(currentPosition);

                super.onLongPress(e);
            } catch (Exception e1) {
                Log.d(TAG, "onLongPress() called with: e = [" + e + "]");
            }
        }

    }*/



  /*  @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private class RecyclerViewBenOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            View view = binding.recyclerViewSavecoupans.findChildViewUnder(e.getX(), e.getY());
            onClick(view);
            return super.onSingleTapConfirmed(e);
        }

    }
*/
// Add/Remove the item from/to the list

    private void bannerClick(String bannerId, final String ownerId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", String.valueOf(bannerId));
        hashMap.put("owner_id", String.valueOf(ownerId));
        Constant.retrofitService.bannerClick(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        JSONObject jsonObject = new JSONObject(responseString);

                        if (jsonObject.getBoolean("status")) {
                            Intent intent = new Intent(mContext, ProviderDetails.class);
                            intent.putExtra("From", "Ad");
                            intent.putExtra("OWNERID", ownerId);
                            startActivity(intent);
                        } else {
                            // show message error
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utility.showAlert(mContext, "something went wrong");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.showAlert(mContext, "something went wrong");
            }
        });
    }

    private void getAllCoupons() {

        Constant.retrofitService.getAllbanners().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {
                      //      Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<BannerModel>>() {
                            }.getType();

                            if (!bannerArrayList.isEmpty()) {
                                bannerArrayList.clear();
                            }
                            bannerArrayList = gson.fromJson(jsonObject.getString("data"), type);
                  //          Log.e("ArrayList", "" + bannerArrayList.size());

                            try {
                                bannerStart();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    } else {


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void bannerStart() {
        try {
            // Create runnable for posting
            mUpdateResults = new Runnable() {
                public void run() {

                    if (bannerArrayList.size() == currentimageindex) {
                        currentimageindex = 0;
                        AnimateandSlideShow();
                    } else {
                        AnimateandSlideShow();
                    }


                }
            };

            int delay = 1000; // delay for 1 sec.
            int period = 2000; // repeat every 4 sec.

            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {
                    mHandler.post(mUpdateResults);
                }

            }, delay, period);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void AnimateandSlideShow() {



        try {
            String url = "https://www.localneighborhoodapp.com/images/banner/";

            String urlmain = url + bannerArrayList.get(currentimageindex).getImage();
          //  Log.e(TAG, "AnimateandSlideShow: " + urlmain);
            Picasso.with(mContext).load(urlmain).into(binding.slidingimage);
         //   Log.e(TAG, "AnimateandSlideShow: " + currentimageindex);

       //   saveBannerview(bannerArrayList.get(currentimageindex).getId(), bannerArrayList.get(currentimageindex).getOwnerId());

            currentimageindex++;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onStop() {
        super.onStop();
      //  Log.e(TAG, "onStop: ");


        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
     //   Log.e(TAG, "onRestart: ");
        try {
            mHandler = new Handler();
            timer = new Timer();
            if (!bannerArrayList.isEmpty()){
                bannerStart();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void saveBannerview(String banner_id, String owner_id) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", banner_id);
        hashMap.put("owner_id", owner_id);

       // Log.e(TAG, "saveBannerview: " + hashMap);

        Constant.retrofitService.saveBannerview(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (adapter.getSelectedItemCount() != 0) {
            adapter.clearSelections();
        }

        Intent intent = new Intent(mContext, DashBoardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

}

