package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityFacebookloginBinding;
import com.fairfield.model.UserModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FacebookloginActivity extends AppCompatActivity {

    private ActivityFacebookloginBinding binding;
    private Context mContext;
    App app;
    Gson gson;
    UserModel userModel;

    PreferenceHelper helper;

    private static final String TAG = "FacebookloginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        app = App.getInstance();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_facebooklogin);
        helper = new PreferenceHelper(mContext);
        gson = new Gson();
        userModel = new UserModel();
        binding.btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });


    }

    private void submitForm() {

        if (!validateEmail()) {
            return;
        }

        app.userModel.setEmail(binding.inputEmail.getText().toString().trim());
        // app.userModel.setContact(binding.inputPhonenumber.getText().toString().trim());
        emailLogin();
        // Toast.makeText(getActivity(), "Thank You!", Toast.LENGTH_SHORT).show();
    }

    private void emailLogin() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("email_id", binding.inputEmail.getText().toString().trim());

        // hashMap.put("contact" , binding.inputPhonenumber.getText().toString().trim());
        // hashMap.put("social_id" , binding.inputPhonenumber.getText().toString().trim());
        Log.e(TAG, "social_id: " + helper.getSocialId());

        hashMap.put("social_id", helper.getSocialId());

        //Log.e(TAG, "userModel"+app.userModel);
        Utility.showProgressHUD(mContext);

        Constant.retrofitService.updateUserEmail(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utility.hideProgressHud();
                if (response.isSuccessful()) {
                    try {
                        String responseString = response.body().string();

                        Log.e(TAG, "onResponse() called with: call = [" + call + "], response = [" + responseString + "]");

                        JSONObject jsonObject = new JSONObject(responseString);
                        if (jsonObject.getBoolean("status")) {

/*
                            userModel = gson.fromJson(jsonObject.getString("data"), UserModel.class);
                            helper.putUserId(userModel.getUserId());
                            helper.putEmail(userModel.getEmail());
                            helper.putStatus(userModel.getStatus());
                            app.userModel = userModel;

                            Log.e(TAG, "userModel: " + app.userModel);


                            Utility.showLongToast(jsonObject.getString("message"), mContext);

                            Intent intent = new Intent(mContext, DashBoardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();*/




                            Intent intent = new Intent(mContext, VarificationActivity.class);
                            startActivity(intent);

                        } else {
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlert(mContext, "Please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private boolean validateContact() {
        if (binding.inputPhonenumber.getText().toString().trim().isEmpty()) {
            binding.inputPhonenumber.setError(getString(R.string.err_msg_contact));
            requestFocus(binding.inputPhonenumber);
            return false;

        } else {
            binding.inputLayoutPhonenumber.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = binding.inputEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            binding.inputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(binding.inputEmail);
            return false;
        } else {
            binding.inputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
