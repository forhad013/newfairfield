package com.fairfield.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fairfield.R;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivitySettingPageBinding;
import com.fairfield.model.BannerModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingPageActivity extends AppCompatActivity {
    ActivitySettingPageBinding binding;
    Context mContext;
    private static final String TAG = "SettingPageActivity";
    PreferenceHelper preferenceHelper;
    private App app;

    private Gson gson;
    private Handler mHandler;
    private Runnable mUpdateResults;
    public int currentimageindex = 0;
    private Timer timer;
    private ArrayList<BannerModel> bannerArrayList;
    private int[] IMAGE_IDS = {R.drawable.banner, R.drawable.btn_fb, R.drawable.like,
            R.drawable.new_logo};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting_page);
        mContext = this;
        preferenceHelper = new PreferenceHelper(mContext);


        gson = new Gson();
        app = App.getInstance();

        bannerArrayList = new ArrayList<>();
        timer = new Timer();
        mHandler = new Handler();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }


        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Log.e(TAG, "getcoupon: " + preferenceHelper.getshareprefdataBoolean(PreferenceHelper.NEW_COUPON_NOTI));
        Log.e(TAG, "getreward: " + preferenceHelper.getshareprefdataBoolean(PreferenceHelper.NEW_REWARDS_NOTI));




        binding.newCouponsSW.setChecked(preferenceHelper.getshareprefdataBoolean(PreferenceHelper.NEW_COUPON_NOTI));
        binding.newRewardSW.setChecked(preferenceHelper.getshareprefdataBoolean(PreferenceHelper.NEW_REWARDS_NOTI));
        binding.newEventSW.setChecked(preferenceHelper.getshareprefdataBoolean(PreferenceHelper.NEW_EVENTS_NOTI));
        binding.newReviewsSW.setChecked(preferenceHelper.getshareprefdataBoolean(PreferenceHelper.NEW_REVIEWS_NOTI));

        binding.announcementSW.setChecked(preferenceHelper.getshareprefdataBoolean(PreferenceHelper.SPECIAL_NOTI));
        binding.requestedReviewsSW.setChecked(preferenceHelper.getshareprefdataBoolean(PreferenceHelper.REQUESTED_REVIEW_NOTI));
        binding.businessListSW.setChecked(preferenceHelper.getshareprefdataBoolean(PreferenceHelper.NEW_BUSINESS_NOTI));


        binding.rewardInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("a", "Notifications for new giveaways!");


                showDialoge("Info","Notifications for new giveaways!");


            }
        });

        binding.specialInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("a", "Notifications for new giveaways!");


                showDialoge("Info","Notifications for the latest news and special promotions ");


            }
        });

        binding.selectAllSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                Log.e(TAG, "onCheckedChanged: " + b);

                  selectAllCheckedFunc(b);
              }
        });


        binding.businessListSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked) {
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_BUSINESS_NOTI, true);

                    FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_BUSINESS_NOTI_SUBSCRIBE)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    Log.e("status", task.isSuccessful() + "");
//                                    if (task.isSuccessful()) {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_BUSINESS_NOTI, true);
//                                    } else {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_BUSINESS_NOTI, false);
//                                    }

                                }
                            });
                }else{
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Constant.NEW_BUSINESS_NOTI_SUBSCRIBE);
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_BUSINESS_NOTI, false);

                }

            }
        });

        binding.requestedReviewsSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked) {
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.REQUESTED_REVIEW_NOTI, true);

                    FirebaseMessaging.getInstance().subscribeToTopic(Constant.REQUESTED_REVIEW_NOTI_SUBSCRIBE)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    Log.e("status", task.isSuccessful() + "");
//                                    if (task.isSuccessful()) {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.REQUESTED_REVIEW_NOTI, true);
//                                    } else {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.REQUESTED_REVIEW_NOTI, false);
//                                    }

                                }
                            });
                }else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Constant.REQUESTED_REVIEW_NOTI_SUBSCRIBE);
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.REQUESTED_REVIEW_NOTI, false);

                }

            }
        });

        binding.announcementSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.SPECIAL_NOTI, true);

                    FirebaseMessaging.getInstance().subscribeToTopic(Constant.SPECIAL_NOTI_SUBSCRIBE)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    Log.e("status", task.isSuccessful() + "");
//                                    if (task.isSuccessful()) {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.SPECIAL_NOTI, true);
//
//                                    } else {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.SPECIAL_NOTI, false);
//                                    }

                                }
                            });
                }else{
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Constant.SPECIAL_NOTI_SUBSCRIBE);
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.SPECIAL_NOTI, false);

                }

            }
        });

        binding.newReviewsSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REVIEWS_NOTI, true);

                    FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_REVIEWS_NOTI_SUBSCRIBE)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    Log.e("status", task.isSuccessful() + "");
//                                    if (task.isSuccessful()) {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REVIEWS_NOTI, true);
//
//                                    } else {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REVIEWS_NOTI, false);
//                                    }

                                }
                            });
                }else {
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REVIEWS_NOTI, false);

                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Constant.NEW_REVIEWS_NOTI_SUBSCRIBE);


                }

            }
        });

        binding.newRewardSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked) {
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REWARDS_NOTI, true);

                    FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_REWARDS_SUBSCRIBE)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    Log.e("status", task.isSuccessful() + "");
//                                    if (task.isSuccessful()) {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REWARDS_NOTI, true);
//
//                                    } else {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REWARDS_NOTI, false);
//                                    }

                                }
                            });
                }else{
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Constant.NEW_REWARDS_SUBSCRIBE);
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REWARDS_NOTI, false);


                }

            }
        });
        binding.newEventSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_EVENTS_NOTI, true);

                    FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_EVENTS_NOTI_SUBSCRIBE)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

//                                    Log.e("status", task.isSuccessful() + "");
//                                    if (task.isSuccessful()) {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_EVENTS_NOTI, true);
//
//                                    } else {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_EVENTS_NOTI, false);
//                                    }

                                }
                            });

                }else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Constant.NEW_EVENTS_NOTI_SUBSCRIBE);
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_EVENTS_NOTI, false);


                }
            }
        });


        binding.newCouponsSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_COUPON_NOTI, true);

                    FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_COUPON_NOTI_SUBSCRIBE)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    Log.e("status", task.isSuccessful() + "");
//                                    if (task.isSuccessful()) {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_COUPON_NOTI, true);
//
//                                    } else {
//                                        preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_COUPON_NOTI, false);
//                                    }

                                }
                            });
                }else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Constant.NEW_COUPON_NOTI_SUBSCRIBE);
                    preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_COUPON_NOTI, false);


                }

            }
        });




        getAllCoupons();


        binding.slidingimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BannerModel banner = bannerArrayList.get(currentimageindex - 1);
                bannerClick(banner.getId(), banner.getOwnerId());


            }
        });
    }


    public void selectAllCheckedFunc(boolean status){

        binding.announcementSW.setChecked(status);
        binding.businessListSW.setChecked(status);
        binding.newCouponsSW.setChecked(status);
        binding.newReviewsSW.setChecked(status);
        binding.newEventSW.setChecked(status);
        binding.newRewardSW.setChecked(status);
        binding.requestedReviewsSW.setChecked(status);


    }

    private void bannerClick(String bannerId, final String ownerId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", String.valueOf(bannerId));
        hashMap.put("owner_id", String.valueOf(ownerId));
        Constant.retrofitService.bannerClick(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        JSONObject jsonObject = new JSONObject(responseString);

                        if (jsonObject.getBoolean("status")) {
                            Intent intent = new Intent(mContext, ProviderDetails.class);
                            intent.putExtra("From", "Ad");
                            intent.putExtra("OWNERID", ownerId);
                            startActivity(intent);
                        } else {
                            // show message error
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utility.showAlert(mContext, "something went wrong");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.showAlert(mContext, "something went wrong");
            }
        });
    }

    private void getAllCoupons() {

        Constant.retrofitService.getAllbanners().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {
                        //    Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<BannerModel>>() {
                            }.getType();

                            if (!bannerArrayList.isEmpty()) {
                                bannerArrayList.clear();
                            }
                            bannerArrayList = gson.fromJson(jsonObject.getString("data"), type);
                       //     Log.e("ArrayList", "" + bannerArrayList.size());

                            try {
                                bannerStart();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    } else {


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void bannerStart() {
        try {
            // Create runnable for posting
            mUpdateResults = new Runnable() {
                public void run() {

                    if (bannerArrayList.size() == currentimageindex) {
                        currentimageindex = 0;
                        AnimateandSlideShow();
                    } else {
                        AnimateandSlideShow();
                    }


                }
            };

            int delay = 1000; // delay for 1 sec.
            int period = 2000; // repeat every 4 sec.

            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {
                    mHandler.post(mUpdateResults);
                }

            }, delay, period);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void AnimateandSlideShow() {



        try {
            String url = "https://www.localneighborhoodapp.com/images/banner/";

            String urlmain = url + bannerArrayList.get(currentimageindex).getImage();
         //   Log.e(TAG, "AnimateandSlideShow: " + urlmain);
            Picasso.with(mContext).load(urlmain).into(binding.slidingimage);
           // Log.e(TAG, "AnimateandSlideShow: " + currentimageindex);

      //  saveBannerview(bannerArrayList.get(currentimageindex).getId(), bannerArrayList.get(currentimageindex).getOwnerId());

            currentimageindex++;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onStop() {
        super.onStop();
     //   Log.e(TAG, "onStop: ");


        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
     //   Log.e(TAG, "onRestart: ");
        try {
            mHandler = new Handler();
            timer = new Timer();
            if (!bannerArrayList.isEmpty()){
                bannerStart();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void showDialoge( String titleText, String messageText){
        final Dialog dialog = new Dialog(SettingPageActivity.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.info_popup_layout);
        // Set dialog title
        //dialog.setTitle("Category Add");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();



        Button done = (Button) dialog.findViewById(R.id.okBtn);

      //  TextView title = (TextView) dialog.findViewById(R.id.title);
        ImageView icon = (ImageView) dialog.findViewById(R.id.icon);

        TextView message = (TextView) dialog.findViewById(R.id.message);


            icon.setImageResource(R.mipmap.ic_launcher_new);



        message.setText(messageText);
      //  title.setText(titleText);

        //   Log.e("text",text);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });



    }
    private void saveBannerview(String banner_id, String owner_id) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", banner_id);
        hashMap.put("owner_id", owner_id);

        Log.e(TAG, "saveBannerview: " + hashMap);

        Constant.retrofitService.saveBannerview(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
