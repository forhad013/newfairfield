package com.fairfield.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fairfield.R;
import com.fairfield.databinding.ActivityNewsWebviewBinding;

public class NewsWebview extends AppCompatActivity {
    ActivityNewsWebviewBinding binding;
    private float m_downX;
    String url;
    private static final String TAG = "NewsWebview";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        binding = DataBindingUtil.setContentView(this, R.layout.activity_news_webview);
        setSupportActionBar(binding.myToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }



        url = getIntent().getStringExtra("url");
        String textviewtoolbar = getIntent().getStringExtra("texttoolbar");

      //  Log.e(TAG, "url: "+url );

        binding.toolbarTitle.setText(textviewtoolbar);

        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_white));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        binding.progressBar.setScaleY(2f);


        if (TextUtils.isEmpty(url)) {
            finish();
        }

        initWebView();
        binding.webView.loadUrl(url);
        // enable / disable javascript
      //  binding.webView.getSettings().setJavaScriptEnabled(true);
      //  binding. webView.getSettings().setSupportZoom(true);
      //  binding. webView.getSettings().setBuiltInZoomControls(true);
      //  binding. webView.getSettings().setDisplayZoomControls(true);
    }


    private void initWebView() {
        binding.webView.setWebChromeClient(new MyWebChromeClient(this));
        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                binding.progressBar.setVisibility(View.VISIBLE);
                invalidateOptionsMenu();
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                binding.webView.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
              binding.progressBar.setVisibility(View.GONE);
                invalidateOptionsMenu();
            }
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                binding.progressBar.setVisibility(View.GONE);
                invalidateOptionsMenu();
            }
        });
        binding.webView.clearCache(true);
        binding.webView.clearHistory();
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.setHorizontalScrollBarEnabled(true);

        binding.webView.getSettings().setLoadWithOverviewMode(false);
        binding.webView .getSettings().setSupportZoom(true);
        binding.  webView.getSettings().setBuiltInZoomControls(true);
        binding.  webView.getSettings().setDisplayZoomControls(true);
        binding.webView.getSettings().setUseWideViewPort(true);
     //   binding.webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        binding.webView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() > 1) {
                    //Multi touch detected
                    return true;
                }

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        // save the x
                        m_downX = event.getX();
                    }
                    break;

                    case MotionEvent.ACTION_MOVE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP: {
                        // set x so that it doesn't move
                        event.setLocation(m_downX, event.getY());
                    }
                    break;
                }

                return false;
            }
        });
    }


    private void back() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack();
        }
        finish();
    }

    private void forward() {
        if (binding.webView.canGoForward()) {
            binding. webView.goForward();
        }
    }

    private class MyWebChromeClient extends WebChromeClient {
        Context context;

        public MyWebChromeClient(Context context) {
            super();
            this.context = context;
        }


    }

}


