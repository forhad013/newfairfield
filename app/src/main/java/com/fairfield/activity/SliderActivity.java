package com.fairfield.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.fairfield.R;
import com.fairfield.adapter.MyAdapter;
import com.fairfield.controller.App;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class SliderActivity extends AppCompatActivity {

    private static ViewPager mPager;
    private static int currentPage = 0;
    App app;
    private static final String TAG = "SliderActivity";


    private List<String> imageslist = new ArrayList<String>();
    String baseurl_for_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = App.getInstance();



            String from = getIntent().getExtras().getString("FROM", "event");

        //    Log.e(TAG, "from: " + from);

            if (from.equals("event")) {

                baseurl_for_image = "https://www.localneighborhoodapp.com/assets/events/";
            } else {

                baseurl_for_image = "https://www.localneighborhoodapp.com/assets/rating_review/";
            }

        imageslist = app.eventCommentModelArrayList;
       // Log.e(TAG, "imageslist: "+app.eventCommentModelArrayList );

       // Log.e(TAG, "imageslist: "+imageslist );

        setContentView(R.layout.activity_slider);
        init();
    }
    private void init() {
        mPager = (ViewPager) findViewById(R.id.pager);



        mPager.setAdapter(new MyAdapter(SliderActivity.this,imageslist ,baseurl_for_image ));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        /*// Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == imageslist.size()) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);*/
    }
}
