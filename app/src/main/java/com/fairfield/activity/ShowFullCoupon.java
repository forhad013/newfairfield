package com.fairfield.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.fairfield.R;
import com.fairfield.adapter.CouponPagerAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityShowFullCouponBinding;
import com.fairfield.model.ProvidersModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowFullCoupon extends AppCompatActivity {

    private ActivityShowFullCouponBinding binding;

    private CouponPagerAdapter adapter;

    private PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_show_full_coupon);
        setSupportActionBar(binding.myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(" ");
        }
        binding.myToolbar.getBackground().setAlpha(0);
        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_white));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        preferenceHelper = new PreferenceHelper(this);

        adapter = new CouponPagerAdapter(getSupportFragmentManager(), App.getInstance().coupons);
        binding.pagerCoupons.setAdapter(adapter);
        binding.pagerCoupons.setCurrentItem(App.getInstance().couponIndex);

        binding.fab.setVisibility(App.getInstance().showBusinessInfo ? View.VISIBLE : View.GONE);
        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProviderMatchCoupon(App.getInstance().coupons.get(binding.pagerCoupons.getCurrentItem()).getOwnerId());
            }
        });
        binding.aviBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void showProviderMatchCoupon(final String ownerId) {
        startAnim();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("search_keyword", "");
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("value_match", "0");

        Constant.retrofitService.searchBusinessOwner(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        Gson gson = new Gson();
                        String responsestring = response.body().string();
                        JSONObject jsonObject = new JSONObject(responsestring);

                        if (jsonObject.getBoolean("status")) {
                            Type type = new TypeToken<List<ProvidersModel>>() {
                            }.getType();

                            ArrayList<ProvidersModel> providersModels = gson.fromJson(jsonObject.getString("data"), type);
                            for (ProvidersModel provider : providersModels) {
                                if (provider.getOwnerId().equals(ownerId)) {
                                    stopAnim();
                                    App.getInstance().providersModel = provider;
                                    Intent intent = new Intent(ShowFullCoupon.this, ProviderDetails.class);
                                    startActivity(intent);
                                    return;
                                }
                            }
                        } else {
                            stopAnim();
                        }
                    } else {
                        stopAnim();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    stopAnim();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                stopAnim();
            }
        });
    }

    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        binding.aviBackground.setVisibility(View.VISIBLE);
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        binding.aviBackground.setVisibility(View.GONE);
    }
}
