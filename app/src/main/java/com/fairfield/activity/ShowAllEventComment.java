package com.fairfield.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.fairfield.R;
import com.fairfield.adapter.EventCommentAdapter;
import com.fairfield.controller.App;
import com.fairfield.databinding.ActivityShowAllEventCommentBinding;
import com.fairfield.model.EventCommentModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowAllEventComment extends AppCompatActivity {


    private ActivityShowAllEventCommentBinding binding;
    private Context mContext;
    private PreferenceHelper preferenceHelper;
    private HashMap<String , String> hashMap;
    private static final String TAG = "ShowAllEventComment";
    private ArrayList<EventCommentModel> arrayList;
    private Gson gson;
    private EventCommentAdapter adapter;
    private App app;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        binding = DataBindingUtil.setContentView(this, R.layout.activity_show_all_event_comment);
        setSupportActionBar(binding.myToolbar);
        mContext = this;
        gson = new Gson();
        app = App.getInstance();
        preferenceHelper = new PreferenceHelper(mContext);
        arrayList = new ArrayList<>();
        if(!arrayList.isEmpty()){
            arrayList.clear();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        binding.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));
        binding.recyclerView.getLayoutManager().setAutoMeasureEnabled(true);
        binding.recyclerView.setNestedScrollingEnabled(false);
        binding.recyclerView.setHasFixedSize(false);

        adapter = new EventCommentAdapter(mContext, arrayList);
        binding.recyclerView.setAdapter(adapter);
        showEventComment();
        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void showEventComment() {

        startAnim();
        final HashMap<String, String> meMap = new HashMap<String, String>();
        meMap.put("event_id", app.localeventsModel.getId());
        Constant.retrofitService.showEventComments(meMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(final Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {

                        String responsestring = response.body().string();
                        final JSONObject jsonObject = new JSONObject(responsestring);
                        stopAnim();
                        if (jsonObject.getBoolean("status")) {
                       //     Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");

                            if (!arrayList.isEmpty()) {
                                arrayList.clear();
                            }
                            final Type type = new TypeToken<List<EventCommentModel>>() {
                            }.getType();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    try {
                                        arrayList = gson.fromJson(jsonObject.getString("data"), type);


                           //             Log.e("ArrayList", "" + arrayList.size());
                                        adapter.setEventCommentModelList(arrayList);
                                        adapter.notifyDataSetChanged();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        Utility.showAlert(mContext, "Please try after some time.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    void startAnim() {
        binding.avi.setVisibility(View.VISIBLE);
        // or avi.smoothToShow();
    }

    void stopAnim() {
        binding.avi.setVisibility(View.GONE);
        // or avi.smoothToHide();
    }
}
