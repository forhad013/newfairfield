package com.fairfield.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.fairfield.R;
import com.fairfield.databinding.ActivityNewsFeedBinding;
import com.fairfield.fragment.NewBusinessFragment;
import com.fairfield.fragment.NewFeedbackRequest;
import com.fairfield.fragment.NewReviewFragment;
import com.fairfield.model.BannerModel;
import com.fairfield.utility.Constant;
import com.fairfield.utility.PreferenceHelper;
import com.fairfield.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Argalon on 6/19/2017.
 */

public class NewsFeedActivity extends AppCompatActivity {

    ActivityNewsFeedBinding binding;
    Context mContext;
    Gson gson;

    private static final String TAG = "NewsFeedActivity";
    private ImageView[] imgv;
    private ArrayList<BannerModel> bannerArrayList;

    private boolean leftToRight = true;
    private int count = 0;

    private PreferenceHelper preferenceHelper;
    private Handler mHandler;
    private Runnable mUpdateResults;
    public int currentimageindex = 0;
    private Timer timer;
    private int[] IMAGE_IDS = {R.drawable.banner, R.drawable.btn_fb, R.drawable.like,
            R.drawable.new_logo};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        gson = new Gson();
        bannerArrayList = new ArrayList<>();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news_feed);
        setSupportActionBar(binding.myToolbar);

        bannerArrayList = new ArrayList<>();
        preferenceHelper = new PreferenceHelper(mContext);
        timer = new Timer();
        mHandler = new Handler();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }

        binding.myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String Fragment = getIntent().getStringExtra("Fragment");

        if (Fragment.equals("NewReviewFragment")) {
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            NewReviewFragment fragment = new NewReviewFragment();
            transaction.replace(R.id.fragment_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (Fragment.equals("NewFeedbackRequest")) {
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            NewFeedbackRequest fragment = new NewFeedbackRequest();
            transaction.replace(R.id.fragment_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else {
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            NewBusinessFragment fragment = new NewBusinessFragment();
            transaction.replace(R.id.fragment_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        getAllCoupons();


        binding.slidingimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BannerModel banner = bannerArrayList.get(currentimageindex - 1);
                bannerClick(banner.getId(), banner.getOwnerId());





              /*  AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);
                dialog.setView(dialogLayout);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.show();


                ImageView image = (ImageView) dialog.findViewById(R.id.goProDialogImage);
                //  Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),
                //      R.drawable.whygoprodialogimage);

                try {
                    String url = "https://www.localneighborhoodapp.com/images/banner/";
                    String urlmain = url + bannerArrayList.get(currentimageindex - 1).getImage();
                    Log.e(TAG, "AnimateandSlideShow: " + urlmain);
                    Picasso.with(mContext).load(urlmain).into(image);

                } catch (Exception e) {
                    System.out.println(e);
                }
*/

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case R.id.newbusiness_Action:
                android.support.v4.app.FragmentTransaction transaction1 = getSupportFragmentManager().beginTransaction();
                NewBusinessFragment fragment = new NewBusinessFragment();
                transaction1.replace(R.id.fragment_container, fragment);
                transaction1.addToBackStack(null);
                transaction1.commit();
                break;
            case R.id.newreviewsAction:
                android.support.v4.app.FragmentTransaction transactions = getSupportFragmentManager().beginTransaction();
                NewReviewFragment newReviewFragment = new NewReviewFragment();
                transactions.replace(R.id.fragment_container, newReviewFragment);
                transactions.addToBackStack(null);
                transactions.commit();
                break;
            case R.id.reviewrequestAction:
                android.support.v4.app.FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
                NewFeedbackRequest newFeedbackRequest = new NewFeedbackRequest();
                transaction2.replace(R.id.fragment_container, newFeedbackRequest);
                transaction2.addToBackStack(null);
                transaction2.commit();

                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.newsfeed, menu);


        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Quit if back is pressed
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    //------------------------------------------------------------------------------------------------------------------for add--------------------------------------------------------

    private void bannerClick(String bannerId, final String ownerId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", String.valueOf(bannerId));
        hashMap.put("owner_id", String.valueOf(ownerId));
        Constant.retrofitService.bannerClick(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        JSONObject jsonObject = new JSONObject(responseString);

                        if (jsonObject.getBoolean("status")) {
                            Intent intent = new Intent(mContext, ProviderDetails.class);
                            intent.putExtra("From", "Ad");
                            intent.putExtra("OWNERID", ownerId);
                            startActivity(intent);
                        } else {
                            // show message error
                            Utility.showAlert(mContext, jsonObject.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utility.showAlert(mContext, "something went wrong");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utility.showAlert(mContext, "something went wrong");
            }
        });
    }

    private void getAllCoupons() {
        Constant.retrofitService.getAllbanners().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        String responseString = response.body().string();
                        JSONObject jsonObject = new JSONObject(responseString);

                        if (jsonObject.getBoolean("status")) {
                            //   Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                            //  allTaskModelArrayList.clear();
                            Type type = new TypeToken<List<BannerModel>>() {
                            }.getType();

                            if (!bannerArrayList.isEmpty()) {
                                bannerArrayList.clear();
                            }
                            bannerArrayList = gson.fromJson(jsonObject.getString("data"), type);
                            //        Log.e("ArrayList", "" + bannerArrayList.size());

                            try {
                                bannerStart();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void bannerStart() {
        try {
            // Create runnable for posting
            mUpdateResults = new Runnable() {
                public void run() {

                    if (bannerArrayList.size() == currentimageindex) {
                        currentimageindex = 0;
                        AnimateandSlideShow();
                    } else {
                        AnimateandSlideShow();
                    }


                }
            };

            int delay = 1000; // delay for 1 sec.
            int period = 2000; // repeat every 4 sec.

            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {
                    mHandler.post(mUpdateResults);
                }

            }, delay, period);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void AnimateandSlideShow() {


        try {
            String url = "https://www.localneighborhoodapp.com/images/banner/";

            String urlmain = url + bannerArrayList.get(currentimageindex).getImage();
            //    Log.e(TAG, "AnimateandSlideShow: " + urlmain);
            Picasso.with(mContext).load(urlmain).into(binding.slidingimage);
            //     Log.e(TAG, "AnimateandSlideShow: " + currentimageindex);

            // saveBannerview(bannerArrayList.get(currentimageindex).getId(), bannerArrayList.get(currentimageindex).getOwnerId());

            currentimageindex++;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onStop() {
        super.onStop();
        //   Log.e(TAG, "onStop: ");


        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //    Log.e(TAG, "onRestart: ");
        try {
            mHandler = new Handler();
            timer = new Timer();
            if (!bannerArrayList.isEmpty()) {
                bannerStart();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void saveBannerview(String banner_id, String owner_id) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.USER_ID, preferenceHelper.getUserId());
        hashMap.put("banner_id", banner_id);
        hashMap.put("owner_id", owner_id);

        //  Log.e(TAG, "saveBannerview: " + hashMap);

        Constant.retrofitService.saveBannerview(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                //            Log.d(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    /*   private void getAllCoupons() {
           Constant.retrofitService.getAllbanners().enqueue(new Callback<ResponseBody>() {
               @Override
               public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                   try {
                       if (response.isSuccessful()) {
                           String responsestring = response.body().string();
                           JSONObject jsonObject = new JSONObject(responsestring);

                           if (jsonObject.getBoolean("status")) {
                               Log.e(TAG, "onResponse() called with: call = [" + call.request().url() + "], response = [" + jsonObject + "]");
                               //  allTaskModelArrayList.clear();
                               Type type = new TypeToken<List<BannerModel>>() {
                               }.getType();

                               if (!bannerArrayList.isEmpty()) {
                                   bannerArrayList.clear();
                               }
                               bannerArrayList = gson.fromJson(jsonObject.getString("data"), type);
                               Log.e("ArrayList", "" + bannerArrayList.size());

                               try {
                                   setBannerimageView();
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                           } else {

                           }
                       } else {

                       }
                   } catch (IOException e) {
                       e.printStackTrace();
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }

               }

               @Override
               public void onFailure(Call<ResponseBody> call, Throwable t) {
               }
           });
       }


       public void setBannerimageView() {
           imgv = new ImageView[bannerArrayList.size()];
           for (int j = 0; j < bannerArrayList.size(); j++) {
               imgv[j] = new ImageView(this);

               ImageView img = new ImageView(NewsFeedActivity.this);
               LinearLayout.LayoutParams para = new LinearLayout.LayoutParams(600,200);
               para.leftMargin = 10;
               para.topMargin = 5;
               imgv[j].setLayoutParams(para);


               imgv[j].setScaleType(ImageView.ScaleType.FIT_XY);
               //  imgv[j].setOnClickListener(this);
               //   imgv[j].setBackgroundResource(R.drawable.banner);

               String url = "https://www.localneighborhoodapp.com/images/banner/";

               String urlmain = url + bannerArrayList.get(j).getImage();
               Log.e(TAG, "AnimateandSlideShow: " + urlmain);
               Picasso.with(mContext).load(urlmain).into(imgv[j]);
               Log.e(TAG, "AnimateandSlideShow: " + j);
               binding.horizontalOuterLayout.addView(imgv[j], para);

             *//*  images.add(Imgid[j].toString());

            System.out.println("string arraylist@@@@@@@" + images.get(j));*//*
        }
        MCObject = new MyCounter(bannerArrayList.size(), 1);
        MCObject.start();

    }


    public class MyCounter extends CountDownTimer {

        public MyCounter(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            if (leftToRight == true) {

                MCObject.cancel();
                //if (gallery.getScrollX() > (((imgv.length - 1) * multiplyValue)+addValue)) {

                if (binding.horizontalScrollview.getScrollX() > ((imgv.length - 1 ) * 800) ) {
                    count=((imgv.length - 1) *800);
                    leftToRight = false;
                    MCObject.start();
                }

                if (count != binding.horizontalScrollview.getScrollX()) {
                    binding.horizontalScrollview.scrollBy(1, 0);

                    count++;
                    MCObject.start();
                } else {
                    count=((imgv.length - 1) *800);
                    leftToRight = false;
                    MCObject.start();
                }
            } else {
                MCObject.cancel();
                if (binding.horizontalScrollview.getScrollX() <= 0) {
                    count = -2;
                    leftToRight = true;
                    MCObject = new MyCounter(bannerArrayList.size(), 1);
                    MCObject.start();
                }
                if (count != 0) {
                    binding.horizontalScrollview.scrollBy(-1, 0);
                    count--;
                    MCObject.start();
                } else {
                    count = -2;
                    leftToRight = true;
                    MCObject = new MyCounter(bannerArrayList.size(), 1);
                    MCObject.start();

                }
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

    }
*/
}
