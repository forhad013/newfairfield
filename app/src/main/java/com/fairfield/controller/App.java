package com.fairfield.controller;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.facebook.stetho.Stetho;
import com.fairfield.model.Bannerdatum;
import com.fairfield.model.Coupondatum;
import com.fairfield.model.EventCommentModel;
import com.fairfield.model.FeedbackModel;
import com.fairfield.model.LocalEventsModel;
import com.fairfield.model.ProvidersModel;
import com.fairfield.model.UserModel;
import com.fairfield.utility.FontsOverride;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Argalon on 5/4/2017.
 */

public class App extends Application {

    static App mInstance;
    public double myCurrentLat = 0.0;
    public double myCurrentLng = 0.0;

    public UserModel  userModel;
    public LocalEventsModel localeventsModel;
    public EventCommentModel eventCommentModel;
//    public ShowCoupansModel showCoupansModel;
    public List<Coupondatum> coupons;
    public int couponIndex;
    public boolean showBusinessInfo;
    public List<Bannerdatum> banners;
    public int bannerIndex;
    public ProvidersModel providersModel;
    public String ownerid;
    public List<String> eventCommentModelArrayList;

    public FeedbackModel feedbackModel;
    public  int badgeCount = 0;

//    public int showwelcome = 0;
    @Override
    public void onCreate() {
        super.onCreate();
        setmInstance(this);
        Realm.init(this);
        userModel = new UserModel();
        localeventsModel = new LocalEventsModel();
        eventCommentModel = new EventCommentModel();
//        showCoupansModel = new ShowCoupansModel();
        providersModel = new ProvidersModel();
        feedbackModel = new FeedbackModel();
        coupons = new ArrayList<>();
        couponIndex = 0;
        showBusinessInfo = false;
        banners = new ArrayList<>();
        bannerIndex = 0;

        eventCommentModelArrayList = new ArrayList<>();

        FontsOverride.setDefaultFont(this, "SERIF", "fonts/button_regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/edittext_regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/toolbar_lato_regIta.ttf");


        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());

    }

    public void setmInstance(App mInstance) {
        App.mInstance = mInstance;
    }

    public static synchronized App getInstance() {
        return mInstance;
    }


    private static RealmConfiguration getRealmDatabaseConfiguration() {
        // return new RealmConfiguration.Builder().name(getInstance().getString(R.string.app_name) + PreferenceManager.getToken(getInstance()) + ".realm").deleteRealmIfMigrationNeeded().build();
        return new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
    }

    public static Realm getRealmDatabaseInstance() {
        return Realm.getInstance(getRealmDatabaseConfiguration());
    }

    public static boolean DeleteRealmDatabaseInstance() {
        return Realm.deleteRealm(getRealmDatabaseConfiguration());
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
