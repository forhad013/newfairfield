/*
package com.fairfield.Intentservice;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import com.fairfield.controller.App;
import com.fairfield.utility.CheckNetworkConnectivity;
import com.fairfield.utility.Constant;
import com.fairfield.utility.MarshMallowPermission;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

*/
/**
 * Created by Argalon on 6/12/2017.
 * Checks whether two providers are the same
 * Checks whether two providers are the same
 * Checks whether two providers are the same
 * Checks whether two providers are the same
 * Checks whether two providers are the same
 *//*


public class MyTestService extends IntentService{

    Context mContext;
    CheckNetworkConnectivity checkNetworkConnectivity;
    private Handler handler = new Handler();
    Bitmap imagegdsgd;
    private GoogleApiClient mGoogleApiClient;
    private MarshMallowPermission marshMallowPermission;

    public static final String BROADCAST_ACTION = "Hello World";
   // private static final int TWO_MINUTES = 1000 * 60 * 1;
    private static final int TWO_MINUTES = 6000;
    public LocationManager locationManager;
    public MyLocationListener listener;
    public Location previousBestLocation = null;

    String  lat;
    String log;
    App app;
    Intent intent;
    int counter = 0;
    @Override




    public void onCreate() {
        super.onCreate();
        mContext = this;
        app = App.getInstance();
        checkNetworkConnectivity = new CheckNetworkConnectivity(mContext);
        marshMallowPermission = new MarshMallowPermission(mContext);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();

        intent = new Intent(BROADCAST_ACTION);


    */
/*    Runnable runnable2 = new Runnable() {
            public void run() {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                listener = new MyLocationListener();

                try {
                    if (!marshMallowPermission.checkPermissionForLocation()) {
                        try {
                            marshMallowPermission.requestPermissionForLocation();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        getLocationLatLng();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onConnected: ", e);
                }

                handler.postDelayed(this, 1200000);
            }
        };
        runnable2.run();*//*

    }

    public MyTestService() {
        super("MyTestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Do the task here
        Log.e("MyTestService", "Service running");
        getLocationLatLng();

    //    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
     //   listener = new MyLocationListener();

    */
/*    try {
            if (!marshMallowPermission.checkPermissionForLocation()) {
                try {
                    marshMallowPermission.requestPermissionForLocation();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

            }
        } catch (Exception e) {
            Log.e(TAG, "onConnected: ", e);
        }*//*


        if (checkNetworkConnectivity.isNetworkAvailable()) {
         //  notification();
        }
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        super.onStart(intent, startId);


    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }



    */
/** Checks whether two providers are the same *//*

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }



    @Override
    public void onDestroy() {
        // handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
        Log.v("STOP_SERVICE", "DONE");
        locationManager.removeUpdates(listener);
    }

    public static Thread performOnBackgroundThread(final Runnable runnable) {
        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } finally {

                }
            }
        };
        t.start();
        return t;
    }




    public class MyLocationListener implements LocationListener
    {


        public void onLocationChanged(final Location loc)
        {
            Log.i("*****************", "Location changed");
            if(isBetterLocation(loc, previousBestLocation)) {
                app.myCurrentLat =loc.getLatitude();
                app.myCurrentLng = loc.getLongitude();

                Toast.makeText( getApplicationContext(), ""+loc.getLatitude(), Toast.LENGTH_SHORT ).show();
                Log.e(TAG, "getLatitude: "+loc.getLatitude() );
                Log.e(TAG, "getLatitude: "+loc.getLongitude() );

                intent.putExtra("Latitude", loc.getLatitude());
                intent.putExtra("Longitude", loc.getLongitude());
                intent.putExtra("Provider", loc.getProvider());
                sendBroadcast(intent);

            }
        }

        public void onProviderDisabled(String provider)
        {

            Log.e(TAG, "onProviderDisabled: Disabled" );
            Toast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();
        }


        public void onProviderEnabled(String provider)
        {

            Log.e(TAG, "onProviderEnabled: " );
            Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }


        public void onStatusChanged(String provider, int status, Bundle extras)
        {


            Log.e(TAG, "onStatusChanged: " );
        }
    }

    private void notification() {

        HashMap<String , String> hashMap = new HashMap<>();
        hashMap.put("latitude" , ""+app.myCurrentLat);
        hashMap.put("longitude" , ""+ app.myCurrentLat);

        Constant.retrofitService.businessGeofencing(hashMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onResponse: ");
            }
        });

     */
/*   NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_push);
        contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher);
      //  contentView.setTextViewText(R.id.title, "Custom notification");
      //  contentView.setTextViewText(R.id.text, "This is a custom layout");

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContent(contentView);

        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;


        int jjj = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        manager.notify(jjj, notification);*//*



     */
/*   Notification foregroundNote;

        RemoteViews bigView = new RemoteViews(getApplicationContext().getPackageName(),
                R.layout.custom_push);

*//*

// bigView.setOnClickPendingIntent() etc..
   */
/*     Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.frame_image);
        Notification.Builder mNotifyBuilder = new Notification.Builder(this);
        foregroundNote = mNotifyBuilder.setContentTitle("some string")
                .setContentText("Slide down on note to expand")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .build();*//*



 //  -------------------------------------------------------------------------------for_coustom_notification-----------------------

   */
/*     int jjj = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        try {
            URL url = new URL("http://i.imgur.com/N6SaAlZ.jpg");
            imagegdsgd = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch(Exception e) {
            System.out.println(e);
        }
        Notification notif = new Notification.Builder(mContext)
                .setContentTitle("lat" +   app.myCurrentLat +" "+"log"+   app.myCurrentLng)
                .setContentText("FairField")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(imagegdsgd)
                .setStyle(new Notification.BigPictureStyle()
                        .bigPicture(imagegdsgd))
                .build();

      //  foregroundNote.bigContentView = bigView;

// now show notification..
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyManager.notify(jjj, notif);



*//*



//  -------------------------------------------------------------------------------for_coustom_notification-----------------------
    }



private void  getLocationLatLng(){

    try {
        Log.e(TAG, "getLocationLatLng: " );
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 4000, 0, listener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 4000, 0, listener);

    } catch (SecurityException e) {
        e.printStackTrace();
    }
}

}*/

/***************************************************************************************************************************************/
/*
package com.fairfield.Intentservice;

import android.Manifest;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import com.fairfield.controller.App;
import com.fairfield.utility.CheckNetworkConnectivity;
import com.fairfield.utility.Constant;
import com.fairfield.utility.MarshMallowPermission;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

*/
/**
 * Created by Argalon on 6/12/2017.
 *//*


public class MyTestService extends IntentService implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    Context mContext;
    CheckNetworkConnectivity checkNetworkConnectivity;
    private Handler handler = new Handler();
    Bitmap imagegdsgd;
    private GoogleApiClient mGoogleApiClient;
    private MarshMallowPermission marshMallowPermission;

    public static final String BROADCAST_ACTION = "Hello World";
    // private static final int TWO_MINUTES = 1000 * 60 * 1;
    private static final int TWO_MINUTES = 1000;
    public LocationManager locationManager;
    public MyLocationListener listener;
    public Location previousBestLocation = null;
    Runnable runnable2;
    String lat;
    String log;
    App app;
    Intent intent;
    int counter = 0;
    int i = 0;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        app = App.getInstance();
        checkNetworkConnectivity = new CheckNetworkConnectivity(mContext);
        marshMallowPermission = new MarshMallowPermission(mContext);
        intent = new Intent(BROADCAST_ACTION);

        // initializeLocationManager();


        try {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public MyTestService() {
        super("MyTestService");
    }



    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (locationManager == null) {
            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

            Log.e(TAG, "locationManager in : " + locationManager);
            listener = new MyLocationListener();
        } else {

            Log.e(TAG, "locationManager in else: " + locationManager);
        }

        try {
            if (!marshMallowPermission.checkPermissionForLocation()) {
                try {
                    marshMallowPermission.requestPermissionForLocation();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                //  handler.removeCallbacks(this);
                // handler.removeCallbacks(runnable2);
                // handler.removeCallbacksAndMessages(0);
                Log.e(TAG, "onConnected: in else ");
                getLocationLatLng();


            }
        } catch (Exception e) {
            Log.e(TAG, "onConnected: ", e);
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Do the task here
        Log.e("MyTestService", "Service running");



       */
/* locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();
*//*




        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        super.onStart(intent, startId);

        Log.e(TAG, "onStart: ");

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        //  myHandler = new Handler();
        //  myHandler.post(myRunnable);


    */
/*    runnable2 = new Runnable() {
            public void run() {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                listener = new MyLocationListener();

                try {
                    if (!marshMallowPermission.checkPermissionForLocation()) {
                        try {
                            marshMallowPermission.requestPermissionForLocation();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                             handler.removeCallbacks(this);
                              handler.removeCallbacks(runnable2);
                             handler.removeCallbacksAndMessages(0);

                           getLocationLatLng();


                    }
                } catch (Exception e) {
                    Log.e(TAG, "onConnected: ", e);
                }


                handler.postDelayed(this, 120000);
            }
        };
        runnable2.run();
*//*

    }

   */
/* private Handler myHandler;
    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            listener = new MyLocationListener();
            Log.e(TAG, "in run: ");
            try {
                if (!marshMallowPermission.checkPermissionForLocation()) {
                    try {
                        marshMallowPermission.requestPermissionForLocation();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    Log.e(TAG, "start: ");

                    getLocationLatLng();


                }
            } catch (Exception e) {
                Log.e(TAG, "onConnected: ", e);
            }

        }
    };*//*


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }


    */
/** Checks whether two providers are the same *//*

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }


    @Override
    public void onDestroy() {
        // handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
        Log.v("STOP_SERVICE", "DONE");
    //    locationManager.removeUpdates(listener);

    }

    public static Thread performOnBackgroundThread(final Runnable runnable) {
        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } finally {

                }
            }
        };
        t.start();
        return t;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.e(TAG, "onConnected: ");


        try {
            if (!marshMallowPermission.checkPermissionForLocation()) {
                try {
                    marshMallowPermission.requestPermissionForLocation();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                //  handler.removeCallbacks(this);
                // handler.removeCallbacks(runnable2);
                // handler.removeCallbacksAndMessages(0);
                Log.e(TAG, "onConnected: in else ");
                try {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.

                        Log.e(TAG, "in permission: " );
                        return;
                    }
                    Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    if (mLastLocation != null) {
                        Double lat = mLastLocation.getLatitude();
                        Double longi = mLastLocation.getLongitude();

                        Log.e(TAG, "lat: " + lat);
                        Log.e(TAG, "longi: " + longi);


                        app.myCurrentLat = lat;
                        app.myCurrentLng = longi;
                        Log.e(TAG, "getLatitude: " + app.myCurrentLat);
                        Log.e(TAG, "getLatitude: " +  app.myCurrentLng);

                        try {
                            if (checkNetworkConnectivity.isNetworkAvailable()) {
                                notification();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to get Location Check GPS is ON", Toast.LENGTH_LONG).show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        } catch (Exception e) {
            Log.e(TAG, "onConnected: ", e);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.e(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: ");

    }


    public class MyLocationListener implements LocationListener {

        public void onLocationChanged(final Location loc) {
            try {

                Toast.makeText(getApplicationContext(), "Location changed", Toast.LENGTH_SHORT).show();
                Log.i("*****************", "Location changed");
                if (isBetterLocation(loc, previousBestLocation)) {
                    app.myCurrentLat = loc.getLatitude();
                    app.myCurrentLng = loc.getLongitude();
                    Log.e(TAG, "getLatitude: " + loc.getLatitude());
                    Log.e(TAG, "getLatitude: " + loc.getLongitude());

                    intent.putExtra("Latitude", loc.getLatitude());
                    intent.putExtra("Longitude", loc.getLongitude());
                    intent.putExtra("Provider", loc.getProvider());
                    sendBroadcast(intent);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();
        }


        public void onProviderEnabled(String provider) {
            Toast.makeText(getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }


        public void onStatusChanged(String provider, int status, Bundle extras) {

            Log.e(TAG, "onStatusChanged: ");
        }

    }


    private void notification() {

        try {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("latitude", "" + app.myCurrentLat);
            hashMap.put("longitude", "" + app.myCurrentLng);

            Log.e(TAG, "latitude: " + app.myCurrentLat);
            Log.e(TAG, "longitude: " + app.myCurrentLng);
            Log.e(TAG, "notification: " + hashMap);


            Constant.retrofitService.businessGeofencing(hashMap).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String res = response.body().string();


                        Log.e(TAG, "onResponse: " + res);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(TAG, "onResponse: ");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }



     */
/*   NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_push);
        contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher);
      //  contentView.setTextViewText(R.id.title, "Custom notification");
      //  contentView.setTextViewText(R.id.text, "This is a custom layout");

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContent(contentView);

        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;


        int jjj = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        manager.notify(jjj, notification);*//*



     */
/*   Notification foregroundNote;

        RemoteViews bigView = new RemoteViews(getApplicationContext().getPackageName(),
                R.layout.custom_push);

*//*

// bigView.setOnClickPendingIntent() etc..
   */
/*     Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.frame_image);
        Notification.Builder mNotifyBuilder = new Notification.Builder(this);
        foregroundNote = mNotifyBuilder.setContentTitle("some string")
                .setContentText("Slide down on note to expand")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .build();*//*





   */
/*


        int jjj = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        try {
            URL url = new URL("http://i.imgur.com/N6SaAlZ.jpg");
            imagegdsgd = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch(Exception e) {
            System.out.println(e);
        }
        Notification notif = new Notification.Builder(mContext)
                .setContentTitle("lat" +   app.myCurrentLat +" "+"log"+   app.myCurrentLng)
                .setContentText("FairField")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(imagegdsgd)
                .setStyle(new Notification.BigPictureStyle()
                        .bigPicture(imagegdsgd))
                .build();
        *//*



        //  foregroundNote.bigContentView = bigView;

// now show notification..
        //      NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //    mNotifyManager.notify(jjj, notif);

    }

    private void getLocationLatLng() {

        try {
            try {
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, listener);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, listener);

            } catch (SecurityException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}*/
