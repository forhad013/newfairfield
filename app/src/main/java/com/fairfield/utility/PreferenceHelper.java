package com.fairfield.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class PreferenceHelper {

    private SharedPreferences app_prefs;
    private final String USER_ID = "user_id";
    private final String STATUS = "status";
    private final String EMAIL = "email";
    private final String PICTURE = "picture";
    private final String PUSHNOTIFICATION = "notification";
    private final String DEVICE_TOKEN = "device_token";
    private final String SESSION_TOKEN = "session_token";
    private final String LOGIN_BY = "login_by";
    private final String SOCIAL_ID = "social_id";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String PRE_LOAD = "preLoad";
    private final String REQUEST_ID = "request_id";
    private final String NAME = "name";
    private final String REQ_TIME = "req_time";
    private final String ACCEPT_TIME = "accept_time";
    private final String CURRENT_TIME = "current_time";
    private final String CURRENCY = "currency";
    private final String LANGUAGE = "language";
    private final String REQUEST_TYPE = "type";
    private final String PAYMENT_MODE = "payment_mode";
    private final String SHOWDIALOGCOUNT = "showdialogcount";
    private final String CLICKTIME = "clicktime";

    public static final String NEW_REWARDS_NOTI = "newRewardsNoti";
    public static final String SPECIAL_NOTI = "specialNoti";
    public static final String NEW_EVENTS_NOTI = "newEventsNoti";
    public static final String NEW_BUSINESS_NOTI = "newBusinessNoti";
    public static final String REQUESTED_REVIEW_NOTI = "requestedReviewNoti";
    public static  final String NEW_REVIEWS_NOTI = "newReviewsNoti";
    public static  final String NEW_COUPON_NOTI = "newCouponNoti";
    public static  final String PREVIOUS_USER = "firstTime";



    private Context context;


    //status = 0 nonvarify
    //status = 1 varify

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(Constant.PREF_NAME,
                Context.MODE_PRIVATE);
        this.context = context;
    }

    public int getshareprefdata(String KEY) {
        return app_prefs.getInt(KEY, 160);
    }

    public void setshareprefdata(String KEY, int value) {

        app_prefs.edit().putInt(KEY, value).commit();
    }


    public String getshareprefdatastring(String KEY) {
        return app_prefs.getString(KEY, "");
    }

    public void setshareprefdatastring(String KEY, String values) {

        app_prefs.edit().putString(KEY, values).commit();
    }

    public Boolean getshareprefdataBoolean(String KEY) {
        return app_prefs.getBoolean(KEY, false);
    }

    public void setshareprefdataBoolean(String KEY, Boolean values) {

        app_prefs.edit().putBoolean(KEY, values).commit();
    }
    public void putUserId(String userId) {
        Editor edit = app_prefs.edit();
        edit.putString(USER_ID, userId);
        edit.commit();
    }

    public void putUser_name(String name) {
        Editor edit = app_prefs.edit();
        edit.putString(NAME, name);
        edit.commit();
    }

    public String getUser_name() {
        return app_prefs.getString(NAME, "");
    }


    public void putEmail(String email) {
        Editor edit = app_prefs.edit();
        edit.putString(EMAIL, email);
        edit.commit();
    }

    public String getEmail() {
        return app_prefs.getString(EMAIL, null);
    }

    public void putNotification(Boolean value){
        Editor edit = app_prefs.edit();
        edit.putBoolean(PUSHNOTIFICATION , value);
        edit.commit();

    }

    public Boolean getNotification() {
        return app_prefs.getBoolean(PUSHNOTIFICATION, true);
    }



    public void putShowDialogCount(int i){
        Editor edit = app_prefs.edit();
        edit.putInt(SHOWDIALOGCOUNT , i);
        edit.commit();
    }


    public void putClickTime(long i){
        Editor edit = app_prefs.edit();
        edit.putLong(CLICKTIME , i);
        edit.commit();
    }

    public long getClickTime() {
        return app_prefs.getLong(CLICKTIME, 0);
    }



    public int getShowDialogCount() {
        return app_prefs.getInt(SHOWDIALOGCOUNT, 0);
    }


    public void putPicture(String picture) {
        Editor edit = app_prefs.edit();
        edit.putString(PICTURE, picture);
        edit.commit();
    }

    public void putRequestId(int reqId) {
        Editor edit = app_prefs.edit();
        edit.putInt(REQUEST_ID, reqId);
        edit.commit();
    }


    public void putSocialId(String id) {
        Editor edit = app_prefs.edit();
        edit.putString(SOCIAL_ID, id);
        edit.commit();
    }

    public String getSocialId() {
        return app_prefs.getString(SOCIAL_ID, null);
    }

    public String getUserId() {
        return app_prefs.getString(USER_ID, null);
    }

    public void putStatus(String status){
        Editor edit = app_prefs.edit();
        edit.putString(STATUS, status);
        edit.commit();
    }

    public String getStaus(){
        return app_prefs.getString(STATUS, Constant.NONVEARIFY);
    }

    public void putDeviceToken(String deviceToken) {
        Editor edit = app_prefs.edit();
        edit.putString(DEVICE_TOKEN, deviceToken);
        edit.commit();
    }

    public String getDeviceToken() {
        return app_prefs.getString(DEVICE_TOKEN, null);

    }

    public void putLoginBy(String loginBy) {
        Editor edit = app_prefs.edit();
        edit.putString(LOGIN_BY, loginBy);
        edit.commit();
    }

    public String getLoginBy() {
        return app_prefs.getString(LOGIN_BY, Constant.MANUAL);
    }


    public void putRegisterationID(String RegID) {
        Editor edit = app_prefs.edit();
        edit.putString(PROPERTY_REG_ID, RegID);
        edit.apply();
    }




    public void putLanguage(String language) {
        Editor edit = app_prefs.edit();
        edit.putString(LANGUAGE, language);
        edit.commit();
    }

    public String getLanguage() {
        return app_prefs.getString(LANGUAGE, "");
    }

    public void Logout() {
        putUserId(null);
        putLoginBy(Constant.MANUAL);
        putUserId(null);

    }

}


