package com.fairfield.utility;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.Build;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.fairfield.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class FirebaseSubscribe {


    PreferenceHelper preferenceHelper;

    Context context;

    public FirebaseSubscribe(Context context) {

        preferenceHelper = new PreferenceHelper(context);
        this.context = context;

    }

    public void subscribeToTopics() {




        FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_BUSINESS_NOTI_SUBSCRIBE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Log.e("status", task.isSuccessful() + "");

                       // Toast.makeText(context,"business topics success",Toast.LENGTH_LONG).show();
                        if (task.isSuccessful()) {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.PREVIOUS_USER, true);
                            subscribeOthers();
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_BUSINESS_NOTI, true);
                        } else {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.PREVIOUS_USER, false);
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_BUSINESS_NOTI, false);
                        }

                    }
                });


    }


    public void subscribeOthers(){
        FirebaseMessaging.getInstance().subscribeToTopic(Constant.REQUESTED_REVIEW_NOTI_SUBSCRIBE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Log.e("status", task.isSuccessful() + "");
                        if (task.isSuccessful()) {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.REQUESTED_REVIEW_NOTI, true);
                        } else {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.REQUESTED_REVIEW_NOTI, false);
                        }

                    }
                });


        FirebaseMessaging.getInstance().subscribeToTopic(Constant.SPECIAL_NOTI_SUBSCRIBE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Log.e("status", task.isSuccessful() + "");
                        if (task.isSuccessful()) {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.SPECIAL_NOTI, true);

                        } else {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.SPECIAL_NOTI, false);
                        }

                    }
                });


        FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_REVIEWS_NOTI_SUBSCRIBE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Log.e("status", task.isSuccessful() + "");
                        if (task.isSuccessful()) {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REVIEWS_NOTI, true);

                        } else {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REVIEWS_NOTI, false);
                        }

                    }
                });


        FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_REWARDS_SUBSCRIBE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Log.e("status", task.isSuccessful() + "");
                        if (task.isSuccessful()) {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REWARDS_NOTI, true);

                        } else {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_REWARDS_NOTI, false);
                        }

                    }
                });


        FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_EVENTS_NOTI_SUBSCRIBE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Log.e("status", task.isSuccessful() + "");
                        if (task.isSuccessful()) {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_EVENTS_NOTI, true);

                        } else {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_EVENTS_NOTI, false);
                        }

                    }
                });


        FirebaseMessaging.getInstance().subscribeToTopic(Constant.NEW_COUPON_NOTI_SUBSCRIBE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Log.e("status", task.isSuccessful() + "");
                        if (task.isSuccessful()) {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_COUPON_NOTI, true);

                        } else {
                            preferenceHelper.setshareprefdataBoolean(PreferenceHelper.NEW_COUPON_NOTI, false);
                        }

                    }
                });


    }

}



