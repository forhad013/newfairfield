/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fairfield.fcmservice;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.RemoteViews;

import com.fairfield.R;
import com.fairfield.activity.DashBoardActivity;
import com.fairfield.activity.LocalEventActivity;
import com.fairfield.activity.PopUpActivity;
import com.fairfield.activity.PramotionActivity;
import com.fairfield.activity.ProviderDetails;
import com.fairfield.activity.RewardActivity;
import com.fairfield.controller.App;
import com.fairfield.utility.PreferenceHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.Date;
import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static String REPLY_ACTION = "com.segunfamisa.notification.directreply.REPLY_ACTION";
    private static String KEY_REPLY = "key_reply_message";

    private int mNotificationId;
    private int mMessageId;
    Context mContext;
    JSONObject objectdata;
    JSONObject messageBodyobject;

    boolean showNotification = false;

    String userId;
    NotificationCompat.Builder builder;
    //    private static final String KEY_TEXT_REPLY = "key_text_reply";
    Bitmap bitmap;
    PreferenceHelper preferenceHelper;
    // Key for the string that's delivered in the action's intent.
    private static final String KEY_TEXT_REPLY = "key_text_reply";

    // mRequestCode allows you to update the notification.
    int mRequestCode = 1000;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override


    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "onMessageReceived() called with: remoteMessage = [" + remoteMessage + "]");
        Log.e(TAG, "From: " + remoteMessage.getFrom());


        try {


           Log.e("print", remoteMessage.getNotification() + "");

            showFCMNoti(remoteMessage);
        } catch (Exception e) {

        }


    }

    private void showNotification(String messageBody) {
        Log.e(TAG, "showNotification: in>>>>>>>>>>>>");
        Log.e(TAG, "messageBody: " + messageBody);
        try {
            messageBodyobject = new JSONObject(messageBody);

            Log.e(TAG, "showNotification: in>>>>>>>>>>>>");


            if (messageBodyobject.getString("flag").equals("new_reviews")) {
                objectdata = messageBodyobject.getJSONObject("data");

                Log.e(TAG, "Inside if******** = [" + messageBodyobject.get("flag") + "]");


                App.getInstance().badgeCount = App.getInstance().badgeCount + 1;

                Intent intentnot = new Intent("notification");
                intentnot.putExtra("count", "1");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intentnot);


                Intent intent = new Intent(this, ProviderDetails.class);
                intent.putExtra("From", "Ad");
                intent.putExtra("OWNERID", objectdata.getString("owner_id"));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle(objectdata.getString("businessname") + "  received a new review!")
                        .setContentText(objectdata.getString("review"))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(new Random().nextInt(11000), notificationBuilder.build());

            } else if (messageBodyobject.getString("flag").equals("new_business")) {


                App.getInstance().badgeCount = App.getInstance().badgeCount + 1;

                Intent intentnot = new Intent("notification");
                intentnot.putExtra("count", "1");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intentnot);


                objectdata = messageBodyobject.getJSONObject("data");
                Intent intent = new Intent(this, ProviderDetails.class);
                intent.putExtra("From", "Ad");
                intent.putExtra("OWNERID", objectdata.getString("owner_id"));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                int jjj = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                try {
                    URL url = new URL("https://www.localneighborhoodapp.com/" + objectdata.getString("image"));
                    bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (Exception e) {
                    System.out.println(e);
                }
                Notification notif = new Notification.Builder(this)
                        .setContentTitle(objectdata.getString("businessname") + " just registered !")
                        .setContentText(objectdata.getString("businessname"))
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setLargeIcon(bitmap).setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setStyle(new Notification.BigPictureStyle()
                                .bigPicture(bitmap))
                        .build();

                //  foregroundNote.bigContentView = bigView;

// now show notification..
                NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotifyManager.notify(jjj, notif);

            } else if (messageBodyobject.getString("flag").equals("geofencing")) {


                App.getInstance().badgeCount = App.getInstance().badgeCount + 1;

                Intent intentnot = new Intent("notification");
                intentnot.putExtra("count", "1");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intentnot);

                objectdata = messageBodyobject.getJSONObject("data");

                Intent intent = new Intent(this, ProviderDetails.class);
                intent.putExtra("From", "Ad");
                intent.putExtra("OWNERID", objectdata.getString("owner_id"));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                int jjj = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                try {
                    URL url = new URL("https://www.localneighborhoodapp.com/" + objectdata.getString("image"));
                    bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (Exception e) {
                    System.out.println(e);
                }
                Notification notif = new Notification.Builder(this)
                        .setContentTitle("You are near " + objectdata.getString("businessname"))
                        .setContentText(objectdata.getString("businessname"))
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setLargeIcon(bitmap).setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setStyle(new Notification.BigPictureStyle()
                                .bigPicture(bitmap))
                        .build();

                NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotifyManager.notify(jjj, notif);

            } else if (messageBodyobject.getString("flag").equals("feedback_request")) {


                objectdata = messageBodyobject.getJSONObject("data");

                if (!objectdata.getString("user_id").equals(preferenceHelper.getUserId())) {

                    App.getInstance().badgeCount = App.getInstance().badgeCount + 1;
                    Intent intentnot = new Intent("notification");
                    intentnot.putExtra("count", "1");
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intentnot);


                    Log.e(TAG, "Inside if******** = [" + messageBodyobject.get("flag") + "]");

                    Intent intent = new Intent(this, ProviderDetails.class);
                    intent.putExtra("From", "Ad");
                    intent.putExtra("OWNERID", objectdata.getString("owner_id"));

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher_round)
                            .setContentTitle("Requested Review")
                            .setContentText("A review is requested for " + objectdata.getString("businessname") + " please share your Experience!")
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);
                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(new Random().nextInt(11000), notificationBuilder.build());
                } else {


                    Log.e(TAG, "showNotification: feedback_request in else ");
                }


            } else if (messageBodyobject.getString("flag").equals("email_recevied")) {
                //   objectdata = messageBodyobject.getJSONObject("data");

                //  Log.e(TAG, "Inside if******** = [" + messageBodyobject.get("flag") + "]");


                App.getInstance().badgeCount = App.getInstance().badgeCount + 1;

                Intent intentnot = new Intent("notification");
                intentnot.putExtra("count", "1");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intentnot);


                Intent intent = new Intent(this, DashBoardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle("Email")
                        .setContentText(" Received email . Check Inbox - Email")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(new Random().nextInt(11000), notificationBuilder.build());

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showFCMNoti(RemoteMessage remoteMessage) {
        try {

            String type = remoteMessage.getData().get("type");
            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");

            Log.e("remote",remoteMessage.getData()+"");

            preferenceHelper = new PreferenceHelper(this);
            userId = preferenceHelper.getUserId();

//            if(title.equals("")){
//                title = getString(R.string.app_name);
//            }
            //Log.e("type",type);

//            String messageBody = remoteMessage.getData().get("data");
//
           // Log.e("messageBody",remoteMessage.getData()+"");
//            objectdata = new JSONObject(messageBody);


            String datas = remoteMessage.getData()+"";




            try {
                JSONObject jsonObject = new JSONObject(datas);
                Log.e("data",jsonObject+"");
                  type = jsonObject.getJSONObject("data").getString("type");
            }catch (Exception e){

            }

            Log.e("type",type);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher_round, BIND_IMPORTANT)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);


            Intent intent = null;

         //   Log.e("typ",type);
            showNotification = true;
            if (type.equals("new_reward")) {
                intent = new Intent(this, RewardActivity.class);
            } else if (type.equals("new_coupon")) {
                intent = new Intent(this, PramotionActivity.class);
            } else if (type.equals("new_event")) {
                intent = new Intent(this, LocalEventActivity.class);
            } else if (type.equals("new_review")) {

                String owner_id = remoteMessage.getData().get("owner_id");

                 intent = new Intent(this, ProviderDetails.class);
                intent.putExtra("From", "Ad");
                intent.putExtra("OWNERID", owner_id);

            } else if (type.equals("new_business")) {

                String owner_id = remoteMessage.getData().get("owner_id");
                intent = new Intent(this, ProviderDetails.class);
                intent.putExtra("From", "Ad");
                intent.putExtra("OWNERID", owner_id);
            } else if (type.equals("review_request")) {

                String owner_id = remoteMessage.getData().get("owner_id");
                String requesterId = remoteMessage.getData().get("user_id");

                if(requesterId.equals(userId)){
                    showNotification = false;
                }else {
                    showNotification = true;
                }

                intent = new Intent(this, ProviderDetails.class);
                intent.putExtra("From", "Ad");
                intent.putExtra("OWNERID", owner_id);


            } else if (type.equals("new_announcement")) {

                String owner_id = remoteMessage.getData().get("owner_id");
                String notificationId = remoteMessage.getData().get("id");
                String image = remoteMessage.getData().get("image");
                String titleText = remoteMessage.getData().get("title");
                String bodyText = remoteMessage.getData().get("body");
                intent = new Intent(this, PopUpActivity.class);
                intent.putExtra("image", image);
                intent.putExtra("titleText", titleText);
                intent.putExtra("bodyText", bodyText);
                intent.putExtra("notificationId", notificationId);



            }

            if(showNotification) {
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);


                notificationBuilder.setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(new Random().nextInt(11000), notificationBuilder.build());

//            NotificationManager notificationManager =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
            }

        } catch (Exception e) {

            e.printStackTrace();
        }

    }


    private RemoteViews getComplexNotificationView() {
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews notificationView = new RemoteViews(
                this.getPackageName(),
                R.layout.notification_review
        );

        // Locate and set the Image into customnotificationtext.xml ImageViews
        notificationView.setImageViewResource(R.id.fab_pick_gallery, R.mipmap.ic_launcher_round);

        try {
            // Locate and set the Text into customnotificationtext.xml TextViews
            notificationView.setTextViewText(R.id.textview_title, "fdfdf");
            notificationView.setTextViewText(R.id.textview_review, objectdata.getString("review"));
            notificationView.setInt(R.id.ratingbar, "0", 3);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return notificationView;
    }
}
