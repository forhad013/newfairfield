package com.fairfield.service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by Rohit on 7/19/2016.
 */


public interface RetrofitService {


    @FormUrlEncoded
    @POST("Userajax/userregister")
    Call<ResponseBody> user_signup(@FieldMap HashMap<String, Object> hm);

    @FormUrlEncoded
    @POST("Userajax/verifycode")
    Call<ResponseBody> verifyEmailid(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/login")
    Call<ResponseBody> emaillogin(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/resendcode")
    Call<ResponseBody> resendcodeForverify(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/forgot_password")
    Call<ResponseBody> forgotpassword(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/change_password")
    Call<ResponseBody> changepassword(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/update_profile")
    Call<ResponseBody> updateProfile(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/facebook_login")
    Call<ResponseBody> facebooklogin(@FieldMap HashMap<String, Object> hm);

    @FormUrlEncoded
    @POST("Userajax/show_local_event")
    Call<ResponseBody> getLocalEvents(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/eventlikes_by_user")
    Call<ResponseBody> saveEventbyUser(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/save_event")
    Call<ResponseBody> getsaveEvent(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/like_coupon_by_user")
    Call<ResponseBody> likeCouponByuser(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/coupon_list")
    Call<ResponseBody> allCoupan(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/like_coupon_list")
    Call<ResponseBody> show_save_coupons(@FieldMap HashMap<String, String> hm);


    @Multipart
    @POST("Userajax/add_comment")
    Call<ResponseBody> postcommrnt(@PartMap Map<String, RequestBody> params, @Part List<MultipartBody.Part> parts);


    //  @POST("Userajax/add_comment")
//    Call<ResponseBody> postMeme(@Query("user_id") String userid , @Query("event_id") String event_id , @Query("comment") String comment , @Body RequestBody files);

    @POST("Userajax/add_comment")
    Call<ResponseBody> postMeme(@Body RequestBody files);

    @POST("Userajax/active_rewards")
    Call<ResponseBody> getrewarddata();

    @FormUrlEncoded
    @POST("Userajax/view_reward")
    Call<ResponseBody> view_reward(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/is_user_entered_reward")
    Call<ResponseBody> is_user_entered_reward(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/add_reward_participant")
    Call<ResponseBody> add_reward_participant(@FieldMap HashMap<String, String> hm);

    @POST("Userajax/ratings_and_review")
    Call<ResponseBody> submitRating_Review(@Body RequestBody files);

    @FormUrlEncoded
    @POST("Userajax/show_comments")
    Call<ResponseBody> showEventComments(@FieldMap HashMap<String, String> hm);


    @POST("Userajax/category_list")
    Call<ResponseBody> getBusinessdata();


    @FormUrlEncoded
    @POST("Userajax/business")
    Call<ResponseBody> getProvidersData(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/get_rating_review")
    Call<ResponseBody> showFeedbackComments(@FieldMap HashMap<String, String> hm);


    @POST("Userajax/new_business_owner")
    Call<ResponseBody> newbusinessOwner();

    @POST("Userajax/new_reviews")
    Call<ResponseBody> new_reviews();

    @FormUrlEncoded
    @POST("Userajax/search_business_owner")
    Call<ResponseBody> searchBusinessOwner(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/business_geofencing")
    Call<ResponseBody> businessGeofencing(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/all_notification")
    Call<ResponseBody> getNotification(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/feedback_request")
    Call<ResponseBody> getAllFeedbackRequest(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/page_viewer")
    Call<ResponseBody> setpageViewer(@FieldMap HashMap<String, String> hm);


    @POST("Userajax/banners")
    Call<ResponseBody> getAllbanners();

    @FormUrlEncoded
    @POST("Userajax/banner_click")
    Call<ResponseBody> bannerClick(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/save_page_viewer")
    Call<ResponseBody> saveBannerview(@FieldMap HashMap<String, String> hm);

    @POST("Userajax/importants")
    Call<ResponseBody> getAllImportantdata();

    @FormUrlEncoded
    @POST("Userajax/remove_coupon")
    Call<ResponseBody> removecoupon(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/supports")
    Call<ResponseBody> sendTosupport(@FieldMap HashMap<String, String> hm);

    @POST("Userajax/add_reward_comment")
    Call<ResponseBody> uploadComment(@Body RequestBody files);

    @FormUrlEncoded
    @POST("Userajax/get_business_owner")
    Call<ResponseBody> getProviderDetails(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/update_user_email")
    Call<ResponseBody> updateUserEmail(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/review_request")
    Call<ResponseBody> reviewRequest(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/delete_rating")
    Call<ResponseBody> deletereview(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/delete_review_image")
    Call<ResponseBody> deleteReviewImage(@FieldMap HashMap<String, String> hm);


    @FormUrlEncoded
    @POST("Userajax/check_user")
    Call<ResponseBody> checkUser(@FieldMap HashMap<String, String> hm);

    @FormUrlEncoded
    @POST("Userajax/check_announcement")
    Call<ResponseBody> checkAnnouncement(@FieldMap HashMap<String, String> hm);





  /*  @GET("api/getcategories/{id}")
    Call<ResponseBody> subCategary(@Path("id") String subCategoriesId);
*/

//31 (main) + 5(mitificatioin) + 2(feed) + 2(fullprofile)
}
